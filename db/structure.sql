--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: areas; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.areas (
    id integer NOT NULL,
    business_id integer NOT NULL,
    name character varying(40) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    color character varying(6)
);


--
-- Name: areas_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.areas_id_seq OWNED BY public.areas.id;


--
-- Name: attachments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.attachments (
    id integer NOT NULL,
    name character varying(255),
    attachment character varying(255),
    attachable_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    attachable_type character varying(255)
);


--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.attachments_id_seq OWNED BY public.attachments.id;


--
-- Name: bookmarks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.bookmarks (
    id integer NOT NULL,
    user_id integer NOT NULL,
    bookmarkable_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    bookmarkable_type character varying(255)
);


--
-- Name: bookmarks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bookmarks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bookmarks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bookmarks_id_seq OWNED BY public.bookmarks.id;


--
-- Name: businesses; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.businesses (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    confirmation_token character varying(255),
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying(255) NOT NULL,
    cycle_length integer DEFAULT 4,
    default_vat_rate numeric(10,2) DEFAULT 0.0,
    customer_color_field_name character varying(100) DEFAULT 'Colour'::character varying,
    work_list_payment_methods character varying(255) DEFAULT 'cash,cheque,credit,debit,bank transfer'::character varying,
    credit_charge_cents integer DEFAULT 0,
    debit_charge_cents integer DEFAULT 0,
    credit_charge_is_percentage boolean DEFAULT false,
    debit_charge_is_percentage boolean DEFAULT false,
    default_publication_time time without time zone
);


--
-- Name: businesses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.businesses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: businesses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.businesses_id_seq OWNED BY public.businesses.id;


--
-- Name: customer_detail_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.customer_detail_categories (
    id integer NOT NULL,
    business_id integer NOT NULL,
    name character varying(250) NOT NULL,
    "order" integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: customer_detail_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customer_detail_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_detail_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customer_detail_categories_id_seq OWNED BY public.customer_detail_categories.id;


--
-- Name: customer_details; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.customer_details (
    id integer NOT NULL,
    customer_id integer NOT NULL,
    name character varying(250),
    value text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    parent_id integer,
    trimmed_value text,
    group_index integer DEFAULT 0
);


--
-- Name: customer_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customer_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customer_details_id_seq OWNED BY public.customer_details.id;


--
-- Name: customer_notes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.customer_notes (
    id integer NOT NULL,
    customer_id integer NOT NULL,
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    date timestamp without time zone,
    user_id integer
);


--
-- Name: customer_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customer_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customer_notes_id_seq OWNED BY public.customer_notes.id;


--
-- Name: customer_search_queries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.customer_search_queries (
    id integer NOT NULL,
    user_id integer NOT NULL,
    search_type integer NOT NULL,
    search_terms json,
    search_completed_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    xlsx_file character varying(255),
    xlsx_export_started_at timestamp without time zone,
    xlsx_export_finished_at timestamp without time zone,
    export_terms json
);


--
-- Name: customer_search_queries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customer_search_queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_search_queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customer_search_queries_id_seq OWNED BY public.customer_search_queries.id;


--
-- Name: customer_search_query_customers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.customer_search_query_customers (
    id integer NOT NULL,
    query_id integer NOT NULL,
    customer_id integer NOT NULL,
    "order" integer NOT NULL
);


--
-- Name: customer_search_query_customers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customer_search_query_customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_search_query_customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customer_search_query_customers_id_seq OWNED BY public.customer_search_query_customers.id;


--
-- Name: customers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.customers (
    id integer NOT NULL,
    business_id integer NOT NULL,
    area_id integer NOT NULL,
    regularity integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    start_date date,
    regular_price_cents integer DEFAULT 0,
    regularity_type character varying(6),
    add_vat boolean DEFAULT false,
    color character varying(6),
    non_regular boolean DEFAULT true,
    customer_details_cache public.hstore,
    last_clean date,
    jobs_count integer DEFAULT 0 NOT NULL,
    area_color_cache character varying(6) NOT NULL,
    area_name_cache character varying(255),
    active boolean DEFAULT true,
    outstanding_money_cents integer DEFAULT 0,
    owner_id integer,
    day_span integer DEFAULT 1
);


--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;


--
-- Name: default_customer_details; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.default_customer_details (
    id integer NOT NULL,
    business_id integer NOT NULL,
    name character varying(250),
    large_field boolean DEFAULT false,
    visible_in_index boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    singular boolean DEFAULT false,
    "order" integer NOT NULL,
    customer_detail_category_id integer NOT NULL,
    visible_in_work_list boolean DEFAULT false,
    work_list_order integer NOT NULL
);


--
-- Name: default_customer_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.default_customer_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: default_customer_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.default_customer_details_id_seq OWNED BY public.default_customer_details.id;


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying(255),
    queue character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.delayed_jobs_id_seq OWNED BY public.delayed_jobs.id;


--
-- Name: expenses; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.expenses (
    id integer NOT NULL,
    business_id integer NOT NULL,
    date date NOT NULL,
    cents integer DEFAULT 0,
    vat_cents integer DEFAULT 0,
    description character varying(50),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    receipt_number character varying(50),
    notes text
);


--
-- Name: expenses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.expenses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expenses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.expenses_id_seq OWNED BY public.expenses.id;


--
-- Name: finance_exports; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.finance_exports (
    id integer NOT NULL,
    user_id integer,
    terms json,
    xlsx_file character varying(255),
    export_finished_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: finance_exports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.finance_exports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: finance_exports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.finance_exports_id_seq OWNED BY public.finance_exports.id;


--
-- Name: job_notes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.job_notes (
    id integer NOT NULL,
    job_id integer NOT NULL,
    user_id integer,
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_name_cache character varying(255)
);


--
-- Name: job_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.job_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.job_notes_id_seq OWNED BY public.job_notes.id;


--
-- Name: job_payments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.job_payments (
    id integer NOT NULL,
    job_id integer NOT NULL,
    payment_id integer NOT NULL,
    cents integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: job_payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.job_payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.job_payments_id_seq OWNED BY public.job_payments.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.jobs (
    id integer NOT NULL,
    business_id integer NOT NULL,
    customer_id integer NOT NULL,
    week_id integer NOT NULL,
    cents integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    work_list_id integer,
    day integer DEFAULT (-1),
    work_list_category_id integer,
    vat_rate numeric(10,2) DEFAULT 0.0,
    completed_at date,
    paid_at date,
    payment_method character varying(15),
    payment_amount_cents integer,
    invoice_number character varying(40),
    start_time time without time zone,
    finish_time time without time zone,
    payment_charge_cents integer DEFAULT 0,
    work_list_order integer,
    amount_paid_cents integer DEFAULT 0,
    linked_job_id integer,
    child_jobs_count integer DEFAULT 0,
    linked_job_day integer DEFAULT 1,
    completed_cache boolean DEFAULT false,
    payments_count integer,
    check_risk boolean DEFAULT false
);


--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.payments (
    id integer NOT NULL,
    customer_id integer NOT NULL,
    date date NOT NULL,
    method character varying(255),
    cents integer DEFAULT 0,
    charge_cents integer DEFAULT 0,
    credit boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    job_id integer
);


--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;


--
-- Name: risks_assessments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.risks_assessments (
    id integer NOT NULL,
    yes boolean,
    no boolean,
    too_high boolean,
    ground_not_safe boolean,
    no_clear_access_to_window boolean,
    work_from_inside boolean,
    footed_ladder boolean,
    folded_ladder boolean,
    work_positioning_system boolean,
    read_and_wash boolean,
    secured_ladder boolean,
    harness boolean,
    bar boolean,
    other character varying(255),
    why text,
    operative character varying(255),
    date date,
    user_id integer,
    customer_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    not_all boolean,
    worklist_id integer
);


--
-- Name: risks_assessments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.risks_assessments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: risks_assessments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.risks_assessments_id_seq OWNED BY public.risks_assessments.id;


--
-- Name: risks_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.risks_users (
    id integer NOT NULL,
    risk_cheacked boolean,
    user_id integer,
    risks_assessment_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    date timestamp without time zone
);


--
-- Name: risks_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.risks_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: risks_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.risks_users_id_seq OWNED BY public.risks_users.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    password_digest character varying(255) NOT NULL,
    business_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    role character varying(255) DEFAULT 'Super_Admin'::character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: weeks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.weeks (
    id integer NOT NULL,
    business_id integer NOT NULL,
    notes text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    start_date date NOT NULL,
    tag character varying(255),
    finances_cache public.hstore
);


--
-- Name: weeks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.weeks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: weeks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.weeks_id_seq OWNED BY public.weeks.id;


--
-- Name: work_list_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.work_list_categories (
    id integer NOT NULL,
    business_id integer NOT NULL,
    name character varying(40) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    tag character varying(255),
    target_cents integer DEFAULT 0,
    balance_cents integer DEFAULT 0,
    balance_date_from date,
    user_id integer
);


--
-- Name: work_list_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.work_list_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_list_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.work_list_categories_id_seq OWNED BY public.work_list_categories.id;


--
-- Name: work_list_notes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.work_list_notes (
    id integer NOT NULL,
    work_list_id character varying(255) NOT NULL,
    user_id character varying(255),
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: work_list_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.work_list_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_list_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.work_list_notes_id_seq OWNED BY public.work_list_notes.id;


--
-- Name: work_list_workers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.work_list_workers (
    id integer NOT NULL,
    user_id integer,
    work_list_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: work_list_workers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.work_list_workers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_list_workers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.work_list_workers_id_seq OWNED BY public.work_list_workers.id;


--
-- Name: work_lists; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE public.work_lists (
    id integer NOT NULL,
    work_list_category_id integer NOT NULL,
    week_id integer NOT NULL,
    day integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    token_1 character varying(255),
    token_2 character varying(255),
    tag character varying(255),
    completed boolean DEFAULT false,
    target_cents integer DEFAULT 0,
    date_cache date,
    job_counter_cache integer,
    completed_job_cents_cache integer,
    target_delta_cents_cache integer DEFAULT 0,
    visible boolean DEFAULT false,
    publication_date date,
    publication_time time without time zone
);


--
-- Name: work_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.work_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.work_lists_id_seq OWNED BY public.work_lists.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.areas ALTER COLUMN id SET DEFAULT nextval('public.areas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.attachments ALTER COLUMN id SET DEFAULT nextval('public.attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bookmarks ALTER COLUMN id SET DEFAULT nextval('public.bookmarks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.businesses ALTER COLUMN id SET DEFAULT nextval('public.businesses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_detail_categories ALTER COLUMN id SET DEFAULT nextval('public.customer_detail_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_details ALTER COLUMN id SET DEFAULT nextval('public.customer_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_notes ALTER COLUMN id SET DEFAULT nextval('public.customer_notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_search_queries ALTER COLUMN id SET DEFAULT nextval('public.customer_search_queries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_search_query_customers ALTER COLUMN id SET DEFAULT nextval('public.customer_search_query_customers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.default_customer_details ALTER COLUMN id SET DEFAULT nextval('public.default_customer_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.delayed_jobs ALTER COLUMN id SET DEFAULT nextval('public.delayed_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.expenses ALTER COLUMN id SET DEFAULT nextval('public.expenses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.finance_exports ALTER COLUMN id SET DEFAULT nextval('public.finance_exports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_notes ALTER COLUMN id SET DEFAULT nextval('public.job_notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_payments ALTER COLUMN id SET DEFAULT nextval('public.job_payments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.risks_assessments ALTER COLUMN id SET DEFAULT nextval('public.risks_assessments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.risks_users ALTER COLUMN id SET DEFAULT nextval('public.risks_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.weeks ALTER COLUMN id SET DEFAULT nextval('public.weeks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_list_categories ALTER COLUMN id SET DEFAULT nextval('public.work_list_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_list_notes ALTER COLUMN id SET DEFAULT nextval('public.work_list_notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_list_workers ALTER COLUMN id SET DEFAULT nextval('public.work_list_workers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_lists ALTER COLUMN id SET DEFAULT nextval('public.work_lists_id_seq'::regclass);


--
-- Name: areas_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.areas
    ADD CONSTRAINT areas_pkey PRIMARY KEY (id);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: bookmarks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.bookmarks
    ADD CONSTRAINT bookmarks_pkey PRIMARY KEY (id);


--
-- Name: businesses_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.businesses
    ADD CONSTRAINT businesses_pkey PRIMARY KEY (id);


--
-- Name: contact_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.customer_details
    ADD CONSTRAINT contact_details_pkey PRIMARY KEY (id);


--
-- Name: customer_detail_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.customer_detail_categories
    ADD CONSTRAINT customer_detail_categories_pkey PRIMARY KEY (id);


--
-- Name: customer_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.customer_notes
    ADD CONSTRAINT customer_notes_pkey PRIMARY KEY (id);


--
-- Name: customer_search_queries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.customer_search_queries
    ADD CONSTRAINT customer_search_queries_pkey PRIMARY KEY (id);


--
-- Name: customer_search_query_customers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.customer_search_query_customers
    ADD CONSTRAINT customer_search_query_customers_pkey PRIMARY KEY (id);


--
-- Name: customers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: default_customer_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.default_customer_details
    ADD CONSTRAINT default_customer_details_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: expenses_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.expenses
    ADD CONSTRAINT expenses_pkey PRIMARY KEY (id);


--
-- Name: finance_exports_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.finance_exports
    ADD CONSTRAINT finance_exports_pkey PRIMARY KEY (id);


--
-- Name: job_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.job_notes
    ADD CONSTRAINT job_notes_pkey PRIMARY KEY (id);


--
-- Name: job_payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.job_payments
    ADD CONSTRAINT job_payments_pkey PRIMARY KEY (id);


--
-- Name: jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: risks_assessments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.risks_assessments
    ADD CONSTRAINT risks_assessments_pkey PRIMARY KEY (id);


--
-- Name: risks_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.risks_users
    ADD CONSTRAINT risks_users_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: weeks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.weeks
    ADD CONSTRAINT weeks_pkey PRIMARY KEY (id);


--
-- Name: work_list_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.work_list_categories
    ADD CONSTRAINT work_list_categories_pkey PRIMARY KEY (id);


--
-- Name: work_list_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.work_list_notes
    ADD CONSTRAINT work_list_notes_pkey PRIMARY KEY (id);


--
-- Name: work_list_workers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.work_list_workers
    ADD CONSTRAINT work_list_workers_pkey PRIMARY KEY (id);


--
-- Name: work_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY public.work_lists
    ADD CONSTRAINT work_lists_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX delayed_jobs_priority ON public.delayed_jobs USING btree (priority, run_at);


--
-- Name: index_businesses_on_confirmation_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_businesses_on_confirmation_token ON public.businesses USING btree (confirmation_token);


--
-- Name: index_businesses_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_businesses_on_email ON public.businesses USING btree (email);


--
-- Name: index_businesses_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_businesses_on_reset_password_token ON public.businesses USING btree (reset_password_token);


--
-- Name: index_customer_details_on_name_and_customer_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_customer_details_on_name_and_customer_id ON public.customer_details USING btree (name, customer_id);


--
-- Name: index_customer_search_query_customers_on_query_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_customer_search_query_customers_on_query_id ON public.customer_search_query_customers USING btree (query_id);


--
-- Name: index_customers_on_area_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_customers_on_area_id ON public.customers USING btree (area_id);


--
-- Name: index_customers_on_business_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_customers_on_business_id ON public.customers USING btree (business_id);


--
-- Name: index_default_customer_details_on_name_and_business_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_default_customer_details_on_name_and_business_id ON public.default_customer_details USING btree (name, business_id);


--
-- Name: index_risks_assessments_on_customer_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_risks_assessments_on_customer_id ON public.risks_assessments USING btree (customer_id);


--
-- Name: index_risks_users_on_risks_assessment_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_risks_users_on_risks_assessment_id ON public.risks_users USING btree (risks_assessment_id);


--
-- Name: index_risks_users_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_risks_users_on_user_id ON public.risks_users USING btree (user_id);


--
-- Name: index_work_list_categories_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_work_list_categories_on_user_id ON public.work_list_categories USING btree (user_id);


--
-- Name: index_work_list_workers_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_work_list_workers_on_user_id ON public.work_list_workers USING btree (user_id);


--
-- Name: index_work_list_workers_on_work_list_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_work_list_workers_on_work_list_id ON public.work_list_workers USING btree (work_list_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20130520155129');

INSERT INTO schema_migrations (version) VALUES ('20130520162412');

INSERT INTO schema_migrations (version) VALUES ('20130520163617');

INSERT INTO schema_migrations (version) VALUES ('20130524150106');

INSERT INTO schema_migrations (version) VALUES ('20130524153544');

INSERT INTO schema_migrations (version) VALUES ('20130524154116');

INSERT INTO schema_migrations (version) VALUES ('20130524154406');

INSERT INTO schema_migrations (version) VALUES ('20130524161240');

INSERT INTO schema_migrations (version) VALUES ('20130614193604');

INSERT INTO schema_migrations (version) VALUES ('20130614200649');

INSERT INTO schema_migrations (version) VALUES ('20130614202506');

INSERT INTO schema_migrations (version) VALUES ('20130614202751');

INSERT INTO schema_migrations (version) VALUES ('20130705222356');

INSERT INTO schema_migrations (version) VALUES ('20130705224921');

INSERT INTO schema_migrations (version) VALUES ('20130705225418');

INSERT INTO schema_migrations (version) VALUES ('20130709102607');

INSERT INTO schema_migrations (version) VALUES ('20130709111058');

INSERT INTO schema_migrations (version) VALUES ('20130724183500');

INSERT INTO schema_migrations (version) VALUES ('20130724184201');

INSERT INTO schema_migrations (version) VALUES ('20130724184807');

INSERT INTO schema_migrations (version) VALUES ('20130724185009');

INSERT INTO schema_migrations (version) VALUES ('20130725062534');

INSERT INTO schema_migrations (version) VALUES ('20130725165824');

INSERT INTO schema_migrations (version) VALUES ('20130725170323');

INSERT INTO schema_migrations (version) VALUES ('20130725170752');

INSERT INTO schema_migrations (version) VALUES ('20130803195832');

INSERT INTO schema_migrations (version) VALUES ('20130803201301');

INSERT INTO schema_migrations (version) VALUES ('20130829071842');

INSERT INTO schema_migrations (version) VALUES ('20130829072924');

INSERT INTO schema_migrations (version) VALUES ('20130829073520');

INSERT INTO schema_migrations (version) VALUES ('20130906183539');

INSERT INTO schema_migrations (version) VALUES ('20130906184016');

INSERT INTO schema_migrations (version) VALUES ('20130909133320');

INSERT INTO schema_migrations (version) VALUES ('20130909143916');

INSERT INTO schema_migrations (version) VALUES ('20130916175353');

INSERT INTO schema_migrations (version) VALUES ('20130916182112');

INSERT INTO schema_migrations (version) VALUES ('20130917174509');

INSERT INTO schema_migrations (version) VALUES ('20130917175359');

INSERT INTO schema_migrations (version) VALUES ('20130917203829');

INSERT INTO schema_migrations (version) VALUES ('20130928162743');

INSERT INTO schema_migrations (version) VALUES ('20130928164126');

INSERT INTO schema_migrations (version) VALUES ('20130928201752');

INSERT INTO schema_migrations (version) VALUES ('20131111192442');

INSERT INTO schema_migrations (version) VALUES ('20131129191610');

INSERT INTO schema_migrations (version) VALUES ('20131210192751');

INSERT INTO schema_migrations (version) VALUES ('20131210193701');

INSERT INTO schema_migrations (version) VALUES ('20131226151652');

INSERT INTO schema_migrations (version) VALUES ('20131227124254');

INSERT INTO schema_migrations (version) VALUES ('20131227141551');

INSERT INTO schema_migrations (version) VALUES ('20131227171636');

INSERT INTO schema_migrations (version) VALUES ('20131227183029');

INSERT INTO schema_migrations (version) VALUES ('20140111171931');

INSERT INTO schema_migrations (version) VALUES ('20140111171956');

INSERT INTO schema_migrations (version) VALUES ('20140210192144');

INSERT INTO schema_migrations (version) VALUES ('20140211185701');

INSERT INTO schema_migrations (version) VALUES ('20140211185955');

INSERT INTO schema_migrations (version) VALUES ('20140313172040');

INSERT INTO schema_migrations (version) VALUES ('20140313173838');

INSERT INTO schema_migrations (version) VALUES ('20140314124226');

INSERT INTO schema_migrations (version) VALUES ('20140411155207');

INSERT INTO schema_migrations (version) VALUES ('20140602192823');

INSERT INTO schema_migrations (version) VALUES ('20140622100735');

INSERT INTO schema_migrations (version) VALUES ('20140622101035');

INSERT INTO schema_migrations (version) VALUES ('20140622111131');

INSERT INTO schema_migrations (version) VALUES ('20140623074927');

INSERT INTO schema_migrations (version) VALUES ('20140830184205');

INSERT INTO schema_migrations (version) VALUES ('20140830185431');

INSERT INTO schema_migrations (version) VALUES ('20140906135919');

INSERT INTO schema_migrations (version) VALUES ('20141122144814');

INSERT INTO schema_migrations (version) VALUES ('20141122151212');

INSERT INTO schema_migrations (version) VALUES ('20141124095043');

INSERT INTO schema_migrations (version) VALUES ('20150403195742');

INSERT INTO schema_migrations (version) VALUES ('20150706123546');

INSERT INTO schema_migrations (version) VALUES ('20150706123729');

INSERT INTO schema_migrations (version) VALUES ('20150707105746');

INSERT INTO schema_migrations (version) VALUES ('20150707150313');

INSERT INTO schema_migrations (version) VALUES ('20150713104946');

INSERT INTO schema_migrations (version) VALUES ('20150713105733');

INSERT INTO schema_migrations (version) VALUES ('20150713154623');

INSERT INTO schema_migrations (version) VALUES ('20150714114018');

INSERT INTO schema_migrations (version) VALUES ('20150715064703');

INSERT INTO schema_migrations (version) VALUES ('20150715133647');

INSERT INTO schema_migrations (version) VALUES ('20150715141508');

INSERT INTO schema_migrations (version) VALUES ('20150720094359');

INSERT INTO schema_migrations (version) VALUES ('20150720094539');

INSERT INTO schema_migrations (version) VALUES ('20150722131317');

INSERT INTO schema_migrations (version) VALUES ('20150727092259');

INSERT INTO schema_migrations (version) VALUES ('20150727120646');

INSERT INTO schema_migrations (version) VALUES ('20150727125910');

INSERT INTO schema_migrations (version) VALUES ('20150727140518');

INSERT INTO schema_migrations (version) VALUES ('20150729064059');

INSERT INTO schema_migrations (version) VALUES ('20150729115115');

INSERT INTO schema_migrations (version) VALUES ('20150729115419');

INSERT INTO schema_migrations (version) VALUES ('20150803170244');

INSERT INTO schema_migrations (version) VALUES ('20150804100425');

INSERT INTO schema_migrations (version) VALUES ('20150804101913');

INSERT INTO schema_migrations (version) VALUES ('20150804134330');

INSERT INTO schema_migrations (version) VALUES ('20150805082748');

INSERT INTO schema_migrations (version) VALUES ('20150810074429');

INSERT INTO schema_migrations (version) VALUES ('20150811100738');

INSERT INTO schema_migrations (version) VALUES ('20150811144251');

INSERT INTO schema_migrations (version) VALUES ('20150812075756');

INSERT INTO schema_migrations (version) VALUES ('20150812113622');

INSERT INTO schema_migrations (version) VALUES ('20150812132601');

INSERT INTO schema_migrations (version) VALUES ('20151228122418');

INSERT INTO schema_migrations (version) VALUES ('20151229115620');

INSERT INTO schema_migrations (version) VALUES ('20151229162051');

INSERT INTO schema_migrations (version) VALUES ('20151231121834');

INSERT INTO schema_migrations (version) VALUES ('20160103191257');

INSERT INTO schema_migrations (version) VALUES ('20190121194621');

INSERT INTO schema_migrations (version) VALUES ('20190219193119');

INSERT INTO schema_migrations (version) VALUES ('20190219194517');

INSERT INTO schema_migrations (version) VALUES ('20190219201341');

INSERT INTO schema_migrations (version) VALUES ('20190221182732');

INSERT INTO schema_migrations (version) VALUES ('20190312202410');

INSERT INTO schema_migrations (version) VALUES ('20190708192750');

INSERT INTO schema_migrations (version) VALUES ('20190730195636');

INSERT INTO schema_migrations (version) VALUES ('20200220154648');

INSERT INTO schema_migrations (version) VALUES ('20200304212630');

INSERT INTO schema_migrations (version) VALUES ('20200309150138');

INSERT INTO schema_migrations (version) VALUES ('20200309172627');

INSERT INTO schema_migrations (version) VALUES ('20200322192644');

INSERT INTO schema_migrations (version) VALUES ('20200719112703');

INSERT INTO schema_migrations (version) VALUES ('20201104205707');
