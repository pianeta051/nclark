class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.integer :business_id, null: false
      t.date :date, null: false
      t.integer :cents, default: 0
      t.integer :vat_cents, default: 0
      t.text :description

      t.timestamps
    end
  end
end
