class CreateRisksAssessments < ActiveRecord::Migration
  def change
    create_table :risks_assessments do |t|
      t.boolean :yes
      t.boolean :no
      t.boolean :too_high
      t.boolean :ground_not_safe
      t.boolean :no_clear_access_to_window
      t.boolean :work_from_inside
      t.boolean :footed_ladder
      t.boolean :folded_ladder
      t.boolean :work_positioning_system
      t.boolean :read_and_wash
      t.boolean :secured_ladder
      t.boolean :harness
      t.boolean :bar
      t.string :other
      t.string :why
      t.string :operative
      t.date :date
      t.references :user
      t.references :customer, index: true

      t.timestamps
    end
  end
end
