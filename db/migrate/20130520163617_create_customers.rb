class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.integer :business_id, :null => false
      t.string :title, limit: 4
      t.string :forename, limit: 35
      t.string :surname, limit: 35
      t.string :address_line_1, limit: 100
      t.string :address_line_2, limit: 100
      t.string :town, limit: 35
      t.string :postcode, limit: 9
      t.string :phone, limit: 11
      t.string :email, limit: 200
      t.integer :area_id
      t.integer :regularity

      t.timestamps
    end
  end
end
