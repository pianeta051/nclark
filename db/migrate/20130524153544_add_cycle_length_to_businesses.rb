class AddCycleLengthToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :cycle_length, :integer, default: 4
  end
end
