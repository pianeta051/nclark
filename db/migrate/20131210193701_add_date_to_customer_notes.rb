class AddDateToCustomerNotes < ActiveRecord::Migration
  def up
    add_column :customer_notes, :date, :date
    CustomerNote.find_each { |customer_note| customer_note.update_column(:date, customer_note.created_at) }
  end

  def down
    remove_column :customer_notes, :date
  end
end
