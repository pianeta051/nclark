class AddNotAllToRisksAssessments < ActiveRecord::Migration
  def change
    add_column :risks_assessments, :not_all, :boolean
  end
end
