class AddCustomerDetailsCacheToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :customer_details_cache, :hstore
  end
end
