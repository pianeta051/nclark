class AddCreditChargeIsPercentageAndDebitChargeIsPercentageToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :credit_charge_is_percentage, :boolean, default: false
    add_column :businesses, :debit_charge_is_percentage, :boolean, default: false
  end
end
