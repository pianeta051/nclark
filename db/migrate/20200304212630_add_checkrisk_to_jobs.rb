class AddCheckriskToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :check_risk, :boolean, default: false
  end
end
