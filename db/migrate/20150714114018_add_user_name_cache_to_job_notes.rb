class AddUserNameCacheToJobNotes < ActiveRecord::Migration
  def up
    add_column :job_notes, :user_name_cache, :string
    User.find_each do |user|
      user.job_notes.update_all(user_name_cache: user.name)
    end
  end

  def down
    remove_column :job_notes, :user_name_cache
  end
end
