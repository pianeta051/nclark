class AddTagToWorkListCategories < ActiveRecord::Migration
  def up
    add_column :work_list_categories, :tag, :string
    WorkListCategory.find_each do |work_list_category|
      work_list_category.update_column(:tag, work_list_category.name)
    end
  end

  def down
    remove_column :work_list_categories, :tag
  end
end
