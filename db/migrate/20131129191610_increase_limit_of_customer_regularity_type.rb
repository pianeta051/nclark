class IncreaseLimitOfCustomerRegularityType < ActiveRecord::Migration
  def up
    change_column :customers, :regularity_type, :string, limit: 6
  end

  def down
    change_column :customers, :regularity_type, :string, limit: 5
  end
end
