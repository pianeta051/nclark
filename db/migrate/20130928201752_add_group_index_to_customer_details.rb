class AddGroupIndexToCustomerDetails < ActiveRecord::Migration
  def change
    add_column :customer_details, :group_index, :integer, default: 0
  end
end
