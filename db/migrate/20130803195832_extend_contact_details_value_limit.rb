class ExtendContactDetailsValueLimit < ActiveRecord::Migration
  def up
    change_column :contact_details, :value, :string, limit: 100
  end

  def down
    change_column :contact_details, :value, :string, limit: 40
  end
end
