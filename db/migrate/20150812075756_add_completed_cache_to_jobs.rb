class AddCompletedCacheToJobs < ActiveRecord::Migration
  def up
    add_column :jobs, :completed_cache, :boolean, default: false
  end

  def down
    remove_column :jobs, :completed_cache
  end
end
