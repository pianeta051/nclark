class ChangeDataTypeForWhy < ActiveRecord::Migration
  def change
    change_column(:risks_assessments, :why, :text)
  end
end
