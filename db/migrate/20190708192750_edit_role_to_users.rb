class EditRoleToUsers < ActiveRecord::Migration
  def change
    change_column :users, :role, :string, default: 'Super_Admin'
  end
end
