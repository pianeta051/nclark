class RemoveAttachmentFromExpenses < ActiveRecord::Migration
  def up
    remove_column :expenses, :attachment
  end

  def down
    add_column :expenses, :attachment, :string
  end
end
