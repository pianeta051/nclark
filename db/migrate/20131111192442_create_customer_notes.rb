class CreateCustomerNotes < ActiveRecord::Migration
  def change
    create_table :customer_notes do |t|
      t.integer :customer_id, null: false
      t.text :content

      t.timestamps
    end
  end
end
