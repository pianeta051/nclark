class CreateRisksUsers < ActiveRecord::Migration
  def change
    create_table :risks_users do |t|
      t.boolean :risk_cheacked
      t.references :user, index: true
      t.references :risks_assessment, index: true

      t.timestamps
    end
  end
end
