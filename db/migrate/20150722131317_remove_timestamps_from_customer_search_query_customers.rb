class RemoveTimestampsFromCustomerSearchQueryCustomers < ActiveRecord::Migration
  def up
    remove_column :customer_search_query_customers, :created_at
    remove_column :customer_search_query_customers, :updated_at
  end

  def down
    add_column :customer_search_query_customers, :created_at, :datetime
    add_column :customer_search_query_customers, :updated_at, :datetime
  end
end
