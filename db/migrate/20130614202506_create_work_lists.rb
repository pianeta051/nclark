class CreateWorkLists < ActiveRecord::Migration
  def change
    create_table :work_lists do |t|
      t.integer :work_list_category_id, null: false
      t.integer :week_id, null: false
      t.integer :day, null: false

      t.timestamps
    end
  end
end
