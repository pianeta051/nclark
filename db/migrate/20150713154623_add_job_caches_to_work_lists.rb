class AddJobCachesToWorkLists < ActiveRecord::Migration
  def up
    add_column :work_lists, :job_counter_cache, :integer
    add_column :work_lists, :completed_job_cents_cache, :integer
    WorkList.find_each(&:update_job_caches!)
  end

  def down
    remove_column :work_lists, :job_counter_cache
    remove_column :work_lists, :completed_job_cents_cache
  end
end
