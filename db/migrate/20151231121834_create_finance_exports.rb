class CreateFinanceExports < ActiveRecord::Migration
  def change
    create_table :finance_exports do |t|
      t.integer :user_id
      t.json :terms
      t.string :xlsx_file
      t.datetime :export_finished_at

      t.timestamps
    end
  end
end
