class AddIgnoredToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :ignored, :boolean, default: false
  end
end
