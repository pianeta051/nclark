class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :customer_id, null: false
      t.date :date, null: false
      t.string :method
      t.integer :cents, default: 0
      t.integer :charge_cents, default: 0
      t.boolean :credit, default: false

      t.timestamps
    end
  end
end
