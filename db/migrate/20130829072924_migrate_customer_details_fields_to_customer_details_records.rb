class MigrateCustomerDetailsFieldsToCustomerDetailsRecords < ActiveRecord::Migration
  def up
    Customer.find_each do |customer|
      %w(title forename surname address_line_1 address_line_2 town postcode notes county).each do |field_name|
        customer.customer_details.create(:name => field_name.titleize, :value => customer.send(field_name))
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
