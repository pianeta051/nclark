class AddIndexes < ActiveRecord::Migration
  def change
    add_index :customers, :business_id
    add_index :customers, :area_id
    add_index :customer_details, [:name, :customer_id]
    add_index :default_customer_details, [:name, :business_id]
  end
end
