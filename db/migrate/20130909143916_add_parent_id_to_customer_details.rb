class AddParentIdToCustomerDetails < ActiveRecord::Migration
  def change
    add_column :customer_details, :parent_id, :integer
  end
end
