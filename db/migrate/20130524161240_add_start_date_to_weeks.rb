class AddStartDateToWeeks < ActiveRecord::Migration
  def change
    add_column :weeks, :start_date, :date, null: false
  end
end
