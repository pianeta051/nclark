class RemoveIgnoredFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :ignored
  end
end
