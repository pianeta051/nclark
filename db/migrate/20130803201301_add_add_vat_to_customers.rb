class AddAddVatToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :add_vat, :boolean, default: false
  end
end
