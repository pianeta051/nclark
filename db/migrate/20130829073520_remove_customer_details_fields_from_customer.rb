class RemoveCustomerDetailsFieldsFromCustomer < ActiveRecord::Migration
  def up
    remove_column :customers, :title
    remove_column :customers, :forename
    remove_column :customers, :surname
    remove_column :customers, :address_line_1
    remove_column :customers, :address_line_2
    remove_column :customers, :town
    remove_column :customers, :postcode
    remove_column :customers, :notes
  end

  def down
    add_column :customers, :title, :string, limit: 10
    add_column :customers, :forename, :string, limit: 35
    add_column :customers, :surname, :string, limit: 35
    add_column :customers, :address_line_1, :string, limit: 100
    add_column :customers, :address_line_2, :string, limit: 100
    add_column :customers, :town, :string, limit: 35
    add_column :customers, :postcode, :string, limit: 9
    add_column :customers, :notes, :text
  end
end
