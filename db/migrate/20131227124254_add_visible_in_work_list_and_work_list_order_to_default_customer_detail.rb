class AddVisibleInWorkListAndWorkListOrderToDefaultCustomerDetail < ActiveRecord::Migration
  def up
    add_column :default_customer_details, :visible_in_work_list, :boolean, default: false
    add_column :default_customer_details, :work_list_order, :integer

    Business.find_each do |business|
      count = 0
      business.customer_detail_categories.order('customer_detail_categories.order ASC').find_each do |cdc|
        cdc.default_customer_details.order('default_customer_details.order ASC').find_each do |dcd|
          dcd.update_columns(visible_in_work_list: dcd.visible_in_index, work_list_order: count)
          count += 1
        end
      end
    end

    change_column :default_customer_details, :work_list_order, :integer, null: false
  end

  def down
    remove_column :default_customer_details, :visible_in_work_list
    remove_column :default_customer_details, :work_list_order
  end
end
