class RemoveNotesFromJobs < ActiveRecord::Migration
  def up
    JobNote.delete_all
    Job.find_each do |job|
      job.job_notes.create!(content: job.notes) if job.notes.present?
    end
    remove_column :jobs, :notes
  end

  def down
    add_column :jobs, :notes, :text
    Job.find_each do |job|
      job.update_column(:notes, job.job_notes.map(&:content).join("<br/><br/>"))
    end
    JobNote.delete_all
  end
end
