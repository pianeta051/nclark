class AddBalanceDateFromToWorkListCategories < ActiveRecord::Migration
  def change
    add_column :work_list_categories, :balance_date_from, :date
  end
end
