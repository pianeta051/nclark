class AddCustomerDetailCategoryIdToDefaultCustomerDetails < ActiveRecord::Migration
  def up
    Business.find_each do |business|
      business.customer_detail_categories.create!(name: "Details", order: 0)
    end
    add_column :default_customer_details, :customer_detail_category_id, :integer
    Business.find_each do |business|
      business.default_customer_details.update_all(customer_detail_category_id: business.customer_detail_categories.first.id)
    end
    change_column :default_customer_details, :customer_detail_category_id, :integer, null: false
  end

  def down
    remove_column :default_customer_details, :customer_detail_category_id
    CustomerDetailCategory.destroy_all
  end
end
