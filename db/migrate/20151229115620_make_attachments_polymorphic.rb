class MakeAttachmentsPolymorphic < ActiveRecord::Migration
  def up
    rename_column :attachments, :customer_id, :attachable_id
    add_column :attachments, :attachable_type, :string
    Attachment.update_all(attachable_type: "Customer")
  end

  def down
    Attachment.where("attachments.attachable_type != ?", "Customer").update_all(attachable_id: nil)
    remove_column :attachments, :attachable_type
    rename_column :attachments, :attachable_id, :customer_id
  end
end
