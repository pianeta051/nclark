class AddAreaNameCacheToCustomers < ActiveRecord::Migration
  def up
    add_column :customers, :area_name_cache, :string
    Area.find_each do |area|
      area.customers.update_all(area_name_cache: area.name)
    end
  end

  def down
    remove_column :customers, :area_name_cache
  end
end
