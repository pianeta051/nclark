class AddXlsxFileToCustomerSearchQueries < ActiveRecord::Migration
  def change
    add_column :customer_search_queries, :xlsx_file, :string
    add_column :customer_search_queries, :xlsx_export_started_at, :datetime
    add_column :customer_search_queries, :xlsx_export_finished_at, :datetime
  end
end
