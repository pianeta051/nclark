class RenameContactDetailsToCustomerDetails < ActiveRecord::Migration
  def up
    change_column :contact_details, :name, :string, limit: 250
    change_column :contact_details, :value, :text
    rename_table :contact_details, :customer_details
  end

  def down
    rename_table :customer_details, :contact_details
    change_column :contact_details, :value, :string, limit: 100
    change_column :contact_details, :name, :string, limit: 40
  end
end
