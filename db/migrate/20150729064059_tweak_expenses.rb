class TweakExpenses < ActiveRecord::Migration
  def up
    add_column :expenses, :receipt_number, :string, limit: 50
    add_column :expenses, :notes, :text
    add_column :expenses, :attachment, :string
    change_column :expenses, :description, :string, limit: 50
  end

  def down
    change_column :expenses, :description, :text
    remove_column :expenses, :attachment
    remove_column :expenses, :notes
    remove_column :expenses, :receipt_number
  end
end
