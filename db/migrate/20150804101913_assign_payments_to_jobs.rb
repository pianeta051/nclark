class AssignPaymentsToJobs < ActiveRecord::Migration
  def up
    Payment.find_each do |payment|
      job_ids = payment.customer
                       .jobs
                       .where(paid_at: payment.date, payment_amount_cents: payment.cents)
                       .where('jobs.id NOT IN (SELECT payments.job_id FROM payments WHERE payments.job_id IS NOT NULL)')
                       .pluck(:id)
      if job_ids.length > 0
        payment.update_column(:job_id, job_ids.first)
      end
    end
  end

  def down
    Payment.update_all(job_id: nil)
  end
end
