class AddCreditChargeCentsAndDebitChargeCentsToBusiness < ActiveRecord::Migration
  def change
    add_column :businesses, :credit_charge_cents, :integer, default: 0
    add_column :businesses, :debit_charge_cents, :integer, default: 0
  end
end
