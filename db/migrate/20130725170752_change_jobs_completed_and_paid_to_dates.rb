class ChangeJobsCompletedAndPaidToDates < ActiveRecord::Migration
  def up
    add_column :jobs, :completed_at, :date
    add_column :jobs, :paid_at, :date

    Job.where(completed: true).update_all(completed_at: Date.today)
    Job.where(paid: true).update_all(paid_at: Date.today)

    remove_column :jobs, :completed
    remove_column :jobs, :paid
  end

  def down
    add_column :jobs, :completed, :boolean, default: false
    add_column :jobs, :paid, :boolean, default: false

    Job.where('jobs.completed_at IS NOT NULL').update_all(completed: true)
    Job.where('jobs.paid_at IS NOT NULL').update_all(paid: true)

    remove_column :jobs, :completed_at
    remove_column :jobs, :paid_at
  end
end
