class AddTrimmedValueToCustomerDetails < ActiveRecord::Migration
  def up
    add_column :customer_details, :trimmed_value, :text
    CustomerDetail.find_each do |customer_detail|
      customer_detail.update_column(:trimmed_value, customer_detail.value[/[a-zA-Z].*/])
    end
  end

  def down
    remove_column :customer_details, :trimmed_value
  end
end
