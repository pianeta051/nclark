class AddCustomerColorFieldNameToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :customer_color_field_name, :string, limit: 100, default: 'Colour'
  end
end
