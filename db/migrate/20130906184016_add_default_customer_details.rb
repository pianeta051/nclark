class AddDefaultCustomerDetails < ActiveRecord::Migration
  def up
    Business.find_each do |business|
      %w(title forename surname address_line_1 address_line_2 town postcode county).each_with_index do |field_name, i|
        business.default_customer_details.create(name: field_name.titleize, visible_in_index: i < 3, large_field: false)
        CustomerDetail.where(name: field_name).update_all(name: field_name.titleize)
      end

      %w(notes).each do |field_name|
        business.default_customer_details.create(name: field_name.titleize, visible_in_index: false, large_field: true)
        CustomerDetail.where(name: field_name).update_all(name: field_name.titleize)
      end
    end
  end

  def down
    DefaultCustomerDetail.delete_all
  end
end
