class AddNonRegularToCustomers < ActiveRecord::Migration
  def up
    add_column :customers, :non_regular, :boolean, default: false
    Customer.where(regularity_type: 'callus').update_all(regularity_type: nil)
  end

  def down
    remove_column :customers, :non_regular
  end
end
