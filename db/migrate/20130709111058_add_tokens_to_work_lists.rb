class AddTokensToWorkLists < ActiveRecord::Migration
  def change
    add_column :work_lists, :token_1, :string
    add_column :work_lists, :token_2, :string
  end
end
