class AddStartDateAndRegularPriceCentsToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :start_date, :date
    add_column :customers, :regular_price_cents, :integer, default: 0
  end
end
