class CreateCustomerSearchQueries < ActiveRecord::Migration
  def change
    create_table :customer_search_queries do |t|
      t.integer :user_id, null: false
      t.integer :search_type, null: false
      t.json :search_terms
      t.datetime :search_completed_at

      t.timestamps
    end
  end
end
