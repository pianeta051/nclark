class AddOrderToDefaultCustomerDetails < ActiveRecord::Migration
  def up
    add_column :default_customer_details, :order, :integer
    Business.find_each do |business|
      default_customer_details = business.default_customer_details.all
      default_customer_details.each do |default_customer_detail|
        default_customer_detail.update_column(:order, default_customer_details.index(default_customer_detail))
      end
    end
    change_column :default_customer_details, :order, :integer, null: false
  end

  def down
    remove_column :default_customer_details, :order
  end
end
