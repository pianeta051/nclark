class AddPaymentMethodAndPaymentAmountAndInvoiceNumberToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :payment_method, :string, limit: 15
    add_column :jobs, :payment_amount_cents, :integer
    add_column :jobs, :invoice_number, :string, limit: 40
  end
end
