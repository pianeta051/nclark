class AddCompletedToWorkLists < ActiveRecord::Migration
  def change
    add_column :work_lists, :completed, :boolean, default: false
  end
end
