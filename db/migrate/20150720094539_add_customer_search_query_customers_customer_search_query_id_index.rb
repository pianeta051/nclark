class AddCustomerSearchQueryCustomersCustomerSearchQueryIdIndex < ActiveRecord::Migration
  def change
    add_index :customer_search_query_customers, :query_id
  end
end
