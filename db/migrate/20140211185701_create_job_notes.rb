class CreateJobNotes < ActiveRecord::Migration
  def change
    create_table :job_notes do |t|
      t.integer :job_id, null: false
      t.integer :user_id
      t.text :content

      t.timestamps
    end
  end
end
