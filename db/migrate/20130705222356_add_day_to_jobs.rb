class AddDayToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :day, :integer, default: -1
  end
end
