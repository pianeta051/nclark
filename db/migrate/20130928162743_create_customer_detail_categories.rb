class CreateCustomerDetailCategories < ActiveRecord::Migration
  def change
    create_table :customer_detail_categories do |t|
      t.integer :business_id, null: false
      t.string :name, null: false, limit: 250
      t.integer :order, null: false

      t.timestamps
    end
  end
end
