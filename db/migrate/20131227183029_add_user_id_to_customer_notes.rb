class AddUserIdToCustomerNotes < ActiveRecord::Migration
  def change
    add_column :customer_notes, :user_id, :integer
  end
end
