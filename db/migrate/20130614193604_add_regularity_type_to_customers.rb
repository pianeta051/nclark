class AddRegularityTypeToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :regularity_type, :string, limit: 5
  end
end
