class AddWorkListCategoryIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :work_list_category_id, :integer
  end
end
