class CreateWorkListWorkers < ActiveRecord::Migration
  def change
    create_table :work_list_workers do |t|
      t.references :user, index: true
      t.references :work_list, index: true

      t.timestamps
    end
  end
end
