class AddDateCacheToWorkLists < ActiveRecord::Migration
  def up
    add_column :work_lists, :date_cache, :date

    Week.find_each do |week|
      week.work_lists.each do |work_list|
        work_list.update_column(:date_cache, week.start_date + work_list.day.days)
      end
    end
  end

  def down
    remove_column :work_lists, :date_cache
  end
end
