class AddDateToRisksUsers < ActiveRecord::Migration
  def change
    add_column :risks_users, :date, :datetime
  end
end
