class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.integer :business_id, :null => false
      t.string :name, null: false, limit: 40

      t.timestamps
    end
  end
end
