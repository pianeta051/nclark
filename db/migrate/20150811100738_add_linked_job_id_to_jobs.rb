class AddLinkedJobIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :linked_job_id, :integer
    add_column :jobs, :child_jobs_count, :integer, default: 0
    add_column :jobs, :linked_job_day, :integer, default: 1
    add_column :customers, :day_span, :integer, default: 1
  end
end
