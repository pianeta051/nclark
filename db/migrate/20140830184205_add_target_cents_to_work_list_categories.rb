class AddTargetCentsToWorkListCategories < ActiveRecord::Migration
  def change
    add_column :work_list_categories, :target_cents, :integer, default: 0
  end
end
