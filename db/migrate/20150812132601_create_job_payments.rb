class CreateJobPayments < ActiveRecord::Migration
  def change
    create_table :job_payments do |t|
      t.integer :job_id, null: false
      t.integer :payment_id, null: false
      t.integer :cents, null: false

      t.timestamps
    end
  end
end
