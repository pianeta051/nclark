class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :name
      t.string :attachment
      t.integer :customer_id

      t.timestamps
    end
  end
end
