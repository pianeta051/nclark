class RemovePhoneAndEmailFromCustomers < ActiveRecord::Migration
  def up
    Customer.find_each do |customer|
      customer.contact_details.create(name: 'phone', value: customer.phone) if customer.phone.present?
      customer.contact_details.create(name: 'email', value: customer.email) if customer.email.present?
    end

    remove_column :customers, :phone
    remove_column :customers, :email
  end

  def down
    add_column :phone, :string, limit: 11
    add_column :email, :string, limit: 200
  end
end
