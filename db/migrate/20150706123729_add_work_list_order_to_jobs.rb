class AddWorkListOrderToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :work_list_order, :integer
  end
end
