class ChangeCustomerNotesDateToDateTime < ActiveRecord::Migration
  def up
    change_column :customer_notes, :date, :datetime
    CustomerNote.find_each do |customer_note|
      customer_note.update_column(:date, customer_note.date + 12.hours)
    end
  end

  def down
    change_column :customer_notes, :date, :date
  end
end
