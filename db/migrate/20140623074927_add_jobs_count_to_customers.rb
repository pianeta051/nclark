class AddJobsCountToCustomers < ActiveRecord::Migration
  def up
    add_column :customers, :jobs_count, :integer, default: 0, null: false
    Customer.find_each(select: 'id') do |customer|
      Customer.reset_counters(customer.id, :jobs)
    end
  end

  def down
    remove_column :customers, :jobs_count
  end
end
