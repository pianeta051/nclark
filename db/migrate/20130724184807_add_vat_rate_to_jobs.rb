class AddVatRateToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :vat_rate, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
