class AddPublicationTimeToWorkLists < ActiveRecord::Migration
  def change
    add_column :work_lists, :publication_time, :time
  end
end
