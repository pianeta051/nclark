class AddSingularToDefaultCustomerDetails < ActiveRecord::Migration
  def change
    add_column :default_customer_details, :singular, :boolean, default: false
  end
end
