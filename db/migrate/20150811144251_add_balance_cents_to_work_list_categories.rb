class AddBalanceCentsToWorkListCategories < ActiveRecord::Migration
  def up
    add_column :work_list_categories, :balance_cents, :integer, default: 0
    add_column :work_lists, :target_delta_cents_cache, :integer, default: 0
  end

  def down
    remove_column :work_lists, :target_delta_cents_cache
    remove_column :work_list_categories, :balance_cents
  end
end
