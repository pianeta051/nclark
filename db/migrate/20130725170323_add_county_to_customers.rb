class AddCountyToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :county, :string, :limit => 40
  end
end
