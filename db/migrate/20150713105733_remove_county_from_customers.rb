class RemoveCountyFromCustomers < ActiveRecord::Migration
  def up
    remove_column :customers, :county
  end

  def down
    add_column :customers, :county, :string, limit: 40
  end
end
