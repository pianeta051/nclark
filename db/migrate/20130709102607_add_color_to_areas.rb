class AddColorToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :color, :string, limit: 6
  end
end
