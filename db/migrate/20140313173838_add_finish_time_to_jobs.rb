class AddFinishTimeToJobs < ActiveRecord::Migration
  def change
    rename_column :jobs, :time, :start_time
    add_column :jobs, :finish_time, :time
  end
end
