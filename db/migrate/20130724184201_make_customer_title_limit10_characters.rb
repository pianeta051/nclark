class MakeCustomerTitleLimit10Characters < ActiveRecord::Migration
  def up
    change_column :customers, :title, :string, :limit => 10
  end

  def down
    change_column :customers, :title, :string, :limit => 4
  end
end
