class AddFinancesCacheToWeeks < ActiveRecord::Migration
  def change
    add_column :weeks, :finances_cache, :hstore
  end
end
