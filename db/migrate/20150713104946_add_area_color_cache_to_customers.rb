class AddAreaColorCacheToCustomers < ActiveRecord::Migration
  def up
    add_column :customers, :area_color_cache, :string
    Customer.where(area_id: nil).delete_all
    Area.find_each do |area|
      area.customers.update_all(area_color_cache: area.color)
    end
    change_column :customers, :area_color_cache, :string, limit: 6, null: false
    change_column :customers, :area_id, :integer, null: false
  end

  def down
    remove_column :customers, :area_color_cache
  end
end
