class AddTagToWorkLists < ActiveRecord::Migration
  def up
    add_column :work_lists, :tag, :string
    WorkListCategory.find_each do |work_list_category|
      work_list_category.work_lists.update_all(tag: work_list_category.tag)
    end
  end

  def down
    remove_column :work_lists, :tag
  end
end
