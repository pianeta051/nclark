class AddExportTermsToCustomerSearchQuery < ActiveRecord::Migration
  def change
    add_column :customer_search_queries, :export_terms, :json
  end
end
