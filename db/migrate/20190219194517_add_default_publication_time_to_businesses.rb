class AddDefaultPublicationTimeToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :default_publication_time, :time
  end
end
