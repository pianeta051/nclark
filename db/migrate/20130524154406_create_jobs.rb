class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :business_id, null: false
      t.integer :customer_id, null: false
      t.integer :week_id, null: false
      t.integer :cents, null: false
      t.date :date, null: false
      t.text :notes
      t.boolean :completed, default: false
      t.boolean :paid, default: false

      t.timestamps
    end
  end
end
