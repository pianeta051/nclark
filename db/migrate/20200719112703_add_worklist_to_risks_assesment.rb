class AddWorklistToRisksAssesment < ActiveRecord::Migration
  def change
    add_column :risks_assessments, :worklist_id, :integer
  end
end
