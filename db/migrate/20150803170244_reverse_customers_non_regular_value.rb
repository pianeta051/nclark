class ReverseCustomersNonRegularValue < ActiveRecord::Migration
  def up
    change_column :customers, :non_regular, :boolean, default: true
  end

  def down
    change_column :customers, :non_regular, :boolean, default: false
  end
end
