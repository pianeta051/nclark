class AddRoleToUsers < ActiveRecord::Migration
  def change
    unless column_exists? :users, :role 
      add_column :users, :role, :string, default: 'Cleaner'
    end

  end
end
