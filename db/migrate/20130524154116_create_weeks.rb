class CreateWeeks < ActiveRecord::Migration
  def change
    create_table :weeks do |t|
      t.integer :business_id, null: false
      t.text :notes

      t.timestamps
    end
  end
end
