class CreateDefaultCustomerDetails < ActiveRecord::Migration
  def change
    create_table :default_customer_details do |t|
      t.integer :business_id, null: false
      t.string :name, limit: 250
      t.boolean :large_field, default: false
      t.boolean :visible_in_index, default: false

      t.timestamps
    end
  end
end
