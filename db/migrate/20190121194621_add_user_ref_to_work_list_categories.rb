class AddUserRefToWorkListCategories < ActiveRecord::Migration
  def change
    add_reference :work_list_categories, :user, index: true
  end
end
