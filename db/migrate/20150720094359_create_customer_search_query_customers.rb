class CreateCustomerSearchQueryCustomers < ActiveRecord::Migration
  def change
    create_table :customer_search_query_customers do |t|
      t.integer :query_id, null: false
      t.integer :customer_id, null: false
      t.integer :order, null: false

      t.timestamps
    end
  end
end
