class MakeBookmarksPolymorphic < ActiveRecord::Migration
  def up
    rename_column :bookmarks, :customer_id, :bookmarkable_id
    add_column :bookmarks, :bookmarkable_type, :string
    Bookmark.update_all(bookmarkable_type: "Customer")
  end

  def down
    Bookmark.where("bookmarks.bookmarkable_type != ?", "Customer").destroy_all
    remove_column :bookmarks, :bookmarkable_type
    rename_column :bookmarks, :bookmarkable_id, :customer_id
  end
end
