class AddPaymentsCountToJobs < ActiveRecord::Migration
  def up
    add_column :jobs, :payments_count, :integer
    Job.find_each do |job|
      job.update_column(:payments_count, job.payments.count)
    end
  end

  def down
    remove_column :jobs, :payments_count
  end
end
