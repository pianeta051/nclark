class AddPublicationDateToWorkLists < ActiveRecord::Migration
  def change
    add_column :work_lists, :publication_date, :date
  end
end
