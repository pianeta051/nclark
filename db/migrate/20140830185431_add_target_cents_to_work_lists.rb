class AddTargetCentsToWorkLists < ActiveRecord::Migration
  def change
    add_column :work_lists, :target_cents, :integer, default: 0
  end
end
