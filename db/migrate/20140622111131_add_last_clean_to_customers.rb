class AddLastCleanToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :last_clean, :date
  end
end
