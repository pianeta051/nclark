class AddColorToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :color, :string, limit: 6
  end
end
