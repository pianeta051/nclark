class AddWorkListPaymentMethodsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :work_list_payment_methods, :string, default: "cash,cheque,credit,debit,bank transfer"
  end
end
