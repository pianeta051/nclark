class CreateContactDetails < ActiveRecord::Migration
  def change
    create_table :contact_details do |t|
      t.integer :customer_id, :null => false
      t.string :name, :limit => 40
      t.string :value, :limit => 40

      t.timestamps
    end
  end
end
