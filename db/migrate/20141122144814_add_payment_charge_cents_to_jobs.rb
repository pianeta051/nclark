class AddPaymentChargeCentsToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :payment_charge_cents, :integer, default: 0
  end
end
