class AddVisibleToWorkLists < ActiveRecord::Migration
  def change
    add_column :work_lists, :visible, :boolean, default: false
  end
end
