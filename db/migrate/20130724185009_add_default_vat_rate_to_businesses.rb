class AddDefaultVatRateToBusinesses < ActiveRecord::Migration
  def up
    add_column :businesses, :default_vat_rate, :decimal, precision: 10, scale: 2, default: 0.0
    Business.update_all(:default_vat_rate => DEFAULT_BUSINESS_DEFAULT_VAT_RATE)
  end

  def down
    remove_column :businesses, :default_vat_rate
  end
end
