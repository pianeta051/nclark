class CreateWorkListNotes < ActiveRecord::Migration
  def change
    create_table :work_list_notes do |t|
      t.string :work_list_id, null: false
      t.string :user_id
      t.text :content

      t.timestamps
    end
  end
end
