class TransferPaymentsFromJobsToPayments < ActiveRecord::Migration
  def up
    add_column :jobs, :amount_paid_cents, :integer, default: 0
    add_column :customers, :outstanding_money_cents, :integer, default: 0
    ActiveRecord::Base.connection.execute("
      INSERT INTO payments (customer_id, date, method, cents, charge_cents, created_at, updated_at) (
        SELECT jobs.customer_id, jobs.paid_at, jobs.payment_method, jobs.payment_amount_cents, jobs.payment_charge_cents, localtimestamp, localtimestamp
        FROM jobs
        WHERE jobs.paid_at IS NOT NULL
        AND jobs.payment_amount_cents > 0
      )
    ")
    Customer.find_each(&:calculate_job_payments!)
  end

  def down
    Payment.delete_all
    remove_column :jobs, :amount_paid_cents
    remove_column :customers, :outstanding_money_cents
  end
end
