class GeneratePaymentsWithZeroAmount < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("
      INSERT INTO payments (customer_id, date, method, cents, charge_cents, created_at, updated_at, job_id) (
        SELECT jobs.customer_id, jobs.paid_at, jobs.payment_method, jobs.payment_amount_cents, jobs.payment_charge_cents, localtimestamp, localtimestamp, jobs.id
        FROM jobs
        WHERE jobs.paid_at IS NOT NULL
        AND (
          jobs.payment_amount_cents = 0
          OR jobs.payment_amount_cents IS NULL
        )
      )
    ")
  end

  def down
    Payment.where(cents: 0).destroy_all
  end
end
