class AddWorkListIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :work_list_id, :integer
  end
end
