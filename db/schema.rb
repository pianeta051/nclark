# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140602192823) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: true do |t|
    t.integer  "business_id",            null: false
    t.string   "name",        limit: 40, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "color",       limit: 6
  end

  create_table "businesses", force: true do |t|
    t.string   "email",                                                          default: "",                                       null: false
    t.string   "encrypted_password",                                             default: "",                                       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                                  default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                                                                                                              null: false
    t.integer  "cycle_length",                                                   default: 4
    t.decimal  "default_vat_rate",                      precision: 10, scale: 2, default: 0.0
    t.string   "customer_color_field_name", limit: 100,                          default: "Colour"
    t.string   "work_list_payment_methods",                                      default: "cash,cheque,credit,debit,bank transfer"
  end

  add_index "businesses", ["confirmation_token"], name: "index_businesses_on_confirmation_token", unique: true, using: :btree
  add_index "businesses", ["email"], name: "index_businesses_on_email", unique: true, using: :btree
  add_index "businesses", ["reset_password_token"], name: "index_businesses_on_reset_password_token", unique: true, using: :btree

  create_table "customer_detail_categories", force: true do |t|
    t.integer  "business_id",             null: false
    t.string   "name",        limit: 250, null: false
    t.integer  "order",                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customer_details", force: true do |t|
    t.integer  "customer_id",                           null: false
    t.string   "name",          limit: 250
    t.text     "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id"
    t.text     "trimmed_value"
    t.integer  "group_index",               default: 0
  end

  add_index "customer_details", ["name", "customer_id"], name: "index_customer_details_on_name_and_customer_id", using: :btree

  create_table "customer_notes", force: true do |t|
    t.integer  "customer_id", null: false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "date"
    t.integer  "user_id"
  end

  create_table "customers", force: true do |t|
    t.integer  "business_id",                                    null: false
    t.integer  "area_id"
    t.integer  "regularity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "start_date"
    t.integer  "regular_price_cents",            default: 0
    t.string   "regularity_type",     limit: 6
    t.string   "county",              limit: 40
    t.boolean  "add_vat",                        default: false
    t.string   "color",               limit: 6
    t.boolean  "non_regular",                    default: false
  end

  add_index "customers", ["area_id"], name: "index_customers_on_area_id", using: :btree
  add_index "customers", ["business_id"], name: "index_customers_on_business_id", using: :btree

  create_table "default_customer_details", force: true do |t|
    t.integer  "business_id",                                             null: false
    t.string   "name",                        limit: 250
    t.boolean  "large_field",                             default: false
    t.boolean  "visible_in_index",                        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "singular",                                default: false
    t.integer  "order",                                                   null: false
    t.integer  "customer_detail_category_id",                             null: false
    t.boolean  "visible_in_work_list",                    default: false
    t.integer  "work_list_order",                                         null: false
  end

  add_index "default_customer_details", ["name", "business_id"], name: "index_default_customer_details_on_name_and_business_id", using: :btree

  create_table "job_notes", force: true do |t|
    t.integer  "job_id",     null: false
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.integer  "business_id",                                                               null: false
    t.integer  "customer_id",                                                               null: false
    t.integer  "week_id",                                                                   null: false
    t.integer  "cents",                                                                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "work_list_id"
    t.integer  "day",                                                       default: -1
    t.integer  "work_list_category_id"
    t.decimal  "vat_rate",                         precision: 10, scale: 2, default: 0.0
    t.date     "completed_at"
    t.date     "paid_at"
    t.string   "payment_method",        limit: 15
    t.integer  "payment_amount_cents"
    t.string   "invoice_number",        limit: 40
    t.time     "start_time"
    t.boolean  "ignored",                                                   default: false
    t.time     "finish_time"
  end

  create_table "users", force: true do |t|
    t.string   "name",            null: false
    t.string   "password_digest", null: false
    t.integer  "business_id",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "weeks", force: true do |t|
    t.integer  "business_id", null: false
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "start_date",  null: false
    t.string   "tag"
  end

  create_table "work_list_categories", force: true do |t|
    t.integer  "business_id",            null: false
    t.string   "name",        limit: 40, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tag"
  end

  create_table "work_list_notes", force: true do |t|
    t.string   "work_list_id", null: false
    t.string   "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "work_lists", force: true do |t|
    t.integer  "work_list_category_id",                 null: false
    t.integer  "week_id",                               null: false
    t.integer  "day",                                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "token_1"
    t.string   "token_2"
    t.string   "tag"
    t.boolean  "completed",             default: false
  end

end
