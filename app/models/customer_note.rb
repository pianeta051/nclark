# == Schema Information
#
# Table name: customer_notes
#
#  id          :integer          not null, primary key
#  customer_id :integer          not null
#  content     :text
#  created_at  :datetime
#  updated_at  :datetime
#  date        :datetime
#  user_id     :integer
#

class CustomerNote < ActiveRecord::Base

  attr_accessor :property_identifier, :property_hex_color

  belongs_to :customer
  belongs_to :user

  validates_presence_of :customer, :content, :date

end
