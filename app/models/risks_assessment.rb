# == Schema Information
#
# Table name: risks_assessments
#
#  id                        :integer          not null, primary key
#  yes                       :boolean
#  no                        :boolean
#  too_high                  :boolean
#  ground_not_safe           :boolean
#  no_clear_access_to_window :boolean
#  work_from_inside          :boolean
#  footed_ladder             :boolean
#  folded_ladder             :boolean
#  work_positioning_system   :boolean
#  read_and_wash             :boolean
#  secured_ladder            :boolean
#  harness                   :boolean
#  bar                       :boolean
#  other                     :string(255)
#  why                       :text
#  operative                 :string(255)
#  date                      :date
#  user_id                   :integer
#  customer_id               :integer
#  created_at                :datetime
#  updated_at                :datetime
#  not_all                   :boolean
#  worklist_id               :integer
#

class RisksAssessment < ActiveRecord::Base
  belongs_to :customer
  belongs_to :user
  has_many :users
  has_many :risksusers
  validate :check_yes_and_no

  def check_yes_and_no
    if (self.yes && self.no && self.not_all)||(self.yes==false && self.no ==false && self.not_all ==false)
      errors.add(:yes,'/No/Not_all cannot the same.')
    end
  end

  def user_in(user)
    RisksUser.where(user_id:user, risks_assessment_id:self.id, risk_cheacked:true)
  end

  def time_checked(user)
    RisksUser.find(user_in(user).pluck(:id))
  end

  def who_checked
    User.find(RisksUser.where(risks_assessment_id:self.id, risk_cheacked:true).pluck(:user_id))
  end

  def name_user
    User.find(self.operative)
  end

  def user_in_today(user_id)
    self.user_in(user_id).pluck(:created_at).last.strftime("%m/%d/%Y")
  end

  def accept(user_id)
    RisksUser.create(user_id: user_id, risks_assessment_id:self.id, date:Time.now.strftime("%d %b %Y %k:%M"), risk_cheacked: true)
  end

  def already_have?(user_id)
    if RisksUser.find_by(risks_assessment_id: self.id, user_id:user_id).date.strftime("%m/%d/%Y") == Date.today.strftime("%m/%d/%Y")
      return true
    else
      return false
    end
  end

end
