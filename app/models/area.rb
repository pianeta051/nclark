# == Schema Information
#
# Table name: areas
#
#  id          :integer          not null, primary key
#  business_id :integer          not null
#  name        :string(40)       not null
#  created_at  :datetime
#  updated_at  :datetime
#  color       :string(6)
#

class Area < ActiveRecord::Base

  belongs_to :business
  has_many :customers, dependent: :restrict_with_exception

  validates :name, presence: true, length: { maximum: 40 }, uniqueness: {scope: :business_id}

  after_save :update_customers_area_caches

  default_scope -> { order('areas.name ASC') }

private
  def update_customers_area_caches
    customers.update_all(area_color_cache: color)
    customers.update_all(area_name_cache: name)
  end

end
