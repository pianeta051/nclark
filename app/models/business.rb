# == Schema Information
#
# Table name: businesses
#
#  id                          :integer          not null, primary key
#  email                       :string(255)      default(""), not null
#  encrypted_password          :string(255)      default(""), not null
#  reset_password_token        :string(255)
#  reset_password_sent_at      :datetime
#  remember_created_at         :datetime
#  sign_in_count               :integer          default(0)
#  current_sign_in_at          :datetime
#  last_sign_in_at             :datetime
#  current_sign_in_ip          :string(255)
#  last_sign_in_ip             :string(255)
#  confirmation_token          :string(255)
#  confirmed_at                :datetime
#  confirmation_sent_at        :datetime
#  unconfirmed_email           :string(255)
#  created_at                  :datetime
#  updated_at                  :datetime
#  name                        :string(255)      not null
#  cycle_length                :integer          default(4)
#  default_vat_rate            :decimal(10, 2)   default(0.0)
#  customer_color_field_name   :string(100)      default("Colour")
#  work_list_payment_methods   :string(255)      default("cash,cheque,credit,debit,bank transfer")
#  credit_charge_cents         :integer          default(0)
#  debit_charge_cents          :integer          default(0)
#  credit_charge_is_percentage :boolean          default(FALSE)
#  debit_charge_is_percentage  :boolean          default(FALSE)
#  default_publication_time    :time

class Business < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :areas, dependent: :destroy
  has_many :customers, dependent: :destroy
  has_many :jobs, dependent: :destroy
  has_many :weeks, dependent: :destroy
  has_many :work_list_categories, dependent: :destroy
  has_many :work_lists, through: :work_list_categories
  has_many :default_customer_details, dependent: :destroy
  has_many :customer_detail_categories, dependent: :destroy
  has_many :users, dependent: :destroy
  has_many :expenses, dependent: :destroy
  has_many :payments, dependent: :destroy

  composed_of :credit_charge,
    class_name: "Money",
    mapping: [["credit_charge_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :debit_charge,
    class_name: "Money",
    mapping: [["debit_charge_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  validates :name, presence: true, length: { maximum: 250 }
  validates :customer_color_field_name, presence: true, length: { maximum: 250 }
  validates :cycle_length, numericality: { integer: true, greater_than: 0, less_than_or_equal_to: 10 }
  validates :default_vat_rate, numericality: { greater_than_or_equal_to: 0 }, unless: :new_record?

  before_create :set_default_vat_rate
  after_save :copy_nicks_settings!, if: proc { |l| l.confirmed_at_changed? && l.confirmed_at_was.nil? }
  before_destroy :destroy_all_dependent_records, prepend: true

  def payments
    Payment.where("payments.customer_id IN (
      SELECT customers.id
      FROM customers
      WHERE customers.business_id = ?
    )", id)
  end

  def default_vat_ratio
    (100.0 + default_vat_rate) / 100.0
  end

  def index_default_customer_details
    default_customer_details.visible_in_index
  end

  def work_list_default_customer_details
    default_customer_details.visible_in_work_list
  end

  def current_week
    @current_week ||= weeks.current.first
  end

  def work_list_payment_methods_array
    unless @work_list_payment_methods_array
      @work_list_payment_methods_array = work_list_payment_methods.split(',')
    end
    @work_list_payment_methods_array
  end

  def work_list_payment_methods_array=(array)
    array.select! { |el| Job::PAYMENT_METHODS.include?(el) }
    array.sort! { |a, b| Job::PAYMENT_METHODS.index(a) <=> Job::PAYMENT_METHODS.index(b) }
    self.work_list_payment_methods = array.join(',')
    @work_list_payment_methods_array = nil # invalidate cache
  end

  def amount_paid_on_date_cents(date)
    # jobs.where('jobs.paid_at = ?', date.to_date)
    #     .pluck(:payment_amount_cents)
    payments.where(date: date.to_date)
            .pluck(:cents)
            .inject(0) { |result, value| result + value }
  end

  def amount_paid_on_date(date)
    Money.new(amount_paid_on_date_cents(date), 'GBP')
  end

  def first_default_customer_detail
    if customer_detail_categories.count.zero?
      nil 
    else
      customer_detail_categories.first.default_customer_details.first
    end
  end

private
  def set_default_vat_rate
    self.default_vat_rate = DEFAULT_BUSINESS_DEFAULT_VAT_RATE
  end

  def copy_nicks_settings!
    if self.id == 6
      puts "Skipping Nick's business"
      return
    end

    reload

    self.default_customer_details.destroy_all
    self.customer_detail_categories.destroy_all

    other_business = Business.find(6)

    other_to_this_cdc = {}

    other_business.customer_detail_categories.each do |cdc|
      new_cdc = customer_detail_categories.create!(name: cdc.name, order: cdc.order)
      other_to_this_cdc[cdc.id] = new_cdc.id
    end

    other_business.default_customer_details.each do |dcd|
      default_customer_details.create!(name: dcd.name,
                                      large_field: dcd.large_field,
                                      visible_in_index: dcd.visible_in_index,
                                      singular: dcd.singular,
                                      order: dcd.order,
                                      customer_detail_category_id: other_to_this_cdc[dcd.customer_detail_category_id],
                                      visible_in_work_list: dcd.visible_in_work_list,
                                      work_list_order: dcd.work_list_order)
    end

    update_attributes(cycle_length: other_business.cycle_length, default_vat_rate: other_business.default_vat_rate)
  end

  def destroy_all_dependent_records
    jobs.destroy_all
    customers.destroy_all
    work_lists.destroy_all
  end
end
