# == Schema Information
#
# Table name: payments
#
#  id           :integer          not null, primary key
#  customer_id  :integer          not null
#  date         :date             not null
#  method       :string(255)
#  cents        :integer          default(0)
#  charge_cents :integer          default(0)
#  credit       :boolean          default(FALSE)
#  created_at   :datetime
#  updated_at   :datetime
#  job_id       :integer
#

class Payment < ActiveRecord::Base
  PAYMENT_METHODS = %w(cash cheque credit debit bank\ transfer post)

  attr_accessor :property_identifier

  belongs_to :customer
  belongs_to :customer_only_fetch_customer_details_cache, select: "customers.customer_details_cache", class_name: "Customer", foreign_key: "customer_id"
  has_many :job_payments, dependent: :destroy
  has_many :jobs, through: :job_payments

  composed_of :amount,
    class_name: "Money",
    mapping: [["cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :charge,
    class_name: "Money",
    mapping: [["charge_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  scope :credit, -> { where(credit: true) }
  scope :not_credit, -> { where(credit: false) }
  scope :by_date, lambda { |asc_desc| order("payments.date #{asc_desc.to_s.upcase}") }

  scope :in_date_range, lambda { |start_date, finish_date|
    begin
      start_date = Date.parse(start_date)
    rescue
      start_date = nil
    end
    begin
      finish_date = Date.parse(finish_date).to_date
    rescue
      finish_date = nil
    end
    if start_date.present? && finish_date.present?
      if start_date > finish_date
        temp = start_date
        start_date = finish_date
        finish_date = temp
      end
      where('payments.date >= ? AND payments.date <= ?', start_date, finish_date)
    elsif start_date.present?
      where('payments.date >= ?', start_date)
    elsif finish_date.present?
      where('payments.date <= ?', finish_date)
    else
      all
    end
  }

  validates_presence_of :date
  validates_inclusion_of :method, in: PAYMENT_METHODS, allow_blank: true
  validates_numericality_of :cents, greater_than: 0
  validates_numericality_of :charge_cents, greater_than_or_equal_to: 0

  before_save :set_payment_method_to_nil_if_credit
  after_save :set_payment_charge!, :calculate_job_payments_for_customer, :update_finances_cache_for_week
  after_destroy :calculate_job_payments_for_customer, :update_finances_cache_for_week

  def formatted_method
    if method.blank?
      'None'
    else
      method.humanize
    end
  end

  def job_payment(job)
    @cached_job_payments ||= {}
    job_id = job.is_a?(Job) ? job.id : job.to_i
    @cached_job_payments[job_id] ||= job_payments.where(job_id: job_id).first
  end

private
  def set_payment_charge!
    if method.blank? || date.blank?
      update_column(:charge_cents, 0)
    else
      charge_cents = nil
      if method == 'credit'
        charge_cents = customer.business.credit_charge_is_percentage? ? (cents * (customer.business.credit_charge_cents / 10000.0)) : customer.business.credit_charge_cents
      elsif method == 'debit'
        charge_cents = customer.business.debit_charge_is_percentage? ? (cents * (customer.business.debit_charge_cents / 10000.0)) : customer.business.debit_charge_cents
      end
      update_column(:charge_cents, charge_cents) unless charge_cents.nil?
    end
  end

  def set_payment_method_to_nil_if_credit
    self.method = nil if self.credit?
  end

  def calculate_job_payments_for_customer
    customer.calculate_job_payments!
  end

  def update_finances_cache_for_week
    Week.where(start_date: date.beginning_of_week).each(&:update_finances_cache!)
    return if job_payments.count.zero?
    Job.where(id: job_payments.pluck(:job_id)).each(&:update_finances_cache_for_week)
  end

end
