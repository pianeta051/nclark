# == Schema Information
#
# Table name: bookmarks
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  bookmarkable_id   :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#  bookmarkable_type :string(255)
#

class Bookmark < ActiveRecord::Base

  belongs_to :user
  belongs_to :bookmarkable, polymorphic: true

  scope :customer, -> { where(bookmarkable_type: "Customer") }

end
