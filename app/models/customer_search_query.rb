# == Schema Information
#
# Table name: customer_search_queries
#
#  id                      :integer          not null, primary key
#  user_id                 :integer          not null
#  search_type             :integer          not null
#  search_terms            :json
#  search_completed_at     :datetime
#  created_at              :datetime
#  updated_at              :datetime
#  xlsx_file               :string(255)
#  xlsx_export_started_at  :datetime
#  xlsx_export_finished_at :datetime
#  export_terms            :json
#

class CustomerSearchQuery < ActiveRecord::Base
  SEARCH_TYPE_CUSTOMERS = 0
  SEARCH_TYPE_JOBS = 1
  SEARCH_TYPE_COUNT = 2

  attr_accessor :search_type_string

  mount_uploader :xlsx_file, AttachmentUploader

  belongs_to :user
  has_many :customer_search_query_customers, foreign_key: :query_id
  has_many :customers, through: :customer_search_query_customers

  scope :completed, -> { where("customer_search_queries.search_completed_at IS NOT NULL") }
  scope :not_completed, -> { where(search_completed_at: nil) }
  scope :export_started, -> { where("customer_search_queries.xlsx_export_started_at IS NOT NULL") }
  scope :export_completed, -> { where("customer_search_queries.xlsx_export_finished_at IS NOT NULL") }
  scope :export_not_completed, -> { where(xlsx_export_finished_at: nil) }

  validates_presence_of :user, :search_type, :search_terms
  validates_numericality_of :search_type, greater_than_or_equal_to: 0, less_than: SEARCH_TYPE_COUNT
  validate :user_has_no_other_pending_searches_of_this_type

  before_validation :set_search_type_from_search_type_string
  before_destroy :delete_customer_search_query_customers

  def search_completed?
    search_completed_at?
  end

  def perform_search!
=begin
    search_terms example:
    {
      "search"=>{
        "0"=>{
          "0"=>{
            "field"=>"Address Line",
            "term"=>"Test"
          },
          "1"=>{
            "field"=>"County",
            "term"=>"Three"
          }
        },
        "1"=>{
          "0"=>{
            "field"=>"Postcode",
            "term"=>"WD"
          }
        }
      },
      "sort"=>{
        "0"=>{
          "field"=>"Area",
          "order"=>"ASC",
          "exclude_numbering"=>"false"
        },
        "1"=>{
          "field"=>"Address Line",
          "order"=>"DESC",
          "exclude_numbering"=>"true"
        }
      },
      "extras"=>{
        "show_bookmarked"=>"true",
        "show_unassigned_jobs"=>"true"
      }
    }
=end

    results = Customer.where(business_id: user.business.id)

    results = case search_terms['extras'].try(:[], 'active')
    when 'applied'
      results.active
    when 'unapplied'
      results.inactive
    else
      results
    end

    customer_details_names = user.business.default_customer_details.pluck(:name)

    (search_terms['search'] || {}).each do |_, terms|
      conditions = Customer.all
      terms.each do |_, value|
        field = value['field']
        term = value['term']

        conditions = if customer_details_names.include? field
          perform_search_on_customer_detail_values(conditions, term, field)
        elsif field == 'ALL_FIELDS'
          perform_search_on_all_fields(conditions, term)
        else
          case field.downcase
          when 'notes'
            perform_search_on_customer_notes_and_job_notes(conditions, term)
          when 'area'
            perform_search_on_area(conditions, term)
          else
            conditions
          end
        end
      end

      results = results.where(conditions.where_values.join(' OR '))
    end

    (search_terms['extras'] || {}).each do |key, value|
      next unless value == 'applied' || value == 'unapplied'
      results = case key
      when 'bookmarked'
        if value == 'applied'
          results.bookmarked_by(user)
        else
          results.unbookmarked_by(user)
        end
      when 'assigned'
        if value == 'applied'
          if user.business.current_week.nil?
            results
          else
            results.where(non_regular: false).where('customers.id IN (SELECT jobs.customer_id FROM jobs WHERE jobs.week_id = ? AND jobs.work_list_id IS NOT NULL)', user.business.current_week.id)
          end
        else
          results.where(non_regular: false).where('customers.id IN (SELECT jobs.customer_id FROM jobs WHERE jobs.week_id = ? AND jobs.work_list_id IS NULL)', user.current_week.id)
        end
      when 'owing'
        if value == 'applied'
          results.owing
        else
          results.not_owing
        end
      else
        results
      end
    end

    (search_terms['sort'] || {"0" => {"field" => "id", "order" => "ASC"}}).values.reverse.each do |value|
      field = value['field']
      order = value['order'].try(:downcase) == 'desc' ? 'DESC' : 'ASC'
      exclude_numbering = value['exclude_numbering'] == 'true'
      results = if customer_details_names.include? field
        perform_sort_on_customer_detail_value(results, field, order, exclude_numbering)
      else
        case field.downcase
        when 'area'
          perform_sort_on_area(results, order)
        when 'regular price'
          results.order("customers.regular_price_cents #{order}")
        when 'outstanding money'
          results.order("customers.outstanding_money_cents #{order}")
        when 'id'
          results.order('customers.id ASC')
        else
          results
        end
      end
    end

    sql = results.to_sql
    order_sql_index = sql.rindex("ORDER BY")
    order_sql = sql[order_sql_index..-1]
    full_sql = results.select("#{id}, customers.id, row_number() OVER (#{order_sql}) as rnum").to_sql
    order_sql_index = full_sql.rindex("ORDER BY")
    full_sql.slice!(order_sql_index..-1)
    ActiveRecord::Base.connection.execute("
      INSERT INTO customer_search_query_customers (query_id, customer_id, \"order\") (#{full_sql})
    ".tap { |s| puts s })
    update_column(:search_completed_at, Time.now)
    user.customer_search_queries.where('customer_search_queries.id != ?', id)
                                .where(search_type: search_type)
                                .destroy_all
  rescue Exception => e
    puts "Error: #{e}"
    user.customer_search_queries.where(search_type: search_type).destroy_all
  end

  def result_customers
    customers.order('customer_search_query_customers.order ASC')
  end

  def remove_customer_from_results(customer)
    customer_search_query_customers.where(customer_id: customer.id).delete_all > 0
  end

  def start_exporting_xlsx!(terms)
    reset_export!
    self.update_column(:xlsx_export_started_at, Time.now)
    self.update_attribute(:export_terms, terms)
    self.delay.export_xlsx!
  end

  def xlsx_export_pending?
    xlsx_export_started_at? && !xlsx_export_finished_at?
  end

  def xlsx_export_finished?
    xlsx_export_finished_at? && xlsx_file.present? && xlsx_file.file.exists?
  end

private
  def perform_search_on_customer_detail_values(results, term, fields)
    results.search_by_customer_detail_value(term, fields)
  end

  def perform_search_on_customer_notes_and_job_notes(results, term)
    results.where('customers.id IN (
                    SELECT customer_notes.customer_id
                    FROM customer_notes
                    WHERE customer_notes.content ILIKE ?
                  )', "%#{term}%")
           .where('customers.id IN (
                    SELECT jobs.customer_id
                    FROM jobs
                    WHERE jobs.id IN (
                      SELECT job_notes.job_id
                      FROM job_notes
                      WHERE job_notes.content ILIKE ?
                    )
                  )', "#{term}")
  end

  def perform_search_on_area(results, term)
    if term[/[^1234567890]/]
      results.where("customers.area_id IN (
        SELECT areas.id
        FROM areas
        WHERE areas.business_id = ?
        AND areas.name ILIKE ?
      )", user.business.id, "%#{term}%")
    else
      results.where('customers.area_id = ?', term)
    end
  end

  def perform_search_on_all_fields(results, term)
    results = perform_search_on_customer_detail_values(results, term, nil)
    results = perform_search_on_customer_notes_and_job_notes(results, term)
    perform_search_on_area(results, term)
  end

  def perform_sort_on_customer_detail_value(results, field, order, exclude_numbering)
    results.order("
      (
        SELECT customer_details.#{exclude_numbering ? 'trimmed_' : ''}value
        FROM customer_details
        WHERE customer_details.name = '#{field}'
          AND customer_details.customer_id = customers.id
        LIMIT 1
      ) #{order} NULLS LAST
    ")
  end

  def perform_sort_on_area(results, order)
    results.order("
      (
        SELECT areas.name
        FROM areas
        WHERE areas.id = customers.area_id
      ) #{order}
    ")
  end

  def user_has_no_other_pending_searches_of_this_type
    if user.has_search_pending?(search_type)
      errors.add(:base, "User must have no other searches pending")
    end
  end

  def set_search_type_from_search_type_string
    self.search_type = CustomerSearchQuery::get_search_type(search_type_string) if search_type_string.present?
  end

  def delete_customer_search_query_customers
    ActiveRecord::Base.connection.execute("
      DELETE
      FROM customer_search_query_customers
      WHERE customer_search_query_customers.query_id = #{id}
    ")
  end

  def reset_export!
    self.update_attributes(xlsx_export_started_at: nil, xlsx_export_finished_at: nil, remove_xlsx_file: true)
  end

  def export_xlsx!
    self.export_terms = {} if self.export_terms.nil?
    export_customer_details = self.export_terms['export_customer_details'] || {}
    export_other_details = self.export_terms['export_other_details'] || {}

    customer_details_names = export_customer_details.map { |k, v| v == '1' ? k : nil }.compact
    other_details_names = export_other_details.map { |k, v| v == '1' ? k : nil }.compact
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: "Customers") do |sheet|
        sheet.add_row(customer_details_names + other_details_names)
        result_customers.each do |customer|
          row_array = customer.index_detail_values_from_cache(customer_details_names)
          other_details_names.each do |name|
            row_array << case name.downcase
            when 'area'
              customer.area_name_cache
            when 'regular price'
              customer.regular_price
            when 'last clean'
              customer.last_clean_string
            when 'amount owing'
              customer.outstanding_money
            when 'id'
              customer.id
            else
              ''
            end
          end
          sheet.add_row(row_array)
        end
      end
      file = Tempfile.new(["#{user.business.id}-#{user.id}-#{self.id}", ".xlsx"], "#{Rails.root}/tmp")
      file.write(p.to_stream.read)
      file.close
      self.update_attribute(:xlsx_file, file)
    end
    self.update_column(:xlsx_export_finished_at, Time.now)
  rescue Exception => e
    puts "Error: #{e}"
    reset_export!
  end

  def self.get_search_type(str)
    if str['weeks']
      SEARCH_TYPE_JOBS
    else
      SEARCH_TYPE_CUSTOMERS
    end
  end

end
