# == Schema Information
#
# Table name: weeks
#
#  id             :integer          not null, primary key
#  business_id    :integer          not null
#  notes          :text
#  created_at     :datetime
#  updated_at     :datetime
#  start_date     :date             not null
#  tag            :string(255)
#  finances_cache :hstore
#

class Week < ActiveRecord::Base

  belongs_to :business
  has_many :jobs, dependent: :restrict_with_exception
  has_many :work_lists, dependent: :restrict_with_exception

  validates_presence_of :start_date
  validates_length_of :tag, maximum: 250
  validate :start_date_within_reasonable_limits

  scope :current, -> { where(start_date: Date.today.beginning_of_week) }

  scope :past, ->{where("weeks.start_date <= ?", Date.today.beginning_of_week)}

  scope :in_date_range, lambda { |start_date, finish_date|
    begin
      start_date = Date.parse(start_date).beginning_of_week.to_date
    rescue
      start_date = nil
    end
    begin
      finish_date = Date.parse(finish_date).beginning_of_week.to_date
    rescue
      finish_date = nil
    end
    if start_date.present? && finish_date.present?
      where('weeks.start_date >= ? AND weeks.start_date <= ?', start_date, finish_date)
    elsif start_date.present?
      where('weeks.start_date >= ?', start_date)
    elsif finish_date.present?
      where('weeks.start_date <= ?', finish_date)
    else
      all
    end
  }

  def next_week_start_date
    (start_date + 1.week).beginning_of_week.to_date
  end

  def previous_week_start_date
    (start_date - 1.week).beginning_of_week.to_date
  end

  def reset_jobs_to_another_week(other_week)
    jobs.not_completed.update_all(work_list_id: nil, work_list_category_id: nil, day: nil, week_id: other_week.id)
  end

  def value_cents
    set_value_cents_and_payment_amount_cents unless @value_cents
    @value_cents
  end

  def value
    Money.new(value_cents, 'GBP')
  end

  def value_cents_for_day(day)
    set_value_cents_and_payment_amount_cents_per_day unless @value_cents_per_day
    @value_cents_per_day[day]
  end

  def value_for_day(day)
    Money.new(value_cents_for_day(day), 'GBP')
  end

  def completed_value_cents
    set_value_cents_and_payment_amount_cents unless @completed_value_cents
    @completed_value_cents
  end

  def completed_value
    Money.new(completed_value_cents, 'GBP')
  end

  def outstanding_value_cents
    set_value_cents_and_payment_amount_cents unless @outstanding_value_cents
    @outstanding_value_cents
  end

  def outstanding_value
    Money.new(outstanding_value_cents, 'GBP')
  end

  def outstanding_value_cents_for_day(day)
    set_value_cents_and_payment_amount_cents_per_day unless @outstanding_value_cents_per_day
    @outstanding_value_cents_per_day[day]
  end

  def outstanding_value_for_day(day)
    Money.new(outstanding_value_cents_for_day(day), 'GBP')
  end

  def completed_value_cents_for_day(day)
    set_value_cents_and_payment_amount_cents_per_day unless @completed_value_cents_per_day
    @completed_value_cents_per_day[day]
  end

  def completed_value_for_day(day)
    Money.new(completed_value_cents_for_day(day), 'GBP')
  end

  def paid_value_cents
    set_value_cents_and_payment_amount_cents unless @paid_value_cents
    @paid_value_cents
  end

  def paid_value
    Money.new(paid_value_cents, 'GBP')
  end

  def paid_value_cents_for_day(day)
    set_value_cents_and_payment_amount_cents_per_day unless @paid_value_cents_per_day
    @paid_value_cents_per_day[day]
  end

  def paid_value_for_day(day)
    Money.new(paid_value_cents_for_day(day), 'GBP')
  end

  def amount_paid_cents
    @amount_paid_cents ||= finances_cache.try(:[], "amount_paid_cents").try(:to_i)
    if @amount_paid_cents.nil?
      update_finances_cache!
      @amount_paid_cents = finances_cache["amount_paid_cents"].to_i
    end
    @amount_paid_cents
  end

  def amount_paid
    Money.new(amount_paid_cents, 'GBP')
  end

  def jobs_count
    @jobs_count ||= finances_cache.try(:[], "jobs_count").try(:to_i)
    if @jobs_count.nil?
      update_finances_cache!
      @jobs_count = finances_cache["jobs_count"].to_i
    end
    @jobs_count
  end

  def previous_week
    @previous_week ||= business.weeks.where(start_date: (start_date - 1.week).to_date).first
  end

  def next_week
    @next_week ||= business.weeks.where(start_date: (start_date + 1.week).to_date).first
  end

  def create_work_lists(business)
    unless new_record?
      business.work_list_categories.each do |work_list_category|
        7.times do |i|
          if work_list_category.work_lists.where(week_id: self.id, day: i).first.nil?
            wl = work_list_category.work_lists.create(week: self, day: i, tag: work_list_category.tag, target_cents: work_list_category.target_cents, publication_time: business.default_publication_time)
            if wl.date.monday?
              wl.update(publication_date: wl.date - 3.days)
            else
              wl.update(publication_date: wl.date-1.days)
            end
          end
        end
      end
    end
  end

  def update_finances_cache!
    update_finances_cache_internal!
  end

private
  def set_value_cents_and_payment_amount_cents(skip_cache_on_fail = false)
    if finances_cache.blank?
      update_finances_cache!
    end
    @value_cents = finances_cache["value_cents"].to_i
    @completed_value_cents = finances_cache["completed_value_cents"].to_i
    @outstanding_value_cents = finances_cache["outstanding_value_cents"].to_i
    @paid_value_cents = finances_cache["paid_value_cents"].to_i
    if (@value_cents || @completed_value_cents || @outstanding_value_cents || @paid_value_cents).nil? && !skip_cache_on_fail
      update_finances_cache!
      set_value_cents_and_payment_amount_cents(true)
    end
  end

  def set_value_cents_and_payment_amount_cents_per_day(skip_cache_on_fail = false)
    if finances_cache.blank?
      update_finances_cache!
    end
    
    @value_cents_per_day = arr_str_to_arr(finances_cache["value_cents_per_day"])
    @completed_value_cents_per_day = arr_str_to_arr(finances_cache["completed_value_cents_per_day"])
    @outstanding_value_cents_per_day = arr_str_to_arr(finances_cache["outstanding_value_cents_per_day"])
    @paid_value_cents_per_day = arr_str_to_arr(finances_cache["paid_value_cents_per_day"])
    if (@value_cents_per_day || @completed_value_cents_per_day || @outstanding_value_cents_per_day || @paid_value_cents_per_day).nil? && !skip_cache_on_fail
      update_finances_cache!
      set_value_cents_and_payment_amount_cents_per_day(true)
    end
  end

  def update_finances_cache_internal!
    value_cents_per_day = (1..7).map { 0 }
    completed_value_cents_per_day = (1..7).map { 0 }
    outstanding_value_cents_per_day = (1..7).map { 0 }
    paid_value_cents_per_day = (1..7).map { 0 }

    value_cents, completed_value_cents, outstanding_value_cents, paid_value_cents = jobs.unlinked.includes(:payments).inject([0, 0, 0, 0]) do |result, job|
      unless (job.day || -1) < 0
        value_cents_per_day[job.day] += job.cents || 0
        completed_value_cents_per_day[job.day] += (job.completed_at? ? job.cents : nil) || 0
        outstanding_value_cents_per_day[job.day] += (job.completed_at? && !job.paid? ? job.cents : 0)
        paid_value_cents_per_day[job.day] += (job.completed_at? && job.paid? ? job.cents : nil) || 0
      end

      [
        result[0] + (job.cents || 0),
        result[1] + ((job.completed_at? ? job.cents : nil) || 0),
        result[2] + ((job.completed_at? && !job.paid? ? job.cents : nil) || 0),
        result[3] + ((job.paid? && job.completed_at? ? job.cents : nil) || 0)
      ]
    end

    amount_paid_cents = business.payments
                                .where('payments.date >= ?', start_date)
                                .where('payments.date <= ?', start_date.end_of_week)
                                .pluck(:cents, :charge_cents)
                                .inject(0) { |result, value| result + value[0] + value[1] }

    jobs_count = jobs.unlinked.count

    self.update_attribute(:finances_cache, {
      value_cents: value_cents,
      completed_value_cents: completed_value_cents,
      outstanding_value_cents: outstanding_value_cents,
      paid_value_cents: paid_value_cents,
      value_cents_per_day: value_cents_per_day,
      completed_value_cents_per_day: completed_value_cents_per_day,
      outstanding_value_cents_per_day: outstanding_value_cents_per_day,
      paid_value_cents_per_day: paid_value_cents_per_day,
      amount_paid_cents: amount_paid_cents,
      jobs_count: jobs_count
    })
  end

  def start_date_within_reasonable_limits
    if new_record?
      if start_date < 5.years.ago.beginning_of_week
        errors.add(:start_date, "must be after 5 years ago")
      elsif start_date > 2.years.since.beginning_of_week
        errors.add(:start_date, "must be before 2 years in the future")
      end
    end
  end

  def arr_str_to_arr(arr)
    if arr.is_a? String
      arr[1..-2].split(',').collect { |n| n.to_i }
    else
      arr
    end
  end
end
