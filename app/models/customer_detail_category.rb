# == Schema Information
#
# Table name: customer_detail_categories
#
#  id          :integer          not null, primary key
#  business_id :integer          not null
#  name        :string(250)      not null
#  order       :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#

class CustomerDetailCategory < ActiveRecord::Base

  belongs_to :business
  has_many :default_customer_details, dependent: :nullify

  validates_presence_of :business, :name, :order
  validates_length_of :name, maximum: 250
  validate :name_is_unique

  default_scope -> { order('customer_detail_categories.order ASC') }

private
  def name_is_unique
    if business
      existing_detail_categories = business.customer_detail_categories.where(:name => self.name)
      unless new_record?
        existing_detail_categories = existing_detail_categories.where("customer_detail_categories.id != ?", self.id)
      end
      if existing_detail_categories.count > 0
        errors.add(:name, "must be unique")
      end
    end
  end

end
