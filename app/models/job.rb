# == Schema Information
#
# Table name: jobs
#
#  id                    :integer          not null, primary key
#  business_id           :integer          not null
#  customer_id           :integer          not null
#  week_id               :integer          not null
#  cents                 :integer          not null
#  created_at            :datetime
#  updated_at            :datetime
#  work_list_id          :integer
#  day                   :integer          default(-1)
#  work_list_category_id :integer
#  vat_rate              :decimal(10, 2)   default(0.0)
#  completed_at          :date
#  paid_at               :date
#  payment_method        :string(15)
#  payment_amount_cents  :integer
#  invoice_number        :string(40)
#  start_time            :time
#  finish_time           :time
#  payment_charge_cents  :integer          default(0)
#  work_list_order       :integer
#  amount_paid_cents     :integer          default(0)
#  linked_job_id         :integer
#  child_jobs_count      :integer          default(0)
#  linked_job_day        :integer          default(1)
#  completed_cache       :boolean          default(FALSE)
#  payments_count        :integer
#  check_risk            :boolean          default(FALSE)
#

class Job < ActiveRecord::Base

  attr_accessor :property_identifier

  belongs_to :business
  belongs_to :customer, counter_cache: true
  belongs_to :week
  belongs_to :work_list
  belongs_to :work_list_category
  belongs_to :linked_job, class_name: 'Job', counter_cache: :child_jobs_count
  has_many :child_jobs, class_name: 'Job', foreign_key: :linked_job_id, dependent: :destroy
  has_many :job_notes, dependent: :destroy
  has_many :job_payments, dependent: :destroy
  has_many :payments, through: :job_payments

  composed_of :price,
    class_name: "Money",
    mapping: [["cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :payment_amount,
    class_name: "Money",
    mapping: [["payment_amount_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :amount_paid,
    class_name: "Money",
    mapping: [["amount_paid_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  scope :by_date, lambda { |asc_desc|
    order("jobs.day #{asc_desc.to_s.upcase}").order("(SELECT weeks.start_date FROM weeks WHERE weeks.id = jobs.week_id) #{asc_desc.to_s.upcase}")
  }

  scope :completed, -> { where(completed_cache: true) }
  scope :not_completed, -> { where('jobs.completed_at IS NULL') }
  scope :by_completed_at, lambda { |asc_desc|
    completed.order("jobs.completed_at #{asc_desc.to_s.upcase}")
  }
  scope :paid, -> { where('jobs.id IN (SELECT job_payments.job_id FROM job_payments WHERE job_payments.job_id IS NOT NULL) OR jobs.cents = 0') }
  scope :not_paid, -> { where('jobs.id NOT IN (SELECT job_payments.job_id FROM job_payments WHERE job_payments.job_id IS NOT NULL) AND jobs.cents > 0') }
  scope :work_list_order, -> { order('jobs.work_list_order ASC NULLS LAST, jobs.start_time ASC NULLS LAST, jobs.created_at ASC') }
  scope :time_order, -> { order('jobs.start_time ASC NULLS LAST, jobs.created_at ASC') }
  scope :not_already_in_week, lambda { |week|
    where('jobs.customer_id NOT IN (SELECT jobs.customer_id FROM jobs WHERE jobs.week_id = ?)', week.id)
  }
  scope :untouched, -> { not_completed.not_paid.where('jobs.work_list_id IS NULL OR jobs.day = ?', -1) }
  scope :after_week, lambda { |week|
    where('(SELECT weeks.start_date FROM weeks WHERE weeks.id = jobs.week_id) > ?', week.start_date)
  }
  scope :before_week, lambda { |week|
    where('(SELECT weeks.start_date FROM weeks WHERE weeks.id = jobs.week_id) < ?', week.start_date)
  }
  scope :assigned, -> { where('jobs.work_list_id IS NOT NULL') }
  scope :unassigned, -> { where('jobs.work_list_id IS NULL') }
  scope :linked, -> { where('jobs.linked_job_id IS NOT NULL') }
  scope :unlinked, -> { where('jobs.linked_job_id IS NULL') }
  scope :by_linked_job_day, -> { order('jobs.linked_job_day ASC') }

  scope :not_expired, -> {
    where('(SELECT weeks.start_date FROM weeks WHERE weeks.id = jobs.week_id) > ?', 600.days.ago)
  }

  validates_presence_of :business, :customer, :week, :cents
  validates_numericality_of :cents, integer: true, greater_than_or_equal_to: 0
  validates_numericality_of :payment_amount_cents, integer: true, greater_than_or_equal_to: 0, allow_blank: true
  validates :vat_rate, numericality: { greater_than_or_equal_to: 0 }
  validates_length_of :invoice_number, maximum: 40
  validates_numericality_of :day, only_integer: true, greater_than_or_equal_to: -1, less_than: 7
  validate :start_time_if_finish_time_is_present

  after_create :generate_invoice_number_if_blank
  before_validation :set_week_id_and_day_from_date_if_nil
  before_save :update_work_list_id
  before_save :nullify_work_list_order_if_no_work_list_or_work_list_changed
  before_save :update_completed_cache_for_self
  after_save :update_customer_last_clean
  after_save :calculate_job_payments_for_customer
  after_save :update_finances_cache_for_week
  after_save :update_work_list_if_work_list_id_changed_or_cents_changed_or_completed_at_changed
  after_save :update_child_jobs_with_same_attributes
  after_save :update_completed_cache_for_linked_job
  after_destroy :calculate_job_payments_for_customer, :update_work_list_for_destroy, :update_finances_cache_for_week

  def linked?
    self.linked_job_id.present?
  end

  def total_payment_amount
    Money.new(total_payment_amount_cents, 'GBP')
  end

  def total_payment_amount_cents
    @total_payment_amount_cents ||= job_payments.pluck(:cents).inject(0, &:+)
  end

  def total_payment_date
    @total_payment_date ||= payments.by_date(:asc).pluck(:date).map { |d| d.strftime("%d/%m/%Y") }.join(', ')
  end

  def total_payment_method
    @total_payment_method ||= begin
      str = payments.by_date(:asc).pluck(:method).map { |m| m.blank? ? 'none' : m }.join(', ')
      if str.blank?
        if cents.zero?
          'free'
        else
          'none'
        end
      else
        str
      end
    end
  end

  def day_set?
    !day.nil? && day > -1 && day < 7
  end

  def date
    @date ||= day_set? ? week.start_date + day.days : nil
  end

  def date=(new_date)
    return if new_date.blank?
    @date = new_date.to_date
    if week.nil? || day.nil? || week.start_date + day.days != @date
      self.week_id = nil
      self.day = nil
    end
  end

  def mark_as_completed!
    update_attribute(:completed_at, Date.today)
  end

  def unmark_as_completed!
    update_attribute(:completed_at, nil)
  end

  def mark_as_paid!(date = nil)
    update_attribute(:paid_at, date || Date.today)
  end

  def mark_as_risk_checked!
    update_attribute(:check_risk, true)
  end

  def unmark_as_risk_checked!
    update_attribute(:completed_at, nil)
  end


  def completed?
    completed_at.present?
  end

  def risk_checked?
    check_risk.present?
  end

  def paid?
    linked? ? linked_job.paid? : (payments_count > 0 || cents.zero?)
  end

  alias_method :ar_payments, :payments
  def payments
    linked? ? linked_job.payments : ar_payments
  end

  def formatted_time
    if start_time.present?
      str = start_time.strftime("%H:%M")
      if finish_time.present?
        str << " - "
        str << finish_time.strftime("%H:%M")
      end
      str
    else
      'N/A'
    end
  end

  def price_including_vat
    price * vat_ratio
  end

  def vat(use_business_vat_rate_if_zero = false, vat_rate_override = nil)
    if use_business_vat_rate_if_zero && vat_rate.zero?
      price - (price / ((100.0 + (vat_rate_override || business.default_vat_rate)) / 100.0))
    else
      price - (price / ((100.0 + vat_rate) / 100.0))
    end
  end

  def price_excluding_vat(use_business_vat_rate_if_zero = false, vat_rate_override = nil)
    if use_business_vat_rate_if_zero && vat_rate.zero?
      price - vat(true, vat_rate_override)
    else
      price
    end
  end

  def assign_attributes_from_customer(existing_customer, business)
    self.cents = existing_customer.try(:regular_price_cents) || 0
    self.vat_rate = existing_customer.try(:add_vat?) ? business.default_vat_rate : 0.0
  end

  def update_cents_excluding_vat(cents)
    if vat_rate.zero?
      self.cents = cents * ((100.0 + business.default_vat_rate) / 100.0)
    else
      self.cents = cents
    end
    self.save
  end

  def update_cents_including_vat(cents)
    if vat_rate.zero?
      self.cents = cents
    else
      self.cents = cents * ((100.0 - vat_rate) / 100.0)
    end
    self.save
  end

  def update_payments_count!
    update_column(:payments_count, payments.count)
  end

  def update_finances_cache_for_week
    week.try(:update_finances_cache!)
  end

  def have_accepted_any_risks_assessment?(user_id)
    self.customer.risks_assessments.each do |risk_assessment|
      unless RisksUser.where(risks_assessment_id: risk_assessment, user_id:user_id).empty?
        RisksUser.find_by(risks_assessment_id: risk_assessment, user_id:user_id)
        break
      end
    end
  end

  def user_in_risk_assessment(risk_assessment_id, user_id)
    RisksUser.where(risks_assessment_id: risk_assessment_id, user_id: user_id).empty?
  end

private
  def vat_ratio
    (100.0 + vat_rate) / 100.0
  end

  def generate_invoice_number_if_blank
    if self.invoice_number.blank?
      str = linked? ? linked_job.invoice_number : "#{id}#{created_at.strftime('%S')}"
      self.update_column(:invoice_number, str)
    end
  end

  def update_work_list_id
    @previous_work_list ||= work_list
    @cents_changed = cents_changed?
    @completed_at_changed = completed_at_changed?

    if day_set? && work_list_category.present?
      new_work_list = business.work_lists.where(work_list_category_id: work_list_category_id, week_id: week_id, day: day).first
      return unless new_work_list
      self.work_list = new_work_list
    else
      self.work_list = nil
    end
    @date = nil
  end

  def set_week_id_and_day_from_date_if_nil
    if (week_id.nil? || day.nil?) && @date.present?
      start_date = @date.beginning_of_week.to_date

      if start_date < 5.years.ago.beginning_of_week || start_date > 2.years.since.beginning_of_week
        errors.add(:date, "must be between 5 years ago and 2 years in the future")
        self.day = -1
        return
      end

      self.week = business.weeks.where(start_date: start_date).first
      if self.week.nil?
        self.week = business.weeks.create(start_date: start_date)
        self.week.create_work_lists(business)
      end
      self.day = (@date - start_date).to_i
      @previous_work_list = work_list
      self.work_list = nil
    elsif day.nil? && week_id.present?
      self.day = -1
    end
  end

  def start_time_if_finish_time_is_present
    if start_time.blank? && finish_time.present?
      errors.add(:start_time, "must be present if finish time is present")
    end
  end

  def update_customer_last_clean
    customer.update_last_clean
  end

  def calculate_job_payments_for_customer
    customer.calculate_job_payments!
  end

  def nullify_work_list_order_if_no_work_list_or_work_list_changed
    if work_list.nil? || work_list_id_changed?
      self.work_list_order = nil
      @work_list_id_changed = true
    end
  end

  def update_work_list_if_work_list_id_changed_or_cents_changed_or_completed_at_changed
    if @work_list_id_changed
      unless self.work_list.nil?
        self.work_list.update_job_ordering(self)
        self.work_list.update_job_caches!
      end
      @previous_work_list.update_job_caches! unless @previous_work_list.nil?
    elsif (@cents_changed || @completed_at_changed) && !self.work_list.nil?
      self.work_list.update_job_caches!
    end
  end

  def update_work_list_for_destroy
    self.work_list.update_job_caches! unless self.work_list.nil?
  end

  def update_child_jobs_with_same_attributes
    child_jobs.update_all(cents: cents, vat_rate: vat_rate, invoice_number: invoice_number)
  end

  def update_completed_cache_for_self
    if child_jobs_count > 0
      self.completed_cache = self.completed_at? && child_jobs.pluck(:completed_at).all?
    else
      self.completed_cache = self.completed_at?
    end
    nil
  end

  def update_completed_cache_for_linked_job
    if linked?
      linked_job.update_column(:completed_cache, linked_job.completed_at? && linked_job.child_jobs.pluck(:completed_at).all?)
    end
  end

end
