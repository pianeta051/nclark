# == Schema Information
#
# Table name: customer_details
#
#  id            :integer          not null, primary key
#  customer_id   :integer          not null
#  name          :string(250)
#  value         :text
#  created_at    :datetime
#  updated_at    :datetime
#  parent_id     :integer
#  trimmed_value :text
#  group_index   :integer          default(0)
#

class CustomerDetail < ActiveRecord::Base

  CACHE_VALUE_SEPARATOR = '|`|'
  CACHE_VALUE_SEPARATOR_REGEX = /\|`\|/

  attr_accessor :default, :large_field, :extra_detail, :singular, :order, :default_customer_detail

  belongs_to :customer
  belongs_to :parent, class_name: 'CustomerDetail'
  has_many :children, class_name: 'CustomerDetail', foreign_key: 'parent_id'

  validates_presence_of :customer, unless: :new_record?
  validates_presence_of :name, unless: :parent_present?
  validates_presence_of :value
  validates_length_of :name, maximum: 40
  validate :name_is_unique, if: :extra_detail
  validates_format_of :value, without: CACHE_VALUE_SEPARATOR_REGEX

  before_save :set_trimmed_value
  after_destroy :reassign_children

  scope :without_parents, -> { where(parent_id: nil) }
  scope :from_business, lambda { |business_id|
    where('customer_details.customer_id IN (SELECT customers.id FROM customers WHERE customers.business_id = ?)', business_id)
  }

  def parent_present?
    parent.present?
  end

  def business_default_customer_detail
    if customer
      customer.business.default_customer_details.where(name: name).first
    elsif new_record?
      default_customer_detail
    else
      nil
    end
  end

private
  def name_is_unique
    if customer
      existing_details = customer.customer_details.where(:name => self.name)
      unless new_record?
        existing_details = existing_details.where("customer_details.id != ?", self.id)
      end
      if existing_details.count > 0
        errors.add(:name, "must be unique")
      end
    end
  end

  def set_trimmed_value
    self.trimmed_value = value[/[a-zA-Z].*/]
  end

  def reassign_children
    first_child = children.first
    if first_child
      children.where(id: first_child.id).update_all(parent_id: nil, name: name)
      children.where('customer_details.id != ?', first_child.id).update_all(parent_id: first_child.id)
    end
  end

end
