# == Schema Information
#
# Table name: attachments
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  attachment      :string(255)
#  attachable_id   :integer
#  created_at      :datetime
#  updated_at      :datetime
#  attachable_type :string(255)
#

class Attachment < ActiveRecord::Base

  mount_uploader :attachment, AttachmentUploader

  belongs_to :attachable, polymorphic: true

  validates :name, length: { maximum: 250 }
  validates_presence_of :attachment

  def real_name
    name.present? ? name : attachment.file.try(:filename)
  end

end
