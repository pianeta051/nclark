# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)      not null
#  password_digest :string(255)      not null
#  business_id     :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#  role            :string(255)      default("Super_Admin")
#

  # == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)      not null
#  password_digest :string(255)      not null
#  business_id     :integer          not null
#  role            :string           default ('Team_leader')
#  created_at      :datetime
#  updated_at      :datetime
#  Add news def    : admin? admin!, Super_Admin?, Super_Admin!, team_leader?, team_leader!, cleaner?, cleaner!

class User < ActiveRecord::Base

  has_secure_password

  belongs_to :business
  has_many :work_list_categories, dependent: :nullify
  has_many :customer_notes, dependent: :nullify
  has_many :job_notes, dependent: :nullify
  has_many :work_list_notes, dependent: :nullify
  has_many :bookmarks, dependent: :destroy
  has_many :customers, through: :bookmarks
  has_many :customer_search_queries, dependent: :destroy
  has_many :finance_exports, dependent: :destroy
  has_many :work_list_workers, dependent: :destroy
  has_many :work_lists, through: :work_list_workers
  has_many :risks_assessments
  validates :name, presence: true, length: {maximum: 255}, uniqueness: true

  after_save :update_job_notes_user_name_cache

  def has_search_pending?(type = nil)
    type = CustomerSearchQuery::get_search_type(type) if type.is_a? String
    customer_search_queries.not_completed
                           .where(search_type: [type || (0...CustomerSearchQuery::SEARCH_TYPE_COUNT).to_a].flatten)
                           .count > 0
  end

  def has_export_pending?(type = nil)
    type = CustomerSearchQuery::get_search_type(type) if type.is_a? String
    customer_search_queries.export_started
                           .export_not_completed
                           .where(search_type: [type || (0...CustomerSearchQuery::SEARCH_TYPE_COUNT).to_a].flatten)
                           .count > 0
  end

  def has_finances_export_pending?
    finance_exports.not_completed.count > 0
  end

  def completed_search(type)
    type = CustomerSearchQuery::get_search_type(type) if type.is_a? String
    @completed_search ||= {}
    @completed_search[type] ||= customer_search_queries.completed.where(search_type: type).first
  end

  def compeleted_search_export(type)
    type = CustomerSearchQuery::get_search_type(type) if type.is_a? String
    @compeleted_search_export ||= {}
    @compeleted_search_export[type] ||= customer_search_queries.export_started.export_completed.where(search_type: type).first
  end

  def completed_finance_export
    @completed_finance_export ||= finance_exports.completed.first
  end

  def super_admin?
    role == 'Super_Admin'
  end

  def admin?
    role == 'Admin'
  end

  def team_leader?
    role == 'Team_leader'
  end

  def cleaner?
    role == 'Cleaner'
  end

  def admin!
    role = 'Admin'
  end

  def super_admin!
    role = 'Super_Admin'
  end

  def team_leader!
    role = 'Team_leader'
  end

  def cleaner!
    role = 'Cleaner'
  end

  def hasRole(r)
    role == r
  end

  def hasRoleInList(roles_list)
    roles_list.include?(role)
  end


private
  def update_job_notes_user_name_cache
    job_notes.update_all(user_name_cache: name)
  end
end
