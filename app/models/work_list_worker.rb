# == Schema Information
#
# Table name: work_list_workers
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  work_list_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class WorkListWorker < ActiveRecord::Base
  belongs_to :user
  belongs_to :work_list
end
