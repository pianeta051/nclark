# == Schema Information
#
# Table name: job_payments
#
#  id         :integer          not null, primary key
#  job_id     :integer          not null
#  payment_id :integer          not null
#  cents      :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class JobPayment < ActiveRecord::Base
  belongs_to :job
  belongs_to :payment

  validates_presence_of :job_id, :payment_id, :cents
  validates_numericality_of :cents, greater_than_or_equal_to: 0

  after_save :calculate_job_payments_for_payment_customer, :update_job_week_finances_cache
  after_destroy :update_job_week_finances_cache

  composed_of :amount,
    class_name: "Money",
    mapping: [["cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

private
  def update_job_week_finances_cache
    job.week.update_finances_cache!
  end

  def calculate_job_payments_for_payment_customer
    payment.customer.calculate_job_payments!
  end
end
