# == Schema Information
#
# Table name: work_list_notes
#
#  id           :integer          not null, primary key
#  work_list_id :string(255)      not null
#  user_id      :string(255)
#  content      :text
#  created_at   :datetime
#  updated_at   :datetime
#

class WorkListNote < ActiveRecord::Base

  belongs_to :work_list
  belongs_to :user

  validates_presence_of :work_list, :content

  default_scope -> { order('work_list_notes.id ASC') }

end
