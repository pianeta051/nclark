# == Schema Information
#
# Table name: expenses
#
#  id             :integer          not null, primary key
#  business_id    :integer          not null
#  date           :date             not null
#  cents          :integer          default(0)
#  vat_cents      :integer          default(0)
#  description    :string(50)
#  created_at     :datetime
#  updated_at     :datetime
#  receipt_number :string(50)
#  notes          :text
#

class Expense < ActiveRecord::Base
  include Bookmarkable

  belongs_to :business
  has_many :attachments, as: :attachable, dependent: :destroy

  validates_presence_of :business, :date
  validates_length_of :description, maximum: 50
  validates_length_of :receipt_number, maximum: 50

  after_save :save_new_attachments
  after_save :destroy_delete_attachments

  composed_of :price,
    class_name: "Money",
    mapping: [["cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :vat,
    class_name: "Money",
    mapping: [["vat_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  scope :in_date_order, -> { order("expenses.date DESC, expenses.receipt_number DESC NULLS LAST, expenses.created_at DESC") }

  scope :in_date_range, lambda { |start_date, finish_date|
    begin
      start_date = Date.parse(start_date)
    rescue
      start_date = nil
    end
    begin
      finish_date = Date.parse(finish_date).to_date
    rescue
      finish_date = nil
    end
    if start_date.present? && finish_date.present?
      if start_date > finish_date
        temp = start_date
        start_date = finish_date
        finish_date = temp
      end
      where('expenses.date >= ? AND expenses.date <= ?', start_date, finish_date)
    elsif start_date.present?
      where('expenses.date >= ?', start_date)
    elsif finish_date.present?
      where('expenses.date <= ?', finish_date)
    else
      all
    end
  }

  scope :search, lambda { |term|
    term = term.downcase
    sterm = "%#{term}%"
    cents = Money.parse(term).cents
    if cents.zero?
      where('expenses.description ILIKE ? OR expenses.notes ILIKE ? OR expenses.receipt_number ILIKE ?', sterm, sterm, sterm)
    else
      where('expenses.description ILIKE ? OR expenses.notes ILIKE ? OR expenses.receipt_number ILIKE ? OR expenses.cents = ? OR (expenses.cents + expenses.vat_cents) = ?', sterm, sterm, sterm, cents, cents)
    end
  }

  def receipt_number_for_table
    return receipt_number if receipt_number.nil? || receipt_number.length < 25
    receipt_number.scan(/.{1,25}/).join(' ')
  end

  def new_attachments=(hash)
    @new_attachments_cache = hash.values.map { |h| h['file'] }.reject(&:blank?)
  end

  def delete_attachments=(hash)
    @delete_attachments_ids_cache = hash.values.select { |h| h['delete'] == '1' && h['id'].present? }.map { |h| h['id'].to_i }
  end

private
  def save_new_attachments
    if @new_attachments_cache.present?
      @new_attachments_cache.each do |file|
        a = attachments.build(attachment: file)
        a.save
      end
    end
  end

  def destroy_delete_attachments
    if @delete_attachments_ids_cache.present?
      attachments.where(id: @delete_attachments_ids_cache).destroy_all
    end
  end
end
