# == Schema Information
#
# Table name: job_notes
#
#  id              :integer          not null, primary key
#  job_id          :integer          not null
#  user_id         :integer
#  content         :text
#  created_at      :datetime
#  updated_at      :datetime
#  user_name_cache :string(255)
#

class JobNote < ActiveRecord::Base

  attr_accessor :property_identifier, :property_hex_color

  belongs_to :job
  belongs_to :user

  validates_presence_of :job, :content

  default_scope -> { order('job_notes.id ASC') }

  after_create :update_user_name_cache

  def date
    created_at
  end

private
  def update_user_name_cache
    update_column(:user_name_cache, user.name) unless user.nil?
  end

end
