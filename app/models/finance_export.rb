# == Schema Information
#
# Table name: finance_exports
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  terms              :json
#  xlsx_file          :string(255)
#  export_finished_at :datetime
#  created_at         :datetime
#  updated_at         :datetime
#

class FinanceExport < ActiveRecord::Base

  mount_uploader :xlsx_file, AttachmentUploader

  belongs_to :user

  validates_presence_of :user, :terms
  validate :dates_present_in_terms
  validate :no_exports_pending

  scope :completed, -> { where("finance_exports.export_finished_at IS NOT NULL") }
  scope :not_completed, -> { where(export_finished_at: nil) }

  def perform_export!
    reset_export!
    destroy_other_exports

    export_customer_details = self.terms['customer_details'] || {}
    customer_details_names = export_customer_details.map { |k, v| v == '1' ? k : nil }.compact
    payments = user.business.payments
                            .includes(:customer_only_fetch_customer_details_cache)
                            .not_credit
                            .in_date_range(terms['from_date'], terms['to_date'])
                            .order('payments.date ASC, payments.method ASC NULLS FIRST')
    expenses = user.business.expenses
                            .in_date_range(terms['from_date'], terms['to_date'])
                            .order('expenses.date ASC, expenses.receipt_number ASC NULLS LAST, expenses.created_at ASC')
    Axlsx::Package.new do |p|
      wb = p.workbook

      currency = wb.styles.add_style(format_code: "[$£-809]#,##0.00;-[$£-809]#,##0.00")

      wb.add_worksheet(name: "Payments") do |sheet|
        sheet.add_row(customer_details_names + ["Date", "Amount", "Method"])
        total = 0.0
        payments.each do |payment|
          index_detail_values = payment.customer.index_detail_values_from_cache(customer_details_names)
          amount = (payment.amount + payment.charge).cents / 100.0
          total += amount
          row_array = index_detail_values + [payment.date, amount, payment.method.to_s]
          sheet.add_row(row_array, types: ([nil] * index_detail_values.length) + [:date, :float, :string], style: ([nil] * index_detail_values.length) + [nil, currency, nil])
        end
        row_array = ([""] * customer_details_names.length) + ["", total, ""]
        sheet.add_row(row_array, types: ([nil] * customer_details_names.length) + [nil, :float, nil], style: ([nil] * customer_details_names.length) + [nil, currency, nil])
      end

      wb.add_worksheet(name: "Expenses") do |sheet|
        sheet.add_row(["Date", "Receipt number", "Price", "VAT", "Total", "Description"])
        total = [0.0, 0.0, 0.0]
        expenses.each do |expense|
          ttotals = [expense.price.cents / 100.0, expense.vat.cents / 100.0, (expense.price + expense.vat).cents / 100.0]
          ttotals.each_with_index { |t, i| total[i] += t }
          row_array = [expense.date, expense.receipt_number, ttotals[0], ttotals[1], ttotals[2], expense.description.gsub(',', ';')]
          sheet.add_row(row_array, types: [:date, :string, :float, :float, :float, :string], style: [nil, nil, currency, currency, currency, nil])
        end
        row_array = ["", "", total[0], total[1], total[2], ""]
        sheet.add_row(row_array, types: [nil, nil, :float, :float, :float, nil], style: [nil, nil, currency, currency, currency, nil])
      end

      file = Tempfile.new(["finances-#{user.business.id}-#{user.id}-#{self.id}", ".xlsx"], "#{Rails.root}/tmp")
      file.write(p.to_stream.read)
      file.close
      self.update_attribute(:xlsx_file, file)
    end
    self.update_column(:export_finished_at, Time.now)
  rescue Exception => e
    puts "Error: #{e}"
    self.destroy
  end

  def xlsx_export_pending?
    !xlsx_export_finished_at?
  end

  def xlsx_export_finished?
    xlsx_export_finished_at? && xlsx_file.present? && xlsx_file.file.exists?
  end

private
  def reset_export!
    self.update_attributes(export_finished_at: nil, remove_xlsx_file: true)
  end

  def destroy_other_exports
    user.finance_exports.where('finance_exports.id != ?', self.id).destroy_all
  end

  def dates_present_in_terms
    errors.add(:base, "From date must be present") if terms['from_date'].blank?
    errors.add(:base, "To date must be present") if terms['to_date'].blank?
    return if terms['from_date'].blank? || terms['to_date'].blank?

    from_date = begin
      Date.parse(terms['from_date'])
    rescue
      nil
    end
    to_date = begin
      Date.parse(terms['to_date'])
    rescue
      nil
    end

    errors.add(:base, "From date is invalid") if from_date.blank?
    errors.add(:base, "To date is invalid") if to_date.blank?
  end

  def no_exports_pending
    if new_record? && user.finance_exports.not_completed.count > 0
      errors.add(:base, "There are finance exports still pending")
    end
  end

end
