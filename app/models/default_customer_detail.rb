# == Schema Information
#
# Table name: default_customer_details
#
#  id                          :integer          not null, primary key
#  business_id                 :integer          not null
#  name                        :string(250)
#  large_field                 :boolean          default(FALSE)
#  visible_in_index            :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  singular                    :boolean          default(FALSE)
#  order                       :integer          not null
#  customer_detail_category_id :integer          not null
#  visible_in_work_list        :boolean          default(FALSE)
#  work_list_order             :integer          not null
#

class DefaultCustomerDetail < ActiveRecord::Base
  RESTRICTED_NAMES = %w(notes area ALL_FIELDS)

  belongs_to :business
  belongs_to :customer_detail_category

  validates_presence_of :business, :order
  validates :name, presence: true, length: { maximum: 250 }
  validate :name_is_unique
  validate :name_is_not_restricted

  default_scope -> { order('default_customer_details.order ASC') }
  scope :work_list_order, -> { order('default_customer_details.work_list_order ASC') }
  scope :visible_in_index, -> { where(visible_in_index: true) }
  scope :visible_in_work_list, -> { where(visible_in_work_list: true) }

  before_create :set_work_list_order

  def update_customer_details_with_name(previous_name)
    return if self.name == previous_name

    CustomerDetail.connection.execute "
WITH parent_customer_details AS (
  SELECT customer_details.id, customer_details.customer_id
  FROM customer_details
  WHERE customer_details.customer_id IN (SELECT customers.id FROM customers WHERE customers.business_id = #{business_id})
  AND customer_details.name = '#{previous_name}'
)
UPDATE customer_details
SET name = '', parent_id = parent_customer_details.id
FROM parent_customer_details
WHERE customer_details.customer_id = parent_customer_details.customer_id
AND customer_details.id != parent_customer_details.id
AND customer_details.name = '#{self.name}'"

    CustomerDetail.from_business(business_id)
                  .where(:name => previous_name)
                  .update_all(:name => self.name)
  end

private
  def name_is_unique
    if business
      existing_details = business.default_customer_details.where(:name => self.name)
      unless new_record?
        existing_details = existing_details.where("default_customer_details.id != ?", self.id)
      end
      if existing_details.count > 0
        errors.add(:name, "must be unique")
      end
    end
  end

  def name_is_not_restricted
    if RESTRICTED_NAMES.include? self.name.downcase
      errors.add(:name, "cannot be #{self.name}")
    end
  end

  def set_work_list_order
    if self.work_list_order.nil?
      self.work_list_order = (business.default_customer_details.pluck(:work_list_order).sort.last || -1) + 1
    end
  end

end
