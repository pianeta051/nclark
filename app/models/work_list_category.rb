# == Schema Information
#
# Table name: work_list_categories
#
#  id                :integer          not null, primary key
#  business_id       :integer          not null
#  name              :string(40)       not null
#  created_at        :datetime
#  updated_at        :datetime
#  tag               :string(255)
#  target_cents      :integer          default(0)
#  balance_cents     :integer          default(0)
#  balance_date_from :date
#  user_id           :integer

class WorkListCategory < ActiveRecord::Base

  belongs_to :business
  belongs_to :user
  has_many :work_lists, dependent: :restrict_with_exception

  validates :name, presence: true, length: { maximum: 40 }
  validates_length_of :tag, maximum: 250
  validates :user_id, uniqueness: false
  after_create :create_work_lists_for_weeks
  before_save :update_tag_if_blank
  after_validation :check_if_target_changed
  after_save :update_future_work_list_tags_and_targets

  default_scope -> { order('work_list_categories.id ASC') }

  composed_of :target,
    class_name: "Money",
    mapping: [["target_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :balance,
    class_name: "Money",
    mapping: [["balance_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }


  def update_balance!
    bc = work_lists.where('work_lists.target_delta_cents_cache != 0')
                   .where('work_lists.job_counter_cache > 0')
                   .where('work_lists.date_cache <= ?', Date.today)
    if balance_date_from?
      bc = bc.where('work_lists.date_cache >= ?', balance_date_from)
    end
    bc = bc.pluck(:target_delta_cents_cache)
           .inject(0, &:+)
    update_column(:balance_cents, bc)
  end

private
  def create_work_lists_for_weeks
    business.weeks.each do |week|
      7.times do |i|
        if work_lists.where(week_id: week.id, day: i).first.nil?
          work_lists.create(week: week, day: i)
        end
      end
    end
  end

  def update_tag_if_blank
    if tag.blank?
      self.tag = name
    end
  end

  def check_if_target_changed
    @target_has_changed = target_cents_changed?
  end

  def update_future_work_list_tags_and_targets
    params = if @target_has_changed
      {
        tag: self.tag,
        target_cents: self.target_cents
      }
    else
      {
        tag: self.tag
      }
    end
    work_lists.in_future_weeks.update_all(params)
  end

end
