# == Schema Information
#
# Table name: work_lists
#
#  id                        :integer          not null, primary key
#  work_list_category_id     :integer          not null
#  week_id                   :integer          not null
#  day                       :integer          not null
#  created_at                :datetime
#  updated_at                :datetime
#  token_1                   :string(255)
#  token_2                   :string(255)
#  tag                       :string(255)
#  completed                 :boolean          default(FALSE)
#  target_cents              :integer          default(0)
#  date_cache                :date
#  job_counter_cache         :integer
#  completed_job_cents_cache :integer
#  target_delta_cents_cache  :integer          default(0)
#  visible                   :boolean          default(FALSE)
#  publication_date          :date
#  publication_time          :time

class WorkList < ActiveRecord::Base

  belongs_to :work_list_category
  belongs_to :week
  has_many :jobs, dependent: :nullify
  has_many :work_list_notes, dependent: :destroy
  has_many :work_list_workers, dependent: :destroy
  has_many :users, through: :work_list_workers

  after_create :generate_tokens!
  after_create :update_date_cache!
  before_save :update_tag_if_blank
  after_save :update_job_caches!

  validates_presence_of :work_list_category
  validates_length_of :tag, maximum: 250

  scope :in_future_weeks, -> { where('work_lists.week_id IN (SELECT weeks.id FROM weeks WHERE weeks.start_date > ?)', Date.today) }
  scope :by_date, lambda { |asc_desc = :asc| order("work_lists.date_cache #{asc_desc.to_s.upcase}") }

  scope :in_date_range, lambda { |start_date, finish_date|
    begin
      start_date = Date.parse(start_date)
    rescue
      start_date = nil
    end
    begin
      finish_date = Date.parse(finish_date)
    rescue
      finish_date = nil
    end
    if start_date.present? && finish_date.present?
      where('work_lists.date_cache >= ? AND work_lists.date_cache <= ?', start_date, finish_date)
    elsif start_date.present?
      where('work_lists.date_cache >= ?', start_date)
    elsif finish_date.present?
      where('work_lists.date_cache <= ?', finish_date)
    else
      all
    end
  }

  composed_of :target,
    class_name: "Money",
    mapping: [["target_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  def name(week_override = nil)
    "#{((week_override || week).start_date + day.days).to_s(:short)} - #{work_list_category.name}"
  end

  def url_token_params
    {:t1 => token_1, :t2 => token_2}
  end

  def full_name
    if work_list_category.name != tag
      "#{work_list_category.name} - #{tag}"
    else
      tag
    end
  end

  def full_name_with_date(week_override = nil)
    "#{((week_override || week).start_date + day.days).strftime("%a %d %b")} - #{full_name}"
  end

  def job_total_price_including_vat
    jobs.inject(Money.new(0, 'GBP')) { |total, job| total + job.price_including_vat }
  end

  def job_total_outstanding_including_vat
    jobs.inject(Money.new(0, 'GBP')) { |total, job| total + job.customer.outstanding_money }
  end

  def completed_job_total_price_including_vat
    jobs.completed.inject(Money.new(0, 'GBP')) { |total, job| total + job.price_including_vat }
  end

  def paid_job_total_price_including_vat
    jobs.paid.inject(Money.new(0, 'GBP')) { |total, job| total + job.price_including_vat }
  end

  def date
    @date ||= (week.start_date + day.days).to_date
  end

  def mark_as_completed!
    update_column(:completed, true)
  end

  def mark_as_incompleted!
    update_column(:completed, false)
  end

  def html_classes
    (job_counter_cache > 0 ? 'work-list-with-jobs ' : '') + (completed? ? 'work-list-signed-off' : '')
  end

  def apply_target?
    if @apply_target.nil?
      @apply_target = !job_counter_cache.zero?
    end
    @apply_target
  end

  def target_met?
    if @target_met.nil?
      @target_met = completed_job_cents_cache >= target_cents
    end
    @target_met
  end

  def target_delta_cents
    target_delta_cents_cache
  end

  def target_delta
    Money.new(target_delta_cents, 'GBP')
  end

  def target_met_html_classes
    if !apply_target? || date_cache > Date.today
      ""
    elsif target_delta_cents >= 0
      "target-met"
    elsif target_delta_cents < 0
      "target-not-met"
    else
      ""
    end
  end

  def update_job_ordering(new_job)
    not_new_job_jobs = self.jobs.where("jobs.id != ?", new_job.id)
    if not_new_job_jobs.count > 0 && not_new_job_jobs.all? { |job| job.work_list_order.present? }
      jobs = self.jobs.where("jobs.start_time IS NOT NULL").where("jobs.id != ?", new_job.id)
      if jobs.count.zero? || new_job.start_time.blank?
        if new_job.start_time.present?
          self.jobs.where("jobs.id != ?", new_job.id).each do |job|
            job.update_column(:work_list_order, job.work_list_order + 1)
          end
          new_job.update_column(:work_list_order, 0)
        else
          new_job.update_column(:work_list_order, self.jobs.where("jobs.id != ?", new_job.id).count)
        end
      else
        jobs_in_work_list_order = jobs.work_list_order.to_a

        smallest_time_difference = nil
        smallest_time_difference_job = nil
        jobs_in_work_list_order.each do |job|
          time = job.start_time - new_job.start_time
          if smallest_time_difference_job.nil? || time.abs < smallest_time_difference.abs
            smallest_time_difference = time
            smallest_time_difference_job = job
          end
        end
        if smallest_time_difference < 0
          self.jobs.where('jobs.work_list_order > ?', smallest_time_difference_job.work_list_order).each do |job|
            job.update_column(:work_list_order, job.work_list_order + 1)
          end
          new_job.update_column(:work_list_order, smallest_time_difference_job.work_list_order + 1)
        else
          self.jobs.where('jobs.work_list_order >= ?', smallest_time_difference_job.work_list_order).each do |job|
            job.update_column(:work_list_order, job.work_list_order + 1)
          end
          new_job.update_column(:work_list_order, smallest_time_difference_job.work_list_order)
        end
      end
    else
      self.jobs.update_all(work_list_order: nil)
    end
  end

  def update_job_caches!
    cjcc = jobs.completed.pluck(:cents).inject(0, &:+)
    update_columns(job_counter_cache: jobs.count,
                   completed_job_cents_cache: cjcc,
                   target_delta_cents_cache: cjcc - target_cents)
    work_list_category.update_balance!
  end

  # def user_work_lists(day_index, user)
  #   if user
  #     if user.hasRole('Admin') || user.hasRole('Super_Admin')
  #       work_lists.where(day: day_index)
  #     elsif user.hasRole('Team_leader')
  #       work_lists.where(day: day_index, work_list_category_id: user.work_list_category.id)
  #     end
  #   end
  # end

  def is_user_in_risk_assement?(risks_assessment_id, user_id)
    RisksUser.where(risks_assessment_id: risks_assessment_id, user_id: user_id).empty?
  end

  def already_accept_any_risks_assessment(risks_assessment_id, user_id)
    RisksUser.find_by(risks_assessment_id: risks_assessment_id, user_id: user_id).created_at.strftime("%m/%d/%Y")==Date.today.strftime("%m/%d/%Y")
  end


private
  def generate_tokens!
    update_attributes(token_1: SecureRandom.uuid, token_2: SecureRandom.uuid)
  end

  def update_date_cache!
    update_column(:date_cache, week.start_date + day)
  end

  def update_tag_if_blank
    if tag.blank?
      self.tag = work_list_category.tag
    end
  end
end
