# == Schema Information
#
# Table name: customer_search_query_customers
#
#  id          :integer          not null, primary key
#  query_id    :integer          not null
#  customer_id :integer          not null
#  order       :integer          not null
#

class CustomerSearchQueryCustomer < ActiveRecord::Base

  belongs_to :customer_search_query, foreign_key: :query_id
  belongs_to :customer

end
