# == Schema Information
#
# Table name: customers
#
#  id                      :integer          not null, primary key
#  business_id             :integer          not null
#  area_id                 :integer          not null
#  regularity              :integer
#  created_at              :datetime
#  updated_at              :datetime
#  start_date              :date
#  regular_price_cents     :integer          default(0)
#  regularity_type         :string(6)
#  add_vat                 :boolean          default(FALSE)
#  color                   :string(6)
#  non_regular             :boolean          default(TRUE)
#  customer_details_cache  :hstore
#  last_clean              :date
#  jobs_count              :integer          default(0), not null
#  area_color_cache        :string(6)        not null
#  area_name_cache         :string(255)
#  active                  :boolean          default(TRUE)
#  outstanding_money_cents :integer          default(0)
#  owner_id                :integer
#  day_span                :integer          default(1)
#

class Customer < ActiveRecord::Base
  include Bookmarkable

  REGULARITY_TYPES = %w(day week month year)
  PARAMS_TO_PERMIT = [:area_id, :regularity, :regularity_type, :start_date, :regular_price, :add_vat, :color, :non_regular, :regular, :owner_id, :day_span]

  attr_accessor :customer_details_data

  belongs_to :business
  belongs_to :area
  belongs_to :owner, class_name: 'Customer'
  has_many :properties, foreign_key: :owner_id, class_name: 'Customer'
  has_many :jobs, dependent: :restrict_with_exception
  has_many :customer_details, dependent: :destroy
  has_many :customer_notes, dependent: :destroy
  has_many :attachments, as: :attachable, dependent: :destroy
  has_many :payments, dependent: :restrict_with_exception
  has_many :risks_assessments

  composed_of :regular_price,
    class_name: "Money",
    mapping: [["regular_price_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  composed_of :outstanding_money,
    class_name: "Money",
    mapping: [["outstanding_money_cents", "cents"]],
    constructor: Proc.new { |cents| Money.new(cents || 0, 'GBP') },
    converter: Proc.new { |rp| rp.to_money('GBP') }

  validates_presence_of :area
  validates :regularity, numericality: { integer: true }
  validates :regular_price_cents, numericality: { integer: true, greater_than_or_equal_to: 0 }
  validates_inclusion_of :regularity_type, in: REGULARITY_TYPES, allow_blank: true
  validates :day_span, numericality: { integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }
  validate :is_either_owned_or_is_owner_or_neither

  before_save :update_area_caches
  after_save :update_last_clean, :update_customer_details_from_customer_details_data, :update_customer_details_cache

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :owing, -> { where("customers.outstanding_money_cents > 0") }
  scope :not_owing, -> { where("customers.outstanding_money_cents <= 0") }
  scope :sort_by, lambda { |field_name, asc_desc, blank_value_order, group_by_area, weak_group_by_area, use_trimmed_value|
    order(Customer.sort_by_query(field_name, asc_desc, blank_value_order, group_by_area, weak_group_by_area, use_trimmed_value))
  }

  scope :search_by_customer_detail_value, lambda { |value, field_names|
    where("customers.id IN (
      SELECT customer_details.customer_id
        FROM customer_details
        WHERE customer_details.value ILIKE ?
        #{field_names.present? ? 'AND customer_details.name IN (?)' : ''}
      )", *(["%#{value}%", field_names.blank? ? nil : [field_names].flatten].reject(&:blank?)))
  }

  scope :not_one_off, -> {
    where("
      customers.regularity IS NOT NULL
      AND customers.regularity_type IS NOT NULL
      AND customers.regularity_type != ?
      AND customers.regularity_type != ?
      AND customers.regularity > 0
      AND customers.start_date IS NOT NULL
      ", '', 'callus')
  }

  def last_risk_assessment
    RisksAssessment.where(customer_id:self.id).pluck(:id)
  end

  # def user_in_risk_assement
  #   RisksUser.where()
  # end

  def owing?
    outstanding_money_cents > 0
  end

  def one_off?
    regularity.blank? || regularity_type.blank? || regularity <= 0 || start_date.blank?
  end

  def regularity_in_time
    regularity.send(regularity_type)
  end

  def regular_price_including_vat
    regular_price * business.default_vat_ratio
  end

  def regular
    regular?
  end

  def regular?
    !self.non_regular?
  end

  def regular=(value)
    self.non_regular = !['1', 'true', 't', true].include?(value)
  end

  def owned?
    owner_id.present?
  end

  def has_pending_jobs(user)
    customer_wlcs=jobs.not_completed.pluck(:work_list_category_id)
    user_wlcs=WorkListCategory.where(user_id: user.id).pluck(:id)
    !(customer_wlcs & user_wlcs).empty?
  end

  def notes(add_property_identifier = false)
    unless @notes
      @notes = customer_notes.to_a
      jobs.each do |job|
        job.job_notes.each { |job_note| @notes << job_note }
      end
      if add_property_identifier
        @notes.each { |note| note.property_identifier = property_identifier.present? ? property_identifier : 'NO ID' }
      end
      @notes.each { |note| note.property_hex_color = property_hex_color }
      properties.each do |property|
        @notes += property.notes(true)
      end
      @notes.sort! { |a, b| [a.date, a.created_at] <=> [b.date, b.created_at] }
    end
    @notes
  end

  def all_jobs(add_property_identifier = false, save_search = true, &scope_block)
    unless @all_jobs
      found_jobs = (scope_block ? scope_block.call(jobs.unlinked) : jobs.unlinked).to_a
      if add_property_identifier
        found_jobs.each { |job| job.property_identifier = property_identifier }
      end
      properties.each do |property|
        found_jobs += property.all_jobs(true, false, &scope_block)
      end
      found_jobs.sort! { |a, b| (b.date || b.week.start_date) <=> (a.date || a.week.start_date) }
      if save_search
        @all_jobs = found_jobs
      else
        return found_jobs
      end
    end
    @all_jobs
  end

  def all_payments(add_property_identifier = false, &scope_block)
    unless @all_payments
      @all_payments = (scope_block ? scope_block.call(payments) : payments).to_a
      if add_property_identifier
        @all_payments.each { |payment| payment.property_identifier = property_identifier }
      end
      properties.each do |property|
        @all_payments += property.all_payments(true, &scope_block)
      end
      @all_payments.sort! { |a, b| b.date <=> a.date }
    end
    @all_payments
  end

  def property_identifier
    @property_identifier ||= (business.first_default_customer_detail.nil? ? nil : customer_detail_value_from_cache(business.first_default_customer_detail.name))
  end

  def property_hex_color
    @property_hex_color ||= "%06x" % (id.hash.abs.to_s.prepend('0.').to_f * 0xffffff)
  end

  def job_in_current_week(params = {})
    unless @job_in_current_week
      current_week = params[:week_override] || business.current_week
      @job_in_current_week = jobs.where(week_id: current_week.id).first if current_week
    end
    @job_in_current_week
  end

  def current_week_job_html_classes(params = {})
    html_classes = ""
    if params[:job_fields_cached]
      if job_id_cache.present?
        html_classes << "job-in-current-week"
        html_classes << " job-non-regular" if non_regular
        html_classes << " job-assigned-to-work-list" if job_in_current_week_work_list_id_cache.present?
      end
    else
      if params[:jobs]
        job = params[:jobs].bsearch { |job| job.customer_id >= id }
        job = nil unless job.customer_id == id
      else
        job = params[:job_override] || job_in_current_week(params)
      end
      if job.present?
        html_classes << "job-in-current-week"
        html_classes << " job-non-regular" if non_regular
        html_classes << " job-assigned-to-work-list" if job.work_list_id.present?
      end
    end
    html_classes
  end

  def index_details(details_included = false, index_default_customer_details_names = nil)
    index_default_customer_details_names ||= business.index_default_customer_details.pluck(:name)
    if details_included
      customer_details.select { |customer_detail| index_default_customer_details_names.include? customer_detail.name }
    else
      customer_details.where(name: index_default_customer_details_names).order('group_index ASC, created_at ASC')
    end
  end

  def index_details_from_cache(index_default_customer_details_names = nil)
    index_default_customer_details_names ||= business.index_default_customer_details.pluck(:name)
    cached_customer_detail = Struct.new(:name, :value, :children)
    result = []
    index_default_customer_details_names.each do |name|
      value = customer_details_cache[name]
      if value.nil?
        ccd = cached_customer_detail.new(name, nil, [])
      else
        values = value.split(CustomerDetail::CACHE_VALUE_SEPARATOR)
        ccd = cached_customer_detail.new(name, values.first, [])
        values[1..-1].each_with_index do |v, i|
          ccd.children << cached_customer_detail.new("#{name} #{i + 2}", v, [])
        end
      end
      result << ccd
    end
    result
  end

  def index_detail_values_from_cache(index_default_customer_details_names = nil)
    index_default_customer_details_names ||= business.index_default_customer_details.pluck(:name)
    result = []
    index_default_customer_details_names.each do |name|
      value = customer_details_cache[name]
      if value.nil?
        result << nil
      else
        result << value.split(CustomerDetail::CACHE_VALUE_SEPARATOR).first
      end
    end
    result
  end

  def customer_detail_value_from_cache(name)
    value = customer_details_cache[name]
    if value.nil?
      nil
    else
      value.split(CustomerDetail::CACHE_VALUE_SEPARATOR).first
    end
  end

  def formatted_index_details(details_included = false, index_default_customer_details_names = nil)
    index_details(details_included, index_default_customer_details_names).map(&:value).join(', ')
  end

  def default_customer_details
    split_default_and_extra_customer_details unless @default_customer_details
    @default_customer_details
  end

  def default_customer_details_by_category
    split_default_customer_details_into_categories unless @default_customer_details_by_category
    @default_customer_details_by_category
  end

  def extra_customer_details
    split_default_and_extra_customer_details unless @extra_customer_details
    @extra_customer_details
  end

  def build_default_customer_details
    group_indexes = [0]
    customer_details.each do |customer_detail|
      if customer_detail.group_index && !group_indexes.include?(customer_detail.group_index)
        group_indexes << customer_detail.group_index
      end
    end

    default_customer_details = business.default_customer_details
    group_indexes.each do |group_index|
      customer_details_names = customer_details.where(group_index: group_index).pluck(:name)
      default_customer_details.reject { |detail| customer_details_names.include? detail.name }.each do |detail|
        customer_details.build(name: detail.name, large_field: detail.large_field, default: true, group_index: group_index)
      end
    end

    customer_details.each do |customer_detail|
      default_customer_detail = default_customer_details.find { |detail| detail.name == customer_detail.name }
      if default_customer_detail
        customer_detail.default = true
        customer_detail.large_field = default_customer_detail.large_field
        if customer_detail.new_record?
          customer_detail.default_customer_detail = default_customer_detail
        end
      end
    end
  end

  def self.sort_by_query(field_name, asc_desc, blank_value_order, group_by_area, weak_group_by_area, use_trimmed_value)
    sort_by_price = field_name == 'Regular price'
    field_name = '' if sort_by_price
    asc_desc = 'asc' unless %w(asc desc).include? asc_desc.downcase
    blank_value_order = 'last' unless %w(first last).include? blank_value_order.downcase
    str = "(
    SELECT customer_details.#{use_trimmed_value ? 'trimmed_' : ''}value
    FROM customer_details
    WHERE customer_details.name = '#{field_name}'
      AND customer_details.customer_id = customers.id
    LIMIT 1
    ) #{asc_desc.upcase} NULLS #{blank_value_order.upcase}"
    if sort_by_price
      str = "customers.regular_price_cents #{asc_desc.upcase}, " + str
    end
    if group_by_area
      if weak_group_by_area
        str << ", customers.area_id ASC"
      else
        str = "customers.area_id ASC, " + str
      end
    end
    str
  end

  def set_customer_details_data_from_typical_params(params)
    new_customer_details_data = {}
    i = 0
    params.each do |key, value|
      new_customer_details_data[i] = value
      i += 1
    end
    self.customer_details_data = new_customer_details_data
  end

  def last_clean_string
    if last_clean.blank?
      "No jobs completed"
    else
      last_clean.to_s(:long)
    end
  end

  def update_last_clean
    return if @updating_customer_details_cache

    if jobs.completed.count.zero?
      self.update_column(:last_clean, nil)
    else
      self.update_column(:last_clean, jobs.by_completed_at(:asc).last.completed_at)
    end
  end

  def activate!
    update_column(:active, true)
  end

  def deactivate!
    update_column(:active, false)
  end

  def calculate_job_payments!
    total_paid_cents = payments.inject(0) { |total, payment| total + payment.cents }
    total_owing_cents = jobs.completed.inject(0) { |total, job| total + job.cents }
    jobs.each do |job|
      job.update_payments_count!
      job.update_column(:amount_paid_cents, job.job_payments.inject(0) { |total, jp| total + jp.cents })
    end
    update_column(:outstanding_money_cents, total_owing_cents - total_paid_cents)
=begin
    leftover_cents = 0
    jobs.completed.by_completed_at(:asc).each do |job|
      if total_paid_cents > job.cents
        job.update_column(:amount_paid_cents, job.cents)
        total_paid_cents -= job.cents
      else # no break at total_paid_cents = 0 to populate other jobs with 0s
        job.update_column(:amount_paid_cents, total_paid_cents)
        leftover_cents += job.cents - total_paid_cents
        total_paid_cents = 0
      end
    end
    if total_paid_cents > 0 && leftover_cents.zero?
      self.update_column(:outstanding_money_cents, -total_paid_cents)
    else
      self.update_column(:outstanding_money_cents, leftover_cents)
    end
=end
  end



private
  def split_default_and_extra_customer_details
    business_default_customer_details = business.default_customer_details.to_a
    @default_customer_details = []
    @extra_customer_details = []
    customer_details.select { |customer_detail| customer_detail.parent.blank? }.each do |customer_detail|
      business_default_customer_detail = business_default_customer_details.find { |bdcd| bdcd.name == customer_detail.name }
      if !customer_detail.extra_detail && business_default_customer_detail
        customer_detail.default_customer_detail = business_default_customer_detail
        customer_detail.singular = business_default_customer_detail.singular
        customer_detail.order = business_default_customer_detail.order
        @default_customer_details << customer_detail
        customer_detail.children.each do |child|
          child.order = customer_detail.order
          child.default_customer_detail = customer_detail.default_customer_detail
          @default_customer_details << child
        end
      else
        @extra_customer_details << customer_detail
      end
    end
    @default_customer_details.sort! { |a, b| a.order <=> b.order }
  end

  def split_default_customer_details_into_categories
    result = default_customer_details.reject(&:parent_id).group_by do |customer_detail|
      customer_detail.default_customer_detail.customer_detail_category
    end.to_a.sort { |a, b| a[0].order <=> b[0].order }
    result.each do |key_value|
      key_value[0] = key_value[0].name
      key_value[1].sort! { |a, b| a.order <=> b.order }
    end
    @default_customer_details_by_category = []
    result.each do |key_value|
      values = []
      key_value[1].each do |customer_detail|
        values << customer_detail
        customer_detail.children.each { |child| values << child }
      end
      @default_customer_details_by_category << [key_value[0], values]
    end
  end

  def update_customer_details_from_customer_details_data
    return if @updating_customer_details_cache
    return unless customer_details_data.present?

    split_customer_details_data = customer_details_data.partition { |key, value| value['parent_input_id'] }
    parent_customer_details_data = Hash[split_customer_details_data[1]]
    child_customer_details_data = Hash[split_customer_details_data[0]]

    parent_customer_details_data.each do |input_id, data|
      if data['id']
        customer_detail = customer_details.find_by_id(data['id'])
        next unless customer_detail
        if data['name'].present? && data['value'].present?
          customer_detail.extra_detail = data['extra_detail'] == 'true'
          customer_detail.update_attributes(name: data['name'], value: data['value'], group_index: data['group_index'])
        else
          customer_detail.destroy
          data['destroyed_customer_detail'] = customer_detail
        end
      elsif data['name'].present? && data['value'].present?
        customer_detail = customer_details.create(extra_detail: data['extra_detail'] == 'true', name: data['name'], value: data['value'], group_index: data['group_index'])
        data['id'] = customer_detail.id
      end
    end

    child_customer_details_data.each do |input_id, data|
      if data['id']
        customer_detail = customer_details.find_by_id(data['id'])
        next unless customer_detail
        if data['value'].present?
          customer_detail.update_attributes(value: data['value'], group_index: data['group_index'])
        else
          customer_detail.destroy
        end
      elsif data['value'].present?
        parent_data = parent_customer_details_data[data['parent_input_id']]
        parent_customer_detail = customer_details.find_by_id(parent_data['id'])
        if parent_customer_detail
          customer_details.create(value: data['value'], group_index: data['group_index'], parent_id: parent_customer_detail.id)
        elsif parent_data['destroyed_customer_detail']
          customer_detail = customer_details.create(name: parent_data['destroyed_customer_detail'].name, value: data['value'], group_index: data['group_index'])
          parent_data['id'] = customer_detail.id
        elsif parent_data['name']
          customer_detail = customer_details.create(name: parent_data['name'], value: data['value'], group_index: data['group_index'])
          parent_data['id'] = customer_detail.id
        end
      end
    end
  end

  def update_customer_details_cache
    return if @updating_customer_details_cache

    cache = {}

    customer_details.without_parents.each do |customer_detail|
      cache[customer_detail.name] = customer_detail.value
      customer_detail.children.each do |child|
        cache[customer_detail.name] << "#{CustomerDetail::CACHE_VALUE_SEPARATOR}#{child.value}"
      end
    end

    @updating_customer_details_cache = true
    self.update_attribute(:customer_details_cache, cache)
    @updating_customer_details_cache = false
  end

  def update_area_caches
    if area_id_changed?
      self.area_color_cache = area.try(:color)
      self.area_name_cache = area.try(:name)
    end
  end

  def is_either_owned_or_is_owner_or_neither
    if self.owner_id.present?
      if Customer.find(self.owner_id).business != business
        errors.add(:base, "Unrecognised customer")
      end
      if self.owner_id == self.id
        errors.add(:base, "Customer cannot own itself")
      end
      if properties.count > 0
        errors.add(:base, "Cannot belong to another customer if properties are owned")
      elsif Customer.find(self.owner_id).owner.present?
        errors.add(:base, "Cannot belong to a property")
      end
    end
  end
end
