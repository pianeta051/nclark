# == Schema Information
#
# Table name: risks_users
#
#  id                  :integer          not null, primary key
#  risk_cheacked       :boolean
#  user_id             :integer
#  risks_assessment_id :integer
#  created_at          :datetime
#  updated_at          :datetime
#  date                :datetime
#

class RisksUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :risks_assessment
end
