module CustomerDetailCategoriesHelper

  def customer_detail_categories_for_select
    current_business.customer_detail_categories.map do |customer_detail_category|
      [customer_detail_category.name, customer_detail_category.id]
    end
  end

end
