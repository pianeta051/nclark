module ApplicationHelper

  def active_if(controller_name, action_name = nil)
    if controller_name.is_a? Array
      controller_name.each do |name|
        result = active_if(name, action_name)
        return result unless result.nil?
      end
    else
      if controller.controller_name == controller_name
        if action_name.nil? || controller.action_name == action_name
          return "active"
        end
      end
    end
    nil
  end

  def current_user
    @current_user
  end

  def quick_search_params_present?
    @quick_search_params_present
  end

end
