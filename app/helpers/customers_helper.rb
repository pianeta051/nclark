module CustomersHelper

  def customer_titles_for_select
    %w(Mr Mrs Mr\ &\ Mrs Ms Dr).map { |a| [a, a] }
  end

  def customer_regularities_for_select
    [['None agreed', 0]] + (1..100).map { |i| [i, i] }
  end

  def customer_regularity_types_for_select
    [['N/A', nil]] + Customer::REGULARITY_TYPES.map { |rt| [rt, rt] }
  end

  def customer_sort_fields_for_select(first_option = nil)
    [[first_option || 'None', nil]] + current_business.index_default_customer_details.pluck(:name).map { |name| [name, name] } + [['Regular price', 'Regular price']]
  end

  def customer_details_fields_for_select(first_option = nil)
    [[first_option || 'None', nil]] + current_business.default_customer_details.pluck(:name).map { |name| [name, name] }
  end

  def customer_area_fields_for_select
    current_business.areas.order('areas.name ASC').pluck(:name, :id)
  end

end
