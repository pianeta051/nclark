module JobsHelper

  def job_work_lists_for_select(job)
    job.week.work_lists.map { |wl| [wl.name, wl.id] }
  end

  def short_days_of_week
    ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  end

end
