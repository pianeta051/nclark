module UsersHelper

  def user_roles_for_select
    if current_user && current_user.admin?
      %w(Admin Team_leader Cleaner).map { |a| [a, a] }
    else
      %w(Super_Admin Admin Team_leader Cleaner).map { |a| [a, a] }
    end
  end
  def user_names_for_select
  	current_business.users.order('users.name ASC').pluck(:name, :id)
  end

  def user_names_for_select
  	current_business.users.order('users.name ASC').pluck(:name, :id)
  end
  def team_leaders_for_select
  	current_business.users.where(role:'Team_leader').pluck(:name, :id)
  end

  def cleaner_for_select
  	current_business.users.where(role:'Cleaner').pluck(:name, :id)
  end

end
