module WorkListCategoriesHelper

  def work_list_categories_for_select
    [['None', nil]] + current_business.work_list_categories.map { |wlc| [wlc.name, wlc.id] }
  end

end
