module CustomerSearchQueriesHelper

  def formatted_customer_search_query(customer_search_query)
    hash = customer_search_query.search_terms

    search_str = hash['search'].nil? ? nil : 'Search: ' + hash['search'].map do |_, group|
      "(" + group.map do |_, term|
        field = term['field']
        term = term['term']
        if field == 'Area'
          term = current_business.areas.find(term).try(:name)
        elsif field == 'ALL_FIELDS'
          field = 'All fields'
        end
        "#{field} = \"#{term}\""
      end.join(' or ') + ")"
    end.join(' and ')

    sort_str = hash['sort'].nil? ? nil : 'Sort: ' + hash['sort'].map do |_, group|
      str = "#{group['field']} #{group['order'].downcase}"
      str << " (excluding numbering)" if group['exclude_numbering'] == 'true'
      str
    end.join(' then ')

    extras_str = hash['extras'].nil? ? nil : hash['extras'].map do |key, value|
      toggle_values = case key
      when 'bookmarked'
        bookmarked_toggle_values
      when 'assigned'
        assigned_toggle_values
      when 'active'
        active_toggle_values
      when 'owing'
        owing_toggle_values
      else
        {}
      end

      result = nil
      toggle_values.each do |k, v|
        if v == value
          result = toggle_values["tooltip_#{k[/\d/]}".to_sym]
          break
        end
      end

      result
    end.compact.join(', ')

    [search_str, sort_str, extras_str].compact.join('<br/>').html_safe
  end

  def bookmarked_toggle_values
    {
      tooltip_0: 'Show bookmarked and unbookmarked', value_0: 'both',
      tooltip_1: 'Show bookmarked', value_1: 'applied',
      tooltip_2: 'Show unbookmarked', value_2: 'unapplied'
    }
  end

  def assigned_toggle_values
    {
      tooltip_0: 'Show assigned and unassigned', value_0: 'both',
      tooltip_1: 'Show assigned', value_1: 'applied',
      tooltip_2: 'Show unassigned', value_2: 'unapplied'
    }
  end

  def active_toggle_values
    {
      tooltip_0: 'Show active and inactive', value_0: 'both',
      tooltip_1: 'Show active', value_1: 'applied',
      tooltip_2: 'Show inactive', value_2: 'unapplied'
    }
  end

  def owing_toggle_values
    {
      tooltip_0: 'Show owing and not owing', value_0: 'both',
      tooltip_1: 'Show owing', value_1: 'applied',
      tooltip_2: 'Show not owing', value_2: 'unapplied'
    }
  end

end
