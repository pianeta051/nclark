class RisksAssessmentSerializer < ActiveModel::Serializer
  attributes :id, :yes, :too_high, :ground_not_safe, :no_clear_access_to_window, :work_from_inside, :footed_ladder, :folded_ladder, :read_and_wash, :secured_ladder, :harness, :bar, :other, :why, :operative, :date
  has_one :customer
end
