class CustomerSerializer < ActiveModel::Serializer

  class CustomerDetailSerializer < ActiveModel::Serializer

    class DefaultCustomerDetailSerializer < ActiveModel::Serializer
      attributes :id, :name, :large_field, :singular, :order
      has_one :customer_detail_category
    end

    attributes :id, :name, :value, :group_index
    has_one :business_default_customer_detail, serializer: DefaultCustomerDetailSerializer
    has_many :children, serializer: CustomerDetailSerializer
  end

  has_many :parent_customer_details, serializer: CustomerDetailSerializer

  def parent_customer_details
    object.customer_details.to_a.reject(&:parent_id)
  end

end
