class UsersController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!, except: [:new, :create, :sign_in, :sign_in_submit, :sign_out_user]
 # before_action :check_user_role, except: [:new, :create, :sign_in, :sign_in_submit, :sign_out_user, :edit, :update]
  before_action :check_correct_user, only: [:edit, :update]
  before_action :get_title_for_resource_action, except: [:sign_in]
  before_action except: [:new, :create, :sign_in, :sign_in_submit, :sign_out_user, :edit, :update] do
    check_user_role(['Super_Admin','Admin','Team_leader','Cleaner'])
  end
  def index
    @users = current_business.users
  end

  def new
    authenticate_user! if current_business.users.count > 0
    @user = current_business.users.new
  end

  def create
    authenticate_user! if current_business.users.count > 0
    @user = current_business.users.build(user_params)
    if @user.save
      if current_user.nil?
        session[:user_id] = @user.id
      end
      flash[:success] = "User '#{@user.name}' created"
      redirect_to users_path
    else
      render :new
    end
  end

  def edit
    @user = current_business.users.find(params[:id])
  end

  def update
    @user = current_business.users.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "User '#{@user.name}' updated"
      if current_user && current_user.admin? || current_user && current_user.super_admin?
        redirect_to users_path
      else
        redirect_to root_path
      end
    else
      render :edit
    end
  end

  def destroy
    @user = current_business.users.find(params[:id])
    if @user.destroy
      flash[:success] = "User deleted"
    else
      flash[:error] = "User could not be deleted"
    end
    redirect_to users_path
  end

  def sign_in
    redirect_to new_user_path if current_business.users.count.zero?
    @title = "Sign in"
    @user = current_business.users.new
  end

  def sign_in_submit
    @user = current_business.users.find_by(name: user_params[:name])
    if @user && @user.authenticate(user_params[:password])
      session[:user_id] = @user.id
      redirect_to root_path
    else
      @user = current_business.users.new
      @failed_sign_in = true
      render :sign_in
    end
  end




  def sign_out_user
    session.delete(:user_id)
    sign_out :business
    redirect_to root_path
  end

private

  def check_correct_user
    unless current_user.admin? || current_user.super_admin? || current_user.id == params[:id].to_i
      deny_access
    end
  end

  def user_params
    if current_user && current_user.super_admin? || current_user && current_user.admin? 
      params[:user].permit(:name, :password, :password_confirmation, :role)
    else
      params[:user].permit(:name, :password, :password_confirmation)
    end
  end

end
