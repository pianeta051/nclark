class TargetsController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action {check_user_role(['Super_Admin','Admin'])}

  def index
    @work_list_categories = current_business.work_list_categories
  end

  def show
    @work_list_category = current_business.work_list_categories.find(params[:id])
    from_date = params[:from_date] || @work_list_category.balance_date_from.to_s
    @work_lists = @work_list_category.work_lists.in_date_range(from_date, params[:to_date]).by_date(:desc).to_a
    begin
      to_date = Date.parse(params[:to_date])
      while to_date.wday != 0
        @work_lists.prepend(nil)
        to_date += 1.day
      end
    rescue
    end
    begin
      from_date = Date.parse(from_date)
      while from_date.wday != 1
        @work_lists.append(nil)
        from_date -= 1.day
      end
    rescue
    end
  end
end
