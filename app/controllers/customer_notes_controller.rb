class CustomerNotesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!

  def create
    @customer = current_business.customers.find(params[:customer_id])
    @customer_note = @customer.customer_notes.build(customer_note_params)
    @success = @customer_note.save
  end

  def destroy
    @customer = current_business.customers.find(params[:customer_id])
    @customer_note = @customer.customer_notes.find(params[:id])
    @success = @customer_note.destroy
  end

private
  def customer_note_params
    params[:customer_note].merge(user_id: session[:user_id]).permit(:content, :date, :user_id)
  end

end
