class DefaultCustomerDetailsController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action {check_user_role(['Super_Admin','Admin'])}

  def new
    @default_customer_detail = current_business.default_customer_details.build
  end

  def create
    next_order = (current_business.default_customer_details.last.try(:order) || -1) + 1
    @default_customer_detail = current_business.default_customer_details.build(default_customer_details_params)
    @default_customer_detail.order = next_order
    if @default_customer_detail.save
      flash[:success] = "Default customer detail field created"
      redirect_to edit_business_registration_path
    else
      render :new
    end
  end

  def edit
    @default_customer_detail = current_business.default_customer_details.find(params[:id])
  end

  def update
    @default_customer_detail = current_business.default_customer_details.find(params[:id])
    previous_name = @default_customer_detail.name
    if @default_customer_detail.update_attributes(default_customer_details_params)
      @default_customer_detail.update_customer_details_with_name(previous_name)
      flash[:success] = "Default customer detail field updated"
      redirect_to edit_business_registration_path
    else
      render :edit
    end
  end

  def destroy
    @default_customer_detail = current_business.default_customer_details.find(params[:id])
    if @default_customer_detail.destroy
      flash[:success] = "Default customer detail field destroyed"
    else
      flash[:error] = "Default customer detail field could not be destroyed"
    end
    redirect_to edit_business_registration_path
  end

  def update_ordering
    default_customer_details = current_business.default_customer_details.to_a
    order_column = params[:work_list_order] == 'true' ? :work_list_order : :order
    params[:ordering].each do |id, order|
      next if id.blank? || order.blank?
      default_customer_detail = default_customer_details.find { |dcd| dcd.id.to_i == id.to_i }
      default_customer_detail.update_column(order_column, order) if default_customer_detail
    end
    render json: {success: true}
  end

  def toggle_visible_in_work_list
    @default_customer_detail = current_business.default_customer_details.find(params[:id])
    @default_customer_detail.update_attribute(:visible_in_work_list, !@default_customer_detail.visible_in_work_list?)
  end

private
  def default_customer_details_params
    params[:default_customer_detail].permit(:name, :large_field, :visible_in_index, :singular, :customer_detail_category_id)
  end

end
