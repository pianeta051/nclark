class CustomersController < ApplicationController
  before_action :authenticate_business!, except: [:show]
  before_action :authenticate_user!
  before_action :get_business_from_public_work_list_tokens, only: [:show]
  before_action :get_title_for_resource_action, except: [:show]
  #before_action :sanitize_customer_customer_details_attributes_params, only: [:create, :update]
  before_action :customer_get_or_set_quick_search_from_cookie, only: [:index]
  before_action :check_user_access_to_customer, except: [:index]
  before_action except: [:show]{check_user_role(['Super_Admin','Admin'])}

  def index
    current_week_id = current_business.current_week.try(:id)
    sort_by_area_from_area_id = false
    if quick_search_params_present?
      @customers = current_business.customers
      if params[:quick_search_term].present?
        @customers = @customers.search_by_customer_detail_value(params[:quick_search_term], params[:quick_search_field])
      end
      if params[:quick_search_area_id].present?
        if params[:quick_search_area_id] == 'sort'
          sort_by_area_from_area_id = true
        else
          @customers = @customers.where(area_id: params[:quick_search_area_id])
        end
      end
      @customers = apply_quick_search_toggles(@customers)
      if params[:quick_search_sort].present? || sort_by_area_from_area_id
        if params[:quick_search_sort] == 'Area'
          @customers = @customers.order('customers.area_name_cache ASC')
        elsif params[:quick_search_sort] == 'outstanding money'
          @customers = @customers.order('customers.outstanding_money_cents DESC')
        else
          @customers = @customers.sort_by(params[:quick_search_sort], 'ASC', 'LAST', false, false, false)
          if sort_by_area_from_area_id
            @customers = @customers.order('customers.area_name_cache ASC')
          end
        end
      end
    else
      @completed_search = current_user.completed_search(CustomerSearchQuery::SEARCH_TYPE_CUSTOMERS)
      @customers = @completed_search.nil? ? current_business.customers.active : @completed_search.result_customers
    end
    @customers = @customers.select("customers.*, culled_jobs.id AS job_id_cache, culled_jobs.work_list_id AS job_in_current_week_work_list_id_cache")
    #start carlos
     join_statement = "LEFT OUTER JOIN (SELECT DISTINCT ON (jobs.week_id, jobs.customer_id) jobs.* FROM jobs) AS culled_jobs ON culled_jobs.customer_id = customers.id"
    unless current_week_id.nil?
      join_statement += " AND culled_jobs.week_id = #{current_week_id}"
    end
    #end carlos
    @customers = @customers.paginate(:page => params[:page], :per_page => 100).joins(join_statement)
      #"LEFT OUTER JOIN (SELECT DISTINCT ON (jobs.week_id, jobs.customer_id) jobs.* FROM jobs) AS culled_jobs ON culled_jobs.customer_id = customers.id AND culled_jobs.week_id = #{current_week_id}")
    @bookmark_customer_ids_cache = current_user.bookmarks.order('bookmarks.bookmarkable_id ASC').pluck(:bookmarkable_id)
    @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
    @title = "Customers (#{current_business.customers.count})"
  end

  def show
    @customer = @business.customers.find(params[:id])
    @customer_details = @customer.customer_details.without_parents.to_a
    default_customer_details = @business.default_customer_details.to_a

    if current_user.team_leader?
      # Sacar la lista de jobs
      joblist = Job.none.to_a
      WorkListCategory.where(user_id: current_user.id).each do |wlc|
        joblist.concat(@customer.jobs.where(work_list_category_id: wlc.id).to_a)
      end

      # deny_access if it is empty
      if joblist.empty?
        deny_access
      end
      # Check if someone if for today
      @full_details = false
      joblist.each do |j|
        if j.date == Date.today || j.date == Date.today+1
          @full_details = true
        end
      end
      # Variable @full_details = true
    elsif current_user.super_admin?
      @full_details = true
    else
      @full_details = false
    end

    at_end_order = 10000
    @customer_details = @customer_details.each do |customer_detail|
      customer_detail.default_customer_detail = default_customer_details.find { |dcd| dcd.name == customer_detail.name }
    end.group_by do |customer_detail|
      customer_detail.default_customer_detail.try(:customer_detail_category).try(:name)
    end.to_a

    @customer_details.each do |key_value|
      key_value[1] = key_value[1].group_by(&:group_index)
    end

    @customer_details.sort! do |a, b|
      (a[1].values.first[0].try(:default_customer_detail).try(:customer_detail_category).try(:order) || at_end_order) <=> (b[1].values.first[0].try(:default_customer_detail).try(:customer_detail_category).try(:order) || at_end_order)
    end

    @customer_details.each do |key, value|
      value.each do |k, v|
        v.sort! do |a, b|
          (a.default_customer_detail.try(:order) || at_end_order) <=> (b.default_customer_detail.try(:order) || at_end_order)
        end
      end
    end

    @extra_customer_details = @customer_details.find { |key_value| key_value[0].nil? }
    if @extra_customer_details
      @extra_customer_details = @extra_customer_details[1].values.flatten
      if @extra_customer_details.any? { |customer_detail| customer_detail.group_index != 0 }
        CustomerDetail.where(id: @extra_customer_details.map(&:id)).update_all(group_index: 0)
      end
    end

    @title = "View #{@customer.owner.present? ? 'property' : 'customer'}"
    @html_title = "
      <span style='background-color:##{@customer.property_hex_color}'>&nbsp;</span>
      View #{@customer.owner.present? ? 'property' : 'customer'}
    "

    unless business_signed_in?
      render layout: 'public'
    end
  end

  def new
    if current_business.areas.count.zero?
      redirect_to new_area_path(return_path: new_customer_path)
    else
      owner = params[:owner_id] ? current_business.customers.where(id: params[:owner_id].to_i).first : nil
      @title = "New property for #{owner.property_identifier}" if owner.present?
      @customer = current_business.customers.build(owner_id: owner.try(:id))
      @customer.build_default_customer_details
      @customer_json = CustomerSerializer.new(@customer, root: false).to_json
    end
  end

  def create
    @customer = current_business.customers.build(customer_params)
    @customer.customer_details_data = params[:customer_details_data]
    if @customer.save
      flash[:success] = "Customer created"
      redirect_to @customer
    else
      @customer.build_default_customer_details
      @customer_json = CustomerSerializer.new(@customer, root: false).to_json
      render :new
    end
  end

  def edit
    @customer = current_business.customers.find(params[:id])
    @customer.build_default_customer_details
    @customer_json = CustomerSerializer.new(@customer, root: false).to_json
  end

  def update
    @customer = current_business.customers.find(params[:id])
    @customer.customer_details_data = params[:customer_details_data]
    if @customer.update_attributes(customer_params)
      flash[:success] = "Customer updated"
      redirect_to @customer
    else
      @customer.build_default_customer_details
      @customer_json = CustomerSerializer.new(@customer, root: false).to_json
      render :edit
    end
  end

  def destroy
    @customer = current_business.customers.find(params[:id])
    if @customer.jobs.count.zero? && @customer.payments.count.zero? && @customer.destroy
      flash[:success] = "Customer deleted"
    else
      flash[:error] = "Customer could not be deleted"
    end
    redirect_to customers_path
  end

  def create_job_for_current_week
    @customer = current_business.customers.find(params[:id])
    week = current_business.current_week

    if week.nil?
      week = current_business.weeks.build(start_date: Date.today.beginning_of_week)
      unless week.save
        @success = false
        return
      end
      WeekGenerator.generate_work_lists_for_week(current_business, week)
    end

    if @customer.jobs.where(week_id: week.id).count.zero?
      job = current_business.jobs.build
      job.customer = @customer
      job.week = week
      job.assign_attributes_from_customer(@customer, current_business)
      @success = job.save
    else
      @success = false
    end
  end

  def activate
    @customer = current_business.customers.find(params[:id])
    @customer.activate!
    redirect_to request.referrer
  end

  def deactivate
    @customer = current_business.customers.find(params[:id])
    @customer.deactivate!
    redirect_to request.referrer
  end

  def clear_quick_search_params
    if request.referrer['customers']
      delete_quick_search_cookie(:customers)
    elsif request.referrer['weeks']
      delete_quick_search_cookie(:weeks)
    end
    redirect_to request.referrer[0...request.referrer.index('?')]
  end

private
  def customer_params
    params[:customer].permit(Customer::PARAMS_TO_PERMIT)
  end

=begin
  def sanitize_customer_customer_details_attributes_params
    if params[:customer][:customer_details_attributes]
      remove_blank_customer_details(params[:customer][:customer_details_attributes])
    end
  end

  def remove_blank_customer_details(customer_details_params)
    default_customer_details = current_business.default_customer_details.pluck(:name)
    customer_detail_indexes_to_remove = []
    customer_details_params.each do |i, attrs|
      remove_customer_detail = true
      if (default_customer_details.include?(attrs['name']) && attrs['value'].blank?) || (attrs['value'].blank? && attrs['parent_id'].present?)
        if attrs['id'].present?
          attrs['_destroy'] = '1'
          remove_customer_detail = false
        end
      else
        attrs.each do |key, value|
          next if key == '_destroy' || key == 'extra_detail'
          if value.present?
            remove_customer_detail = false
            break
          end
        end
      end
      if remove_customer_detail
        customer_detail_indexes_to_remove << i
      end
    end

    customer_details_params.delete_if { |key, value| customer_detail_indexes_to_remove.include? key }
  end
=end

  def get_business_from_public_work_list_tokens
    if business_signed_in?
      @business = current_business
    elsif params[:t1].present? && params[:t2].present? && params[:job_id].present?
      @job = Job.find(params[:job_id])
      if @job.customer_id == params[:id].to_i && @job.work_list && @job.work_list.token_1 == params[:t1] && @job.work_list.token_2 == params[:t2]
        @business = @job.business
      else
        authenticate_business!
      end
    else
      authenticate_business!
    end
  end

  def customer_get_or_set_quick_search_from_cookie
    get_or_set_quick_search_from_cookie(:customers)
  end

  def check_user_access_to_customer
    #@customer = current_business.customers.find(params[:id])
    unless current_user && (current_user.super_admin? || current_user.admin? || current_user.team_leader?)
      flash[:error] = "You're not authorized"
      redirect_to root_path
    end
  end

end
