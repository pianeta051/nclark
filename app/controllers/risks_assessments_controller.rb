class RisksAssessmentsController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :set_risks_assessment, only: [:show, :edit, :update, :destroy, :check_assessment]
  before_action :set_user, only: [:create, :update]
  before_action :set_customer, only: [:create, :new, :edit, :index]
  before_action :set_workList, only: [:show, :index, :new, :check_assessment]
  # GET /risks_assessments
  # GET /risks_assessments.json
 def index
   @risks_assessments = RisksAssessment.where(customer_id:@customer)
   @customer_with_risk_assessment = @risks_assessments.first
 end
  # GET /risks_assessments/1
  # GET /risks_assessments/1.json
  def show

  end

  # GET /risks_assessments/new
  def new
    @risks_assessment = RisksAssessment.new
  end

  # GET /risks_assessments/1/edit
  def edit
  end

  # POST /risks_assessments
  # POST /risks_assessments.json
  def create
    @risks_assessment = RisksAssessment.new(risks_assessment_params)
    @risks_assessment.customer_id = @customer.id
    @risks_assessment.operative = current_user.name
    @risks_assessment.user_id = current_user.id
    @risks_assessment.date = Time.now.strftime("%d %b %Y %k:%M")
    @customer.customer_details_cache["Address Line"]
    respond_to do |format|
      if @risks_assessment.save
        format.html { redirect_to work_list_path(@risks_assessment.worklist_id), notice: 'Risks assessment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @risks_assessment }
        @risks_assessment.accept(current_user.id)
      else
        format.html { render action: 'new' }
        format.json { render json: @risks_assessment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /risks_assessments/1
  # PATCH/PUT /risks_assessments/1.json
  def update
    @risks_assessment.operative= current_user.name
    respond_to do |format|
      if @risks_assessment.update(risks_assessment_params)
        format.html { redirect_to customer_path(@risks_assessment.customer), notice: 'Risks assessment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @risks_assessment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /risks_assessments/1
  # DELETE /risks_assessments/1.json
  def destroy
    @risks_assessment.destroy
    respond_to do |format|
      format.html { redirect_to customer_path(@risks_assessment.customer), notice: 'Risks assessment was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def check_assessment
    RisksUser.create(risks_assessment_id:@risks_assessment.id, user_id:current_user.id, risk_cheacked:true, date:Time.now.strftime("%d %b %Y %k:%M"))
      redirect_to customer_risks_assessment_path(@risks_assessment.customer, @risks_assessment, :workList => @work_list), success:"Thanks for checking this risk assessment."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_risks_assessment
      @risks_assessment = RisksAssessment.find(params[:id])
    end

    def set_user
      @user = User.find(session[:user_id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def risks_assessment_params
      params.require(:risks_assessment).permit(:yes,:no, :not_all, :too_high, :ground_not_safe, :no_clear_access_to_window, :work_from_inside, :footed_ladder, :folded_ladder, :read_and_wash, :secured_ladder, :work_positioning_system, :bar, :other, :why, :operative, :date, :customer_id, :worklist_id)
    end

    def set_workList
      @work_list = params[:workList]
    end
end
