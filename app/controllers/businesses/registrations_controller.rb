class Businesses::RegistrationsController < Devise::RegistrationsController
	before_action only: [:edit] do 
		check_user_role(['Super_Admin'])
	end
end