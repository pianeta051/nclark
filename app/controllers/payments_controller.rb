class PaymentsController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :get_title_for_resource_action
  before_action {check_user_role(['Super_Admin','Admin'])}

  def new
    @customer = current_business.customers.find(params[:customer_id])
    @job = @customer.all_jobs(false, false) { |j| j.where(id: params[:job_id]) }.first
    @payment = @customer.payments.build(date: Date.today, job_id: @job.try(:id), cents: @job.try(:cents))
  end

  def create
    @customer = current_business.customers.find(params[:customer_id])
    @payment = @customer.payments.build(payment_params)
    @success = @payment.save
    if @success
      if params[:redirect_to_edit_job_id].present?
        @job = @customer.jobs.where(id: params[:redirect_to_edit_job_id].to_i).first
        @job.job_payments.create(payment: @payment, cents: @payment.cents) if @job
      end
      update_job_payments
    end
    respond_to do |format|
      format.html do
        if @success
          flash[:success] = "Payment created"
          if params[:redirect_to_finances].present?
            @week = current_business.weeks.where(id: params[:redirect_to_finances]).first
            if @week.present?
              redirect_to finance_path(@week)
            else
              redirect_to @customer
            end
          else
            redirect_to @customer
          end
        else
          render :new
        end
      end
      format.js
    end
  end

  def edit
    @customer = current_business.customers.find(params[:customer_id])
    @payment = @customer.payments.find(params[:id])
  end

  def update
    @customer = current_business.customers.find(params[:customer_id])
    @payment = @customer.payments.find(params[:id])
    if @payment.update_attributes(update_payment_params)
      update_job_payments
      flash[:success] = "Payment updated"
      redirect_to @customer
    else
      render :edit
    end
  end

  def destroy
    @customer = current_business.customers.find(params[:customer_id])
    @payment = @customer.payments.find(params[:id])
    job_ids_from_job_payments = @payment.job_payments.pluck(:job_id)
    puts "YOBEN!!!"
    puts job_ids_from_job_payments.inspect
    @success = @payment.destroy
    Job.where(id: job_ids_from_job_payments).each(&:update_finances_cache_for_week)
    respond_to do |format|
      format.html do
        if @success
          flash[:success] = "Payment deleted"
        else
          flash[:errror] = "Payment could not be deleted"
        end
        redirect_to @customer
      end
      format.js do
        if params[:edit_job_id].present?
          @job = current_business.jobs.find(params[:edit_job_id])
          render 'jobs/delete_payment_in_edit'
        else
          render nothing: true
        end
      end
    end
  end

private
  def payment_params
    params[:payment].permit(:date, :method, :amount, :credit, :job_id)
  end

  def update_payment_params
    params[:payment].permit(:date, :method, :amount, :credit)
  end

  def update_job_payments

    return if params[:job_payment_distribution].blank?
    params[:job_payment_distribution].each do |k, v|
      amount = v.to_money('GBP')
      @job = current_business.jobs.where(id: k).first
      next if @job.nil?
      if amount.cents <= 0
        JobPayment.where(job_id: @job.id, payment_id: @payment.id).delete_all
      elsif JobPayment.where(job_id: @job.id, payment_id: @payment.id).update_all(cents: amount.cents).zero?
        @job.job_payments.create(payment: @payment, cents: amount.cents)
      end
    end
  end

end
