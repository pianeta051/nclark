class WorkListsController < ApplicationController
  before_action :authenticate_business!, except: [:show]
  before_action :authenticate_user!
  #before_action :check_user_role
  before_action {check_user_role(['Super_Admin','Admin','Team_leader', 'Cleaner'])}
  #before_action :check_if_showable
  before_action :check_visibility, only: [:show]

  def show
    if business_signed_in?
      @work_list = current_business.work_lists.find(params[:id])
      @work_list_default_customer_details_names = current_business.work_list_default_customer_details.work_list_order.pluck(:name)
      get_title
    elsif params[:t1].present? && params[:t2].present?
      @work_list = WorkList.find(params[:id])
      unless @work_list.token_1 == params[:t1] && @work_list.token_2 == params[:t2]
        return redirect_to root_path
      end
      @work_list_default_customer_details_names = @work_list.work_list_category.business.work_list_default_customer_details.work_list_order.pluck(:name)
      get_title
      render layout: 'public'
    else
      return redirect_to root_path
    end
  end

  def view
    @work_list = current_business.work_lists.where(:week_id => params[:week_id], :work_list_category_id => params[:work_list_category_id], :day => params[:day]).first
    render nothing: true unless @work_list
  end

  def update_tag
    @work_list = current_business.work_lists.find(params[:id])
    success = @work_list.update_attributes(params.permit(:tag))
    render json: {
      success: success,
      full_name: @work_list.full_name
    }
  rescue
    render json: { success: false }
  end

  def move_jobs
    @work_list = current_business.work_lists.find(params[:id])
    job_ids_to_remove = []
    if params[:job_ids]
      @work_list.jobs.select { |job| params[:job_ids].include?(job.id.to_s) }.each do |job|
        attributes = {
          date: params[:date],
          work_list_category_id: params[:work_list_category_id]
        }
        if params[:date].blank? && params[:day].present?
          attributes.delete(:date)
          attributes[:day] = params[:day]
        end
        if job.update_attributes(attributes)
          if job.work_list_id != @work_list.id
            job_ids_to_remove << job.id
          end
        end
      end
      verify_job_ordering
    end

    render json: {job_ids_to_remove: job_ids_to_remove}
  end

  def mark_as_completed
    @work_list = current_business.work_lists.find(params[:id])
    @work_list.mark_as_completed!
    respond_to do |format|
      format.html { redirect_to @work_list }
      format.js
    end
  end

  def mark_as_incompleted
    @work_list = current_business.work_lists.find(params[:id])
    @work_list.mark_as_incompleted!
    redirect_to @work_list
  end

  def customer_search
    if params[:search_term].blank? || params[:search_term].length < 3
      render nothing: true
    else
      @work_list = current_business.work_lists.find(params[:id])
      @customers = current_business.customers.search_by_customer_detail_value(params[:search_term], nil)
      @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
    end
  end

  def add_job_for_customer
    @work_list = current_business.work_lists.find(params[:id])
    @customer = current_business.customers.find(params[:customer_id])

    if @customer.jobs.untouched.count.zero?
      @job = current_business.jobs.build
      @job.customer = @customer
      @job.assign_attributes_from_customer(@customer, current_business)
    else
      @job = nil
      job_date_diff = nil
      @customer.jobs.untouched.find_each do |job|
        if @job.nil?
          @job = job
          job_date_diff = (@work_list.date - (job.date || job.week.start_date)).to_i.abs
        else
          new_job_date_diff = (@work_list.date - (job.date || job.week.start_date)).to_i.abs
          if new_job_date_diff < job_date_diff
            @job = job
            job_date_diff = new_job_date_diff
          end
        end
      end
    end

    @job.week = @work_list.week
    @job.work_list = @work_list
    @job.work_list_category = @work_list.work_list_category
    @job.day = @work_list.day

    @success = @job.save
  end

  def update_target

    @work_list = current_business.work_lists.find(params[:id])
    @work_list.update_attributes(params[:work_list].permit(:target, :visible, :publication_date, :publication_time))
    user_ids = params[:work_list][:user_ids]
    @work_list.work_list_workers.delete_all
    user_ids.each do |user_id|
      unless user_id.blank?
        cleaner = User.find(user_id)
        WorkListWorker.create(user: cleaner, work_list: @work_list)
      end
    end
    redirect_to @work_list
  end

  def update_job_ordering
    @work_list = current_business.work_lists.find(params[:id])
    jobs = @work_list.jobs.to_a
    params[:ordering].each do |id, order|
      next if id.blank? || order.blank?
      job = jobs.find { |job| job.id.to_i == id.to_i }
      job.update_column(:work_list_order, order) if job
    end

    verify_job_ordering

    render json: {success: true}
  end

  def reset_job_ordering
    @work_list = current_business.work_lists.find(params[:id])
    reset_jobs_work_list_order
    redirect_to @work_list
  end

  def get_simple_show
    @work_list = current_business.work_lists.find(params[:id])
    @work_list_default_customer_details_names = current_business.work_list_default_customer_details.work_list_order.pluck(:name)
  end

private
  def get_title
    @title = "Work list for #{@work_list.full_name_with_date}"
  end

  def reset_jobs_work_list_order
    @work_list.jobs.update_all(work_list_order: nil)
  end

  def verify_job_ordering
    time_order_job_ids = @work_list.jobs.time_order.pluck(:id)
    work_list_order_job_ids = @work_list.jobs.work_list_order.pluck(:id)
    reset_jobs_work_list_order if time_order_job_ids == work_list_order_job_ids
  end

  def check_if_showable
    @work_list = current_business.work_lists.find(params[:id])
    unless current_user.hasRoleInList(['Super_Admin', 'Admin']) || @work_list.date <= Date.today

    #|| @work_list.date <= Date.today+1
      deny_access
    end
  end

  def check_visibility
    @work_list = current_business.work_lists.find(params[:id])
    unless @work_list.visible || current_user.hasRoleInList(['Super_Admin', 'Admin'])
      if @work_list.date <= Date.today || @work_list.publication_date < Date.today
        @work_list.update(visible: true)
      elsif @work_list.publication_date == Date.today
        pub_time = @work_list.publication_time.try(:change, {day: Date.today.day, month: Date.today.month, year: Date.today.year})
        if pub_time
          if pub_time <= Time.now
            @work_list.update(visible: true)
          else
            deny_access
          end
        end

      else
        deny_access
      end
    end
  end

end
