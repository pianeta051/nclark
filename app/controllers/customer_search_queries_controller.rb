class CustomerSearchQueriesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!

  def search
    customer_search_query = current_user.customer_search_queries.build(search_terms: params[:groups], search_type_string: request.referrer)
    if customer_search_query.save
      customer_search_query.delay.perform_search!
      if request.referrer['customers']
        delete_quick_search_cookie(:customers)
      elsif request.referrer['weeks']
        delete_quick_search_cookie(:weeks)
      end
      render json: {success: true}
    elsif params[:groups].blank?
      current_user.completed_search(request.referrer).try(:destroy)
      render json: {redirect: request.referrer}
    else
      render json: {}
    end
  end

  def get_search_status
    render json: {
      status: current_user.has_search_pending?(request.referrer) ? 'pending' : 'complete'
    }
  end

  def clear_search
    completed_search = current_user.completed_search(request.referrer)
    completed_search.try(:destroy)
    redirect_to request.referrer
  end

  def remove_customer_from_search_results
    completed_search = current_user.completed_search(request.referrer)
    customer = current_business.customers.find(params[:customer_id])
    if completed_search.try(:remove_customer_from_results, customer)
      render json: {success: true}
    else
      render json: {}
    end
  end

  def export
    completed_search = current_user.completed_search(request.referrer)
    completed_search.try(:start_exporting_xlsx!, params[:export])
    redirect_to request.referrer
  end

  def get_export_status
    pending = current_user.has_export_pending?(request.referrer)
    render json: {
      status: pending ? 'pending' : 'complete',
      url: pending ? nil : current_user.completed_search(request.referrer).xlsx_file.url
    }
  end

end
