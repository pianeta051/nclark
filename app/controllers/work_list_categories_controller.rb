class WorkListCategoriesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :get_title_for_resource_action
  before_action {check_user_role(['Super_Admin','Admin','Team_leader'])}

  def new
    @work_list_category = current_business.work_list_categories.build
  end

  def create
    @work_list_category = current_business.work_list_categories.build(work_list_category_params)
    if @work_list_category.save
      flash[:success] = "Work list category created"
      redirect_to work_list_settings_path
    else
      render :new
    end
  end

  def edit
    @work_list_category = current_business.work_list_categories.find(params[:id])
  end

  def update
    @work_list_category = current_business.work_list_categories.find(params[:id])
    if @work_list_category.update_attributes(work_list_category_params)
      @work_list_category.update_balance!
      flash[:success] = "Work list category updated"
      redirect_to work_list_settings_path
    else
      render :edit
    end
  end

  def destroy
    @work_list_category = current_business.work_list_categories.find(params[:id])
    if @work_list_category.destroy
      flash[:success] = "Work list category deleted"
    else
      flash[:error] = "Work list category could not be deleted"
    end
    redirect_to work_list_settings_path
  end

  def update_balance_date_from
    @work_list_category = current_business.work_list_categories.find(params[:id])
    if @work_list_category.update_attributes(params[:work_list_category].permit(:balance_date_from))
      @work_list_category.update_balance!
    end
    redirect_to request.referrer
  end

private
  def work_list_category_params
    params[:work_list_category].permit(:name, :tag, :target, :balance_date_from, :user_id)
  end

end
