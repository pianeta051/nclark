class ExpensesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :get_title_for_resource_action, except: :index
  before_action {check_user_role(['Super_Admin','Admin'])}

  def index
    @expenses = current_business.expenses
    @found_expenses = []
    @expense_to_highlight = nil
    if params[:search_term].present?
      @found_expenses = @expenses.search(params[:search_term])
      if params[:commit].downcase.include? 'jump'
        if @found_expenses.count == 1
          @expense_to_highlight = @found_expenses.first
        end
      else
        @expenses = @found_expenses
        @found_expenses = []
      end
    end
    @bookmark_user = current_business.users.where(id: params[:bookmark_user]).first || current_user
    @expenses = case params[:search_bookmarked]
    when 'applied'
      @expenses.bookmarked_by(@bookmark_user)
    when 'unapplied'
      @expenses.unbookmarked_by(@bookmark_user)
    else
      @expenses
    end
    @expenses = @expenses.in_date_order
    if @expense_to_highlight
      amount_before = @expenses.where('expenses.date > ?',
                                     @expense_to_highlight.date).count
      amount_before += @expenses.where('expenses.date = ? AND expenses.receipt_number > ?',
                                      @expense_to_highlight.date,
                                      @expense_to_highlight.receipt_number).count
      amount_before += @expenses.where('expenses.date = ? AND expenses.receipt_number = ? AND expenses.created_at > ?',
                                      @expense_to_highlight.date,
                                      @expense_to_highlight.receipt_number,
                                      @expense_to_highlight.created_at).count
      @highlight_page = (amount_before / 100) + 1
    end
    if params[:page_before].to_i != params[:page].to_i
      @highlight_page = nil
    end
    params[:page_before] = params[:page]
    @expenses = @expenses.paginate(page: @highlight_page || params[:page], per_page: 100)
  end

  def show
    @expense = current_business.expenses.find(params[:id])
  end

  def new
    @expense = current_business.expenses.build
  end

  def create
    @expense = current_business.expenses.build(expense_params)
    if @expense.save
      flash[:success] = "Expense created"
      redirect_to expenses_path
    else
      render :new
    end
  end

  def edit
    @expense = current_business.expenses.find(params[:id])
  end

  def update
    @expense = current_business.expenses.find(params[:id])
    if @expense.update_attributes(expense_params)
      flash[:success] = "Expense updated"
      redirect_to(params[:redirect_to] == 'index' ? expenses_path : @expense)
    else
      render :edit
    end
  end

  def destroy
    @expense = current_business.expenses.find(params[:id])
    flash[:success] = "Expense destroyed" if @expense.destroy
    redirect_to expenses_path
  end

private
  def expense_params
    params[:expense].permit(:date, :price, :vat, :description, :receipt_number, :notes, new_attachments: :file, delete_attachments: [:id, :delete])
  end
end
