class WorkListNotesController < ApplicationController
  before_action :authenticate_business!, :except => [:create, :destroy]
  before_action :authenticate_user!

  def create
    work_list_action_with_work_list_tokens do |work_list|
      @work_list_note = work_list.work_list_notes.build(work_list_note_params)
      @success = @work_list_note.save
    end
  end

  def destroy
    work_list_action_with_work_list_tokens do |work_list|
      @work_list_note = work_list.work_list_notes.find(params[:id])
      @success = @work_list_note.destroy
    end
  end

private
  def work_list_note_params
    params[:work_list_note].merge(user_id: session[:user_id]).permit(:content, :user_id)
  end
end
