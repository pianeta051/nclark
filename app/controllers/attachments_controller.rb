class AttachmentsController < ApplicationController
  before_action :authenticate_business!, except: [:create]
  before_action :authenticate_user!

  def create
    @customer = Customer.find(params[:customer_id])
    @attachment = @customer.attachments.build(attachment_params)
    if @attachment.save
      flash[:success] = "Attachment uploaded"
    else
      flash[:error] = "Attachment could not be uploaded"
    end
    if business_signed_in?
      redirect_to @customer
    else
      redirect_to customer_path(@customer, job_id: params[:job_id], t1: params[:t1], t2: params[:t2])
    end
  end

  def destroy
    @customer = Customer.find(params[:customer_id])
    @attachment = @customer.attachments.find(params[:id])
    if @attachment.destroy
      flash[:success] = "Attachment deleted"
    else
      flash[:error] = "Attachment could not be deleted"
    end
    redirect_to @customer
  end

private
  def attachment_params
    params[:attachment].permit(:name, :attachment)
  end
end
