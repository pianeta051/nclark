class AreasController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :get_title_for_resource_action
  before_action {check_user_role(['Super_Admin','Admin'])}

  def index
    @areas = current_business.areas
  end

  def show
    @area = current_business.areas.find(params[:id])
  end

  def new
    @area = current_business.areas.build
  end

  def create
    @area = current_business.areas.build(area_params)
    if @area.save
      flash[:success] = "Area created"
      redirect_to(params[:return_path].present? ? params[:return_path] : areas_path)
    else
      render :new
    end
  end

  def edit
    @area = current_business.areas.find(params[:id])
  end

  def update
    @area = current_business.areas.find(params[:id])
    if @area.update_attributes(area_params)
      flash[:success] = "Area updated"
      redirect_to @area
    else
      render :new
    end
  end

  def destroy
    @area = current_business.areas.find(params[:id])
    if @area.customers.count.zero? && @area.destroy
      flash[:success] = "Area deleted"
    else
      flash[:error] = "Area could not be deleted"
    end
    redirect_to areas_path
  end

private
  def area_params
    params[:area].permit(:name, :color)
  end
end
