class ApplicationController < ActionController::Base
  QUICK_SEARCH_PARAMS_KEYS = %w(quick_search_term quick_search_field quick_search_sort quick_search_area_id)

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user_on_business_edit, if: :devise_controller?
  before_action :check_if_quick_search_params_present

protected

  def check_user_role(authorized_roles)
    if current_user.nil? || !authorized_roles.include?(current_user.role) 
      deny_access
    end
  end

  def deny_access
    flash[:error] = "You're not authorized"
    redirect_to root_path
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation) }
  end

  def get_title_for_resource_action
    @title = case action_name
    when 'new', 'create'
      "New #{controller_name.singularize.humanize.downcase}"
    when 'edit', 'update'
      "Edit #{controller_name.singularize.humanize.downcase}"
    when 'show'
      "View #{controller_name.singularize.humanize.downcase}"
    else
      controller_name.titleize
    end
  end

  def authenticate_user!
    if business_signed_in?
      @current_user = current_business.users.where(id: session[:user_id]).first
      unless @current_user
        session.delete(:user_id) if session[:user_id]
        if current_business.users.count.zero?
          redirect_to new_user_path
        else
          redirect_to sign_in_users_path
        end
      end
    end
  end

  def current_user
    @current_user
  end

  def job_action_with_work_list_tokens(method = nil)
    if business_signed_in?
      @job = current_business.jobs.find(params[:job_id] || params[:id])
    elsif params[:t1].present? && params[:t2].present?
      @job = Job.find(params[:job_id] || params[:id])
      unless @job.work_list && @job.work_list.token_1 == params[:t1] && @job.work_list.token_2 == params[:t2]
        return render nothing: true
      end
    else
      return render nothing: true
    end

    if method.present?
      @job.send(method)
    else
      yield @job
    end
  end

  def work_list_action_with_work_list_tokens(method = nil)
    if business_signed_in?
      @work_list = current_business.work_lists.find(params[:work_list_id] || params[:id])
    elsif params[:t1].present? && params[:t2].present?
      @work_list = WorkList.find(params[:work_list_id] || params[:id])
      unless @work_list && @work_list.token_1 == params[:t1] && @work_list.token_2 == params[:t2]
        return render nothing: true
      end
    else
      return render nothing: true
    end

    if method.present?
      @work_list.send(method)
    else
      yield @work_list
    end
  end

  def quick_search_params_present?
    quick_search_search_params_present? || quick_search_toggle_params_present? || params[:quick_search_sort].present?
  end

  def quick_search_search_params_present?
    params[:quick_search_term].present? || params[:quick_search_area_id].present?
  end

  def quick_search_toggle_params_present?
    ['applied', 'unapplied'].include?(params[:quick_search_bookmarked]) ||
      ['applied', 'unapplied'].include?(params[:quick_search_assigned]) ||
      ['applied', 'unapplied'].include?(params[:quick_search_active]) ||
      ['applied', 'unapplied'].include?(params[:quick_search_owing])
  end

  def apply_quick_search_toggles(customers, week = nil)
    return customers unless quick_search_toggle_params_present?

    quick_search_bookmarked = params[:quick_search_bookmarked]
    quick_search_assigned = params[:quick_search_assigned]
    quick_search_active = params[:quick_search_active]
    quick_search_owing = params[:quick_search_owing]
    if quick_search_bookmarked == 'applied'
      customers = customers.bookmarked_by(current_user)
    elsif quick_search_bookmarked == 'unapplied'
      customers = customers.unbookmarked_by(current_user)
    end
    if quick_search_assigned == 'applied'
      customers = customers.where('customers.id IN (SELECT jobs.customer_id FROM jobs WHERE jobs.week_id = ? AND jobs.work_list_id IS NOT NULL)', week.try(:id) || current_business.current_week.id)
    elsif quick_search_assigned == 'unapplied'
      customers = customers.where(non_regular: false).where('customers.id IN (SELECT jobs.customer_id FROM jobs WHERE jobs.week_id = ? AND jobs.work_list_id IS NULL)', week.try(:id) || current_business.current_week.id)
    end
    if quick_search_active == 'applied'
      customers = customers.active
    elsif quick_search_active == 'unapplied'
      customers = customers.inactive
    end
    if quick_search_owing == 'applied'
      customers = customers.owing
    elsif quick_search_owing == 'unapplied'
      customers = customers.not_owing
    end

    customers
  end

  def get_or_set_quick_search_from_cookie(cookie_name)
    cookie_name = :"#{cookie_name}_quick_search_params"
    if quick_search_params_present?
      cookies[cookie_name] = {
        value: params.select { |key, value| QUICK_SEARCH_PARAMS_KEYS.include?(key) }.to_json,
        expires: 4.years.from_now
      }
    elsif cookies[cookie_name].present?
      if params[:quick_search_reset] == 'true'
        cookies.delete(cookie_name)
      else
        hash = JSON.parse(cookies[cookie_name])
        QUICK_SEARCH_PARAMS_KEYS.each do |key|
          params[key] = hash[key]
        end
      end
    end
  rescue
    cookies.delete(cookie_name)
  end

  def delete_quick_search_cookie(cookie_name)
    cookies.delete(:"#{cookie_name}_quick_search_params")
  end



private
  def authenticate_user_on_business_edit
    if action_name == 'edit'
      authenticate_business!
      authenticate_user!
    end
  end

  def check_if_quick_search_params_present
    @quick_search_params_present = quick_search_params_present?
  end

end
