class BookmarksController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :get_bookmarkable

  def create
    @existing_bookmark = Bookmark.where(user: current_user, bookmarkable: @bookmarkable).first
    @bookmark = Bookmark.new(user: current_user, bookmarkable: @bookmarkable)
    if @existing_bookmark.present? || @bookmark.save
      render json: { success: true }
    else
      render json: {}
    end
  end

  def destroy
    @bookmark = Bookmark.where(user: current_user, bookmarkable: @bookmarkable).first
    if @bookmark.nil? || @bookmark.destroy
      render json: { success: true }
    else
      render json: {}
    end
  end

private
  def get_bookmarkable
    @bookmarkable = begin
      case params[:bookmarkable_type].downcase
      when 'customer'
        current_business.customers.find(params[:bookmarkable_id])
      when 'expense'
        current_business.expenses.find(params[:bookmarkable_id])
      else
        nil
      end
    rescue
      nil
    end
    unless @bookmarkable.present?
      render json: {}
    end
  end
end
