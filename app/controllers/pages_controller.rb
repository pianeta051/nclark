class PagesController < ApplicationController
  before_action :authenticate_business!, except: [:home]
  before_action :authenticate_user!
    before_action only: [:work_list_settings] do 
    check_user_role(['Super_Admin','Admin'])
  end

  def home
  end

  def work_list_settings
    @title = "Work list settings"
    @default_customer_details = current_business.default_customer_details.order('default_customer_details.work_list_order ASC')
    @work_list_categories = current_business.work_list_categories
  end
  
end
