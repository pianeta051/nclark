class FinancesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action {check_user_role(['Super_Admin','Admin'])}

  def index
    @total_paid_cents = current_business.payments
                                        .in_date_range(params[:from_date], params[:to_date])
                                        .not_credit
                                        .pluck(:cents, :charge_cents)
                                        .inject(0) { |t, p| t + p[0] + p[1] }
    @total_expenses_cents = current_business.expenses
                                            .in_date_range(params[:from_date], params[:to_date])
                                            .pluck(:cents, :vat_cents)
                                            .inject(0) { |t, p| t + p[0] + p[1] }
  end

  def weeks
    @weeks = current_business.weeks.in_date_range(params[:from_week], params[:to_week]).order('weeks.start_date DESC')
    if params[:invoice_number].present?
      @jobs = current_business.jobs.where('jobs.invoice_number ILIKE ?', "%#{params[:invoice_number]}%")
      get_index_default_customer_details_names
    end

    if @weeks.count.zero?
      render :no_finances
    end
  end

  def show
    @week = current_business.weeks.find(params[:id])
    @title = "Finances for week starting #{@week.start_date.to_s(:long)}"
    get_index_default_customer_details_names
  end

  def export
    
    finance_export = current_user.finance_exports.build(terms: params[:export])
    if finance_export.save
      finance_export.delay.perform_export!
    else
      flash[:error] = "Export could not be started (#{finance_export.errors.full_messages.join('; ')})"
    end
    redirect_to request.referrer.present? ? request.referrer : finances_path
  end

  def get_export_status
    if current_user.finance_exports.count > 0
      pending = current_user.has_finances_export_pending?
      render json: {
        status: pending ? 'pending' : 'complete',
        url: pending ? nil : current_user.completed_finance_export.xlsx_file.url
      }
    else
      render json: {
        status: 'none',
        url: nil
      }
    end
  end

private
  def get_index_default_customer_details_names
    @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
  end

end
