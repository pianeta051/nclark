class BusinessesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action {check_user_role(['Super_Admin'])}

  def update
    current_business.update_attributes(business_params)
    redirect_to edit_business_registration_path
  end

  def toggle_work_list_payment_method
    @business = current_business

    array = @business.work_list_payment_methods_array
    if array.include?(params[:payment_method])
      array.reject! { |el| el == params[:payment_method] }
    else
      array << params[:payment_method]
    end

    @business.work_list_payment_methods_array = array
    @business.save
  end

private
  def business_params
    params[:business].permit(:cycle_length, :default_vat_rate, :customer_color_field_name, :debit_charge, :credit_charge, :debit_charge_is_percentage, :credit_charge_is_percentage, :default_publication_time)
  end
end
