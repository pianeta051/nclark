class WeeksController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!
  before_action :get_title_for_resource_action
  before_action :week_get_or_set_quick_search_from_cookie, only: [:show]
  before_action :check_if_showable, except: [:index, :generate_cycle]
 # before_action :check_user_role
  before_action except: [:work_lists, :show] do
    check_user_role(['Super_Admin','Admin','Team_leader'])
  end
  before_action only: [:work_lists] do
    check_user_role(['Super_Admin','Admin'])
  end

  def index
    @weeks = current_business.weeks.order('weeks.start_date DESC')
  end

  def show
    @week = current_business.weeks.find(params[:id])
    @jobs = @week.jobs.unlinked
    sort_by_area_from_area_id = false
    if quick_search_params_present?
      @customers = current_business.customers
      if quick_search_search_params_present? || quick_search_toggle_params_present?
        if params[:quick_search_term].present?
          @customers = @customers.search_by_customer_detail_value(params[:quick_search_term], params[:quick_search_field])
        end
        if params[:quick_search_area_id].present?
          if params[:quick_search_area_id] == 'sort'
            sort_by_area_from_area_id = true
          else
            @customers = @customers.where(area_id: params[:quick_search_area_id])
          end
        end
        @customers = apply_quick_search_toggles(@customers, @week)
        @jobs = @jobs.where(customer_id: @customers.where(id: @week.jobs.pluck(:customer_id)).pluck(:id))
      end

      @jobs = @jobs.includes(:customer).order('jobs.id ASC')
      if params[:quick_search_sort].present? || sort_by_area_from_area_id
        if params[:quick_search_sort] == 'Area'
          @jobs = @jobs.order('customers.area_name_cache ASC')
        elsif params[:quick_search_sort] == 'outstanding money'
          @jobs = @jobs.order('customers.outstanding_money_cents DESC')
        else
          @jobs = @jobs.order(Customer.sort_by_query(params[:quick_search_sort], 'ASC', 'LAST', false, false, false))
          if sort_by_area_from_area_id
            @jobs = @jobs.order('customers.area_name_cache ASC')
          end
        end
      end
    else
      @completed_search = current_user.completed_search(CustomerSearchQuery::SEARCH_TYPE_JOBS)
      if @completed_search.nil?
        @jobs = @jobs.includes(:customer).order('jobs.id ASC')
      else
        @jobs = @jobs.where("
          jobs.customer_id IN (
            SELECT customer_search_query_customers.customer_id
            FROM customer_search_query_customers
            WHERE customer_search_query_customers.query_id = ?
          )
        ", @completed_search.id).order("
          (
            SELECT customer_search_query_customers.order
            FROM customer_search_query_customers
            WHERE customer_search_query_customers.query_id = #{@completed_search.id}
            AND customer_search_query_customers.customer_id = jobs.customer_id
          ) ASC
        ")
      end
    end
    @jobs = @jobs.paginate(page: params[:page], per_page: 100)

    @bookmark_customer_ids_cache = current_user.bookmarks.order('bookmarks.bookmarkable_id ASC').pluck(:bookmarkable_id)

    @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
    @title = "Week starting #{@week.start_date.to_s(:long)}"
    @title << " - #{@week.tag}" if @week.tag.present?
  end

  def edit
    @week = current_business.weeks.find(params[:id])
  end

  def update
    @week = current_business.weeks.find(params[:id])
    if @week.update_attributes(params[:week].permit(:tag))
      if params[:commit]["index"]
        redirect_to weeks_path
      else
        redirect_to @week
      end
    else
      render :edit
    end
  end

  def work_lists
    @week = current_business.weeks.find(params[:id])
    @work_list_default_customer_details_names = current_business.work_list_default_customer_details.work_list_order.pluck(:name)
    @work_lists = @week.work_lists.includes(:work_list_category).order('work_lists.day ASC, work_lists.work_list_category_id ASC')
    @title = "Work lists for week starting #{@week.start_date.to_s(:long)}"
  end

  def generate_cycle
    if current_business.work_list_categories.blank?
      flash[:error] = "Work list categories must be added before generating cycle"
      redirect_to work_list_categories_path
    else
      start_date = Date.today.beginning_of_week
      current_business.cycle_length.times do |i|
        WeekGenerator.generate_week_for(current_business, start_date)
        start_date += 1.week
      end
      redirect_to weeks_path
    end
  end

  def pull_work
    @week = current_business.weeks.find(params[:id])
    WeekShifter.pull_week_work_one_week(current_business, @week)
    WeekShifter.pull_customer_start_dates_one_week(current_business)
    flash[:success] = "Future unfinished work pulled to this week"
    redirect_to @week
  end

  def push_work
    @week = current_business.weeks.find(params[:id])
    WeekShifter.push_week_work_one_week(current_business, @week)
    WeekShifter.push_customer_start_dates_one_week(current_business)
    flash[:success] = "Unfinished work pushed to future weeks"
    redirect_to @week
  end

  def update_time
    @week = current_business.week.find(params[:id])
    if @week.update_attributes(params[:week].permit(:visible_work_list))
      @week.update_balance!
    end
    redirect_to request.referrer
  end
=begin

rescue Exception => e

end
  def week_per_user
    td{class: work_list.html_classes, data: {work_list_id: work_list.id}}
      span.td-content
        span.full-name
           link_to work_list.full_name, work_list
         if current_user && (current_user.admin? || current_user.super_admin?)
           link_to '#', class: 'edit-work-list-tag', title: 'Edit tag' do
            span.icon.icon-pencil
      span.edit-work-list-tag-field.input-append.hide
         text_field_tag :tag, work_list.tag
         button_tag class: 'btn', type: 'button', data: {work_list_id: work_list.id} do
          span.icon.icon-ok
       if work_list.apply_target? && work_list.target_met?
        span.icon.icon-star.target-met-icon
  end
=end

  private
    def week_get_or_set_quick_search_from_cookie
      get_or_set_quick_search_from_cookie(:weeks)
    end

    def check_if_showable
      @week = current_business.weeks.find(params[:id])
      unless current_user.hasRoleInList(['Super_Admin', 'Admin', 'Team_leader', 'Cleaner']) ||  @week.start_date<= Date.today
        deny_access
      end
    end

end
