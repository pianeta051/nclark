class JobNotesController < ApplicationController
  before_action :authenticate_business!, :except => [:create, :destroy]
  before_action :authenticate_user!

  def create
    job_action_with_work_list_tokens do |job|
      @job_note = @job.job_notes.build(job_note_params)
      @work_list = job.work_list
      @work_list_default_customer_details_names = job.business.work_list_default_customer_details.work_list_order.pluck(:name)
      @success = @job_note.save
    end
  end

  def destroy
    job_action_with_work_list_tokens do |job|
      @job_note = @job.job_notes.find(params[:id])
      @work_list = job.work_list
      @work_list_default_customer_details_names = job.business.work_list_default_customer_details.work_list_order.pluck(:name)
      @success = @job_note.destroy
    end
  end

private
  def job_note_params
    params[:job_note].merge(user_id: session[:user_id]).permit(:content, :user_id)
  end

end
