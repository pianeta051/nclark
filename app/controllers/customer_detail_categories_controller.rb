class CustomerDetailCategoriesController < ApplicationController
  before_action :authenticate_business!
  before_action :authenticate_user!

  def new
    @customer_detail_category = current_business.customer_detail_categories.build
  end

  def create
    next_order = (current_business.customer_detail_categories.last.try(:order) || -1) + 1
    @customer_detail_category = current_business.customer_detail_categories.build(customer_detail_categories_params)
    @customer_detail_category.order = next_order
    if @customer_detail_category.save
      flash[:success] = "Customer detail category created"
      redirect_to edit_business_registration_path
    else
      render :new
    end
  end

  def edit
    @customer_detail_category = current_business.customer_detail_categories.find(params[:id])
  end

  def update
    @customer_detail_category = current_business.customer_detail_categories.find(params[:id])
    if @customer_detail_category.update_attributes(customer_detail_categories_params)
      flash[:success] = "Customer detail category updated"
      redirect_to edit_business_registration_path
    else
      render :edit
    end
  end

  def destroy
    @customer_detail_category = current_business.customer_detail_categories.find(params[:id])
    if @customer_detail_category.default_customer_details.count.zero?
      if @customer_detail_category.destroy
        flash[:success] = "Customer detail category destroyed"
      else
        flash[:error] = "Customer detail category could not be destroyed"
      end
    else
      flash[:error] = "Customer detail category could not be destroyed (some default customer details fields are assigned to this category)"
    end
    redirect_to edit_business_registration_path
  end

  def update_ordering
    customer_detail_categories = current_business.customer_detail_categories.to_a
    params[:ordering].each do |id, order|
      next if id.blank? || order.blank?
      customer_detail_category = customer_detail_categories.find { |cdc| cdc.id.to_i == id.to_i }
      customer_detail_category.update_column(:order, order) if customer_detail_category
    end
    render json: {success: true}
  end

private
  def customer_detail_categories_params
    params[:customer_detail_category].permit(:name)
  end

end
