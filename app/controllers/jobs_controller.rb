class JobsController < ApplicationController
  before_action :authenticate_business!, :except => [
    :mark_as_completed, :unmark_as_completed, :mark_as_paid, :mark_as_risk_checked, :update_notes, :update_price, :update_invoice_number
  ]
  before_action :authenticate_user!
  before_action :get_title_for_resource_action
  before_action except: [:mark_as_completed, :unmark_as_completed, :mark_as_paid, :mark_as_risk_checked, :update_price, :update_notes] do
    check_user_role(['Super_Admin','Admin'])
  end

  def new
    @job = current_business.jobs.build
    @job.customer = current_business.customers.find_by_id(params[:customer_id])
    if @job.customer.nil?
      redirect_to customers_path
    else
      @job.assign_attributes_from_customer(@job.customer, current_business)
      @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
    end
  end

  def create
    @job = current_business.jobs.build(new_job_params)
    @job.customer = current_business.customers.find_by_id(@job.customer_id)
    handle_week_starting
    if @job.save
      if params[:initial_note].present?
        @job.job_notes.create(user_id: current_user.id, content: params[:initial_note])
      end
      #handle_payment_updating
      return_to, fail_message = get_return_to_for_job(@job)
      flash[:notice] = fail_message if fail_message.present?
      redirect_to return_to
    else
      @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
      render :new
    end
  end

  def quick_new
    @job = current_business.jobs.build
    begin
      @job.date = params[:date].to_date
    rescue
    end
    @job.work_list_category = current_business.work_list_categories.find_by_id(params[:work_list_category_id])
    @customer = current_business.customers.build
    @customer.build_default_customer_details
  end

  def quick_create
    @customer = current_business.customers.build(customer_params)
    @customer.set_customer_details_data_from_typical_params(params[:customer_details_data])
    @job = current_business.jobs.build(new_job_params)
    handle_week_starting
    if @customer.save
      @job.customer = @customer
      if @job.save
        return redirect_to(@job.work_list || @job.week)
      else
        @customer.destroy
      end
    end

    @customer.build_default_customer_details
    render :quick_new
  end

  def edit
    @job = current_business.jobs.find(params[:id])
    @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
  end

  def update
    @job = current_business.jobs.find(params[:id])
    work_list_before = @job.work_list

    if work_list_before && work_list_before.jobs.length == 1
      work_list_without_jobs_id = work_list_before.id
    else
      work_list_without_jobs_id = nil
    end

    handle_week_starting

    if @job.update_attributes(job_params)
      #handle_payment_updating

      work_list_after = @job.work_list
      if work_list_after
        work_list_with_jobs_id = work_list_after.id
      else
        work_list_with_jobs_id = nil
      end

      respond_to do |format|
        format.html do
          return_to, fail_message = get_return_to_for_job(@job)
          flash[:success] = "Job updated"
          flash[:success] << " (#{fail_message})" if fail_message.present?
          redirect_to return_to
        end
        format.json {
          week = current_business.weeks.where(id: params[:week_id]).first
          render json: {
            success: true,
            current_week_job_html_classes: @job.customer.current_week_job_html_classes(week_override: week),
            work_list_with_jobs_id: work_list_with_jobs_id,
            work_list_without_jobs_id: work_list_without_jobs_id
          }
        }
      end
    else
      @index_default_customer_details_names = current_business.index_default_customer_details.pluck(:name)
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: {success: false, job: @job} }
      end
    end
  end

  def destroy
    @job = current_business.jobs.find(params[:id])
    return_to = params[:return_path] || @job.week

    if @job.destroy
      flash[:success] = "Job deleted"
    else
      flash[:error] = "Job could not be deleted"
    end
    redirect_to return_to
  end

  def mark_as_completed
    job_action_with_work_list_tokens(&:mark_as_completed!)
  end

  def unmark_as_completed
    job_action_with_work_list_tokens(&:unmark_as_completed!)
  end

  def mark_as_risk_checked
    job_action_with_work_list_tokens(&:mark_as_risk_checked!)
    redirect_to work_list_path(params[:worklist])
  end
    
  def unmark_as_risk_checked
    job_action_with_work_list_tokens(&:unmark_as_risk_checked!)
  end


  def mark_as_paid
    job_action_with_work_list_tokens do |job|
      job = job.linked_job if job.linked?
      raise "Cannot create a payment for a job that already has a payment" if job.payments.count > 0
      payment = job.customer.payments.build(date: Date.today,
                                           method: params[:job][:payment_method],
                                           amount: params[:job][:payment_amount])
      if payment.save
        job.job_payments.create(payment: payment, cents: payment.cents)
        job.reload
      end
    end
  end

  def update_notes
    job_action_with_work_list_tokens do |job|
      job.update_attributes(params[:job].permit(:notes))
      @index_default_customer_details_names = job.business.index_default_customer_details.pluck(:name)
    end
  end

  def update_price
    job_action_with_work_list_tokens do |job|
      if params[:price_excluding_vat].present?
        success = job.update_cents_excluding_vat(Money.parse(params[:price_excluding_vat]).cents)
      elsif params[:price_including_vat].present?
        success = job.update_cents_including_vat(Money.parse(params[:price_including_vat]).cents)
      end
      render json: {
        success: success,
        price_excluding_vat: job.price_excluding_vat(true).to_s,
        price_vat: job.vat(true).to_s,
        price_including_vat: job.price_including_vat.to_s,
        customer_outstanding_money: job.customer.outstanding_money.to_s
      }
    end
  rescue
    render json: { success: false }
  end

  def update_invoice_number
    job_action_with_work_list_tokens do |job|
      success = job.update_attributes(params.permit(:invoice_number))
      render json: {
        success: success,
        invoice_number: job.invoice_number
      }
    end
  rescue
    render json: { success: false }
  end

private
  def job_params
    params[:job].permit(:price, :date, :day, :notes, :completed_at, :paid_at, :payment_amount, :payment_method, :work_list_category_id, :vat_rate, :invoice_number, :start_time, :finish_time)
  end

  def new_job_params
    params[:job].permit(:customer_id, :price, :date, :day, :notes, :completed_at, :paid_at, :payment_amount, :payment_method, :work_list_category_id, :vat_rate, :invoice_number, :start_time, :finish_time)
  end

  def customer_params
    params[:customer].permit(Customer::PARAMS_TO_PERMIT)
  end

  def get_return_to_for_job(job)
    return_to = nil
    fail_message = nil
    if params[:redirect_to_edit_payment_id].present?
      return_to = edit_customer_payment_path(job.customer, job.payments.where(id: params[:redirect_to_edit_payment_id].to_i).first)
    end
    if return_to.nil?
      if params[:commit]['customer']
        return_to = job.customer
        fail_message = "job not assigned to customer"
      elsif params[:commit]['week']
        return_to = job.week
        fail_message = "job not assigned to week"
      elsif params[:commit]['list']
        return_to = job.work_list
        fail_message = "job not assigned to work list"
      end
    end
    if return_to.nil?
      return_to = job.work_list || job.week || job.customer
    else
      fail_message = nil
    end
    [return_to, fail_message]
  end

  def handle_week_starting
    if params[:week_starting].present? && (@job.date.nil? || params[:job].blank? || params[:job][:date].blank?)
      start_date = Date.parse(params[:week_starting])
      @job.week = current_business.weeks.where(start_date: start_date).first
      if @job.week.nil?
        @job.week = current_business.weeks.create(start_date: start_date)
        @job.week.create_work_lists(current_business)
      end
    end
  end

  def handle_payment_updating
    return if @job.linked?

    if params[:job][:paid_at].present?
      payment_attrs = {
        date: params[:job][:paid_at],
        method: params[:job][:payment_method],
        amount: params[:job][:payment_amount],
        credit: params[:credit_to_customer]
      }
      if @job.payment.present?
        @job.payment.update_attributes(payment_attrs)
      else
        @job.customer.payments.create(payment_attrs.merge(job_id: @job.id))
      end
    elsif @job.payment.present?
      @job.payment.destroy
    end
  end
end
