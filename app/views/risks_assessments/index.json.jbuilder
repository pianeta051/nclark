json.array!(@risks_assessments) do |risks_assessment|
  json.extract! risks_assessment, :yes, :too_high, :ground_not_safe, :no_clear_access_to_window, :work_from_inside, :footed_ladder, :folded_ladder, :read_and_wash, :secured_ladder, :harness, :bar, :other, :why, :operative, :date, :customer_id
  json.url risks_assessment_url(risks_assessment, format: :json)
end