$(function() {
  $('table .bookmarkable-bookmark').on('click', function() {
    var self = $(this);
    var tr = self.parents('tr');
    var data = {
      bookmarkable_id: self.data('bookmarkable-id'),
      bookmarkable_type: self.data('bookmarkable-type')
    };

    if (tr.hasClass('bookmarked')) {
      $.ajax({
        url: '/bookmarks',
        dataType: 'json',
        type: 'delete',
        data: data
      }).done(function(data) {
        if (data.success) {
          tr.removeClass('bookmarked');
        }
      });
    } else {
      $.ajax({
        url: '/bookmarks',
        dataType: 'json',
        method: 'post',
        data: data
      }).done(function(data) {
        if (data.success) {
          tr.addClass('bookmarked');
        }
      });
    }
  });
});
