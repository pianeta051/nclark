$(function() {
  var customerDataDiv = $('#customer-data');
  if (!customerDataDiv[0]) {
    return;
  }

  var customerDetails = customerDataDiv.data('customer').parent_customer_details;
  customerDataDiv.remove();

  var customerDetailsByCategory = [];
  var extraCustomerDetails = {category: null, customerDetails: []};

  _.each(customerDetails, function(customerDetail) {
    var index, category;
    if (customerDetail.business_default_customer_detail) {
      index = customerDetail.business_default_customer_detail.customer_detail_category.order;
      category = customerDetail.business_default_customer_detail.customer_detail_category;
    } else {
      extraCustomerDetails.customerDetails.push(customerDetail);
      return;
    }

    if (!customerDetailsByCategory[index]) {
      customerDetailsByCategory[index] = {category: category, customerDetails: []};
    }
    if (!customerDetailsByCategory[index].customerDetails[customerDetail.group_index]) {
      customerDetailsByCategory[index].customerDetails[customerDetail.group_index] = [];
    }
    customerDetailsByCategory[index].customerDetails[customerDetail.group_index][customerDetail.business_default_customer_detail.order] = customerDetail;
  });

  var inputCounter = 0;

  var getCustomerDetailHtml = function(customerDetail, clearValues, parentInputId, fieldOptions) {
    var options = {
      id: 'input_' + inputCounter,
      'data-default-customer-detail-id': customerDetail.business_default_customer_detail.id,
      'data-customer-detail-name': customerDetail.name
    };

    if (!clearValues) {
      options['data-customer-detail-id'] = customerDetail.id;
    }
    if (parentInputId >= 0) {
      options['data-parent-input-id'] = '' + parentInputId;
    }

    var content = '';
    content += $.tag('label', customerDetail.name, {for: 'input_' + inputCounter});
    if (customerDetail.business_default_customer_detail.large_field) {
      content += $.tag('textarea', (clearValues ? '' : customerDetail.value), options);
    } else {
      options.type = 'text';
      if (!clearValues) {
        options.value = customerDetail.value;
      }
      content += $.tag('input', '', options);
    }
    ++inputCounter;
    return content;
  };

  var getCustomerDetailGroupHtml = function(object, groupIndex, clearValues) {
    var options = {
      class: 'customer-detail-category',
      'data-category-id': object.category.id,
      'data-group-index': $('.customer-detail-category[data-category-id="' + object.category.id + '"]').length
    };

    if (groupIndex > 0) {
      var allFieldsNew = true;

      _.each(object.customerDetails[groupIndex], function(customerDetail) {
        if (customerDetail.id) {
          allFieldsNew = false;
        }
      });

      if (allFieldsNew) {
        return '';
      }
    }

    return $.tag('div', '', options, function() {
      var content = '';
      _.each(object.customerDetails[groupIndex], function(customerDetail) {
        content += $.tag('div', '', {class: 'customer-detail-field'}, function() {
          var parentInputId = inputCounter;
          var content = getCustomerDetailHtml(customerDetail, clearValues, -1);
          if (!clearValues) {
            var childCounter = 1;
            _.each(customerDetail.children, function(child) {
              child.business_default_customer_detail = customerDetail.business_default_customer_detail;
              child.name = customerDetail.name + ' ' + (++childCounter);
              content += getCustomerDetailHtml(child, false, parentInputId);
            });
          }

          if (!customerDetail.business_default_customer_detail.singular) {
            content += '<br/>';
            content += $.tag('a', 'Add extra ' + customerDetail.name, {
              class: 'add-extra-detail',
              href: '#',
              'data-input-id': parentInputId
            });
          }

          return content;
        });
      });
      return content + $.tag('hr');
    });
  };

  var getExtraCustomerDetailHtml = function(customerDetail) {
    customerDetail = customerDetail || {};

    var options = {
      class: 'customer-detail-field',
      'data-input-id': inputCounter
    };
    if (customerDetail.id) {
      options['data-customer-detail-id'] = customerDetail.id;
    }

    return $.tag('div', '', options, function() {
      var content = '';
      content += $.tag('label', 'Name', {for: 'input_name_' + inputCounter});
      content += $.tag('input', '', {
        type: 'text',
        id: 'input_name_' + inputCounter,
        class: 'extra-customer-detail',
        value: customerDetail.name || ''
      });

      content += $.tag('label', 'Value', {for: 'input_value_' + inputCounter});
      content += $.tag('textarea', customerDetail.value || '', {
        id: 'input_value_' + inputCounter,
        class: 'extra-customer-detail'
      });

      /*if (customerDetail.id) {
        content += $.tag('label', 'Remove customer detail', {for: 'input_destroy_' + inputCounter});
        content += $.tag('input', '', {
          id: 'input_destroy_' + inputCounter,
          type: 'checkbox',
          value: '1'
        });
      }*/

      ++inputCounter;
      return content + $.tag('hr');
    });
  };

  var form = $('form.customer-form');
  var customerDetailsContainer = form.find('#customer-details');
  customerDetailsContainer.append($.tag('div', '', {class: 'customer-details-columns row'}))
  var customerDetailsColumns = customerDetailsContainer.find('.customer-details-columns');
  customerDetailsColumns.append($.tag('div', '', {class: 'customer-details-column span6'}));
  var customerDetailsColumn = customerDetailsColumns.find('.customer-details-column');
  var categoriesPerColumn = Math.floor(customerDetailsByCategory.length / 2) + Math.floor(customerDetailsByCategory.length % 2);

  _.each(customerDetailsByCategory, function(object) {
    object.customerDetails = _.compact(object.customerDetails);
    customerDetailsColumn.append($.tag('h4', object.category.name));
    for (var i = 0; i < object.customerDetails.length; ++i) {
      object.customerDetails[i] = _.compact(object.customerDetails[i]);
      customerDetailsColumn.append(getCustomerDetailGroupHtml(object, i, false));
    }
    customerDetailsColumn.append($.tag('a', 'Add more ' + object.category.name, {
      class: 'add-extra-group',
      href: '#',
      'data-category-id': object.category.id
    }));
    customerDetailsColumn.append($.tag('hr'));

    if (--categoriesPerColumn === 0) {
      customerDetailsColumns.append($.tag('div', '', {class: 'customer-details-column second-column span6'}));
      customerDetailsColumn = customerDetailsColumns.find('.customer-details-column.second-column');
    }
  });


  customerDetailsContainer.append($.tag('div', '', {class: 'extra-customer-details row'}, function() {
    return $.tag('div', '', {class: 'span6'}, function() {
      var content = $.tag('h4', 'Extra details');
      _.each(extraCustomerDetails.customerDetails, function(customerDetail) {
        content += getExtraCustomerDetailHtml(customerDetail);
      });

      content += $.tag('a', 'Add extra detail', {
        class: 'add-extra-extra-detail',
        href: '#'
      });
      return content;
    });
  }));

  customerDetailsContainer.append($.tag('hr'));

  customerDetailsContainer.on('click', '.add-extra-detail', function(e) {
    e.preventDefault();

    inputId = $(this).data('input-id');
    var input = $('#input_' + inputId);
    var nodeName = ('' + input[0].nodeName).toLowerCase();
    var inputCount = $(this).parents('.customer-detail-field').find(nodeName).length;
    var breakTag = $(this).siblings('br').last();

    var labelName = input.data('customer-detail-name') + ' ' + (inputCount + 1);
    breakTag.before($.tag('label', labelName, {for: 'input_' + inputCounter}));

    var options = {
      id: 'input_' + inputCounter,
      'data-parent-input-id': inputId
    };
    if (nodeName === 'input') {
      options.type = 'text';
    }
    breakTag.before($.tag(nodeName, '', options));

    ++inputCounter;
    initWysihtml5();
  });

  customerDetailsContainer.on('click', '.add-extra-group', function(e) {
    e.preventDefault();

    var categoryId = parseInt($(this).data('category-id'));
    var object = _.find(customerDetailsByCategory, function(o) {
      return o.category.id === categoryId;
    });

    if (object) {
      $(this).before(getCustomerDetailGroupHtml(object, 0, true));
      initWysihtml5();
    }
  });

  customerDetailsContainer.on('click', '.add-extra-extra-detail', function(e) {
    e.preventDefault();
    $(this).before(getExtraCustomerDetailHtml(null));
    initWysihtml5();
  });

  form.on('submit', function(e) {
    var inputNamesAndValues = _.pairs({
      'customer_details_data[*][id]': 'customer-detail-id',
      'customer_details_data[*][parent_input_id]': 'parent-input-id',
      'customer_details_data[*][name]': 'customer-detail-name',
      'customer_details_data[*][value]': 'value',
      'customer_details_data[*][group_index]': 'group-index'
    });

    customerDetailsContainer.find('.customer-detail-category').each(function() {
      var categoryId = $(this).data('category-id');
      var groupIndex = $(this).data('group-index');
      $(this).find('.customer-detail-field').each(function() {
        $(this).find('input:not([name]), textarea:not([name])').each(function() {
          var thisInput = $(this);
          var inputId = thisInput.attr('id').replace('input_', '');
          _.each(inputNamesAndValues, function(keyValue) {
            var value;
            switch (keyValue[1]) {
            case 'customer-detail-id':
            case 'parent-id':
            case 'parent-input-id':
            case 'customer-detail-name': value = thisInput.data(keyValue[1]); break;
            case 'value': value = thisInput.val(); break;
            case 'group-index': value = '' + groupIndex; break;
            }

            if ((value || value === '0' || value === 0) && $.trim(value) != '') {
              var options = {
                name: keyValue[0].replace('*', '' + inputId),
                value: value,
                type: 'hidden'
              };
              form.append($.tag('input', '', options));
            }
          });
        });
      });
    });

    customerDetailsContainer.find('.extra-customer-details .customer-detail-field').each(function() {
      var inputId = $(this).data('input-id');
      var customerDetailId = $(this).data('customer-detail-id');
      var name = $(this).find('input[id="input_name_' + inputId + '"]').val();
      var value = $(this).find('textarea[id="input_value_' + inputId + '"]').val();
      //var destroy = $(this).find('input:checkbox').is(':checked');
      if (customerDetailId || (name && $.trim(name) != '') || (value && $.trim(value) != '')) {
        var nameTemplate = 'customer_details_data[' + inputId + '][*]';
        if (customerDetailId) {
          form.append($.tag('input', '', {name: nameTemplate.replace('*', 'id'), value: customerDetailId, type: 'hidden'}));
        }
        form.append($.tag('input', '', {name: nameTemplate.replace('*', 'name'), value: name, type: 'hidden'}));
        form.append($.tag('input', '', {name: nameTemplate.replace('*', 'value'), value: value, type: 'hidden'}));
        form.append($.tag('input', '', {name: nameTemplate.replace('*', 'extra_detail'), value: 'true', type: 'hidden'}));
        /*if (destroy) {
          form.append($.tag('input', '', {name: nameTemplate.replace('*', '_destroy'), value: '1', type: 'hidden'}));
        }*/
      }
    });

    return true;
  });

  if (initWysihtml5)
    initWysihtml5();
});
