$(function() {

  var checkSearchStatus = function() {
    $.ajax({
      url: '/customer_search_queries/get_search_status',
      dataType: 'JSON'
    }).done(function(data) {
      if (data.status == 'complete') {
        $('.customer-search-pending').html('<a href="' + location.pathname + '" class="btn">View results</a>');
      } else {
        setTimeout(checkSearchStatus, 1000);
      }
    });
  };

  var checkExportStatus = function() {
    $.ajax({
      url: '/customer_search_queries/get_export_status',
      dataType: 'JSON'
    }).done(function(data) {
      if (data.status == 'complete') {
        $('.customer-search-export.pending').removeClass('pending')
                                            .html('<a href="' + data.url + '">Download XLSX file</a>');
      } else {
        setTimeout(checkExportStatus, 1000);
      }
    });
  };

  var organizeRows = function(index, rowsSelector, groupSelector) {
    var rows = $(rowsSelector);
    if (!rows[index]) {
      return;
    }

    var row = $(rows[index]);
    if (row.find(groupSelector).length < 3) {
        for (var i = index + 1; i < rows.length; ++i) {
        var otherRow = $(rows[i]);
        for (var j = 0; j < 3 && row.find(groupSelector).length < 3; ++j) {
          var group = otherRow.find(groupSelector)[0];
          if (!group) {
            break;
          }
          row.append($(group).detach());
        }
      }
      if (row.find(groupSelector).length == 0) {
        row.remove();
      }
    }

    organizeRows(index + 1, rowsSelector, groupSelector);
  };

  var customerSearchGroupIndex = 0;
  var customerSortGroupIndex = 0;

  var addSortGroup = function() {
    var html = $('.customer-search .customer-sort-group-template').html()
                                                                  .split(":x")
                                                                  .join('' + customerSortGroupIndex);
    var sortGroups = $('.customer-search .customer-sort-groups');
    if (sortGroups.find('.row').length == 0 || sortGroups.find('.row').last().find('.customer-sort-group').length >= 3) {
      sortGroups.append('<div class="row"></div>');
    }
    sortGroups.find('.row').last().append(html);
    ++customerSortGroupIndex;
  }

  $('.customer-search').on('click', 'a.start-new-customer-search, a.add-customer-search-group', function(e) {
    e.preventDefault();

    var html = $('.customer-search .customer-search-group-template').html()
                                                                    .split(":x")
                                                                    .join('' + customerSearchGroupIndex)
                                                                    .split(":y")
                                                                    .join('0');

    var searchGroups = $('.customer-search .customer-search-groups');
    if (searchGroups.find('.row').length == 0 || searchGroups.find('.row').last().find('.customer-search-group').length >= 3) {
      searchGroups.append('<div class="row"></div>');
    }

    searchGroups.find('.row').last().append(html);

    $(this).hide();
    if ($(this).hasClass('start-new-customer-search')) {
      $('.customer-search .show-with-customer-search').show();
      $('.customer-search .customer-search-groups').show();
      $('.customer-search .customer-sort-groups').show();
      addSortGroup();
    } else {
      $(this).siblings('.and-text').show();
    }

    ++customerSearchGroupIndex;
  });
  $('.customer-search').on('click', 'a.add-customer-search-term', function(e) {
    e.preventDefault();

    var groupIndex = $(this).parents('.customer-search-group').data('customer-search-group-index');
    var termIndex = 0;
    $(this).parent().siblings('.customer-search-term').each(function() {
      var indexData = parseInt($(this).data('customer-search-term-index'));
      if (indexData > termIndex) {
        termIndex = indexData;
      }
    });
    ++termIndex; // highest index + 1
    var html = '<div class="or-text" data-search-term-index="' + termIndex + '">OR</div>'
    html += $('.customer-search .customer-search-group-template .customer-search-term').clone()
                                                                                       .wrap('<div/>')
                                                                                       .parent()
                                                                                       .html()
                                                                                       .split(":x")
                                                                                       .join('' + groupIndex)
                                                                                       .split(":y")
                                                                                       .join('' + termIndex);
    $(this).parent().before(html);
  });
  $('.customer-search').on('click', 'a.add-customer-sort-group', function(e) {
    e.preventDefault();
    addSortGroup();
    $(this).hide();
    $(this).siblings('.then-text').show();
  });
  $('.customer-search').on('click', '.customer-search-groups .customer-search-group .customer-search-term a.close', function(e) {
    e.preventDefault();

    var customerSearchGroup = $(this).parents('.customer-search-group');
    if (customerSearchGroup.find('.customer-search-term').length == 1) {
      var customerSearchGroupParent = customerSearchGroup.parent();
      customerSearchGroup.remove();
      organizeRows(
        $('.customer-search .customer-search-groups .row').index(customerSearchGroupParent),
        '.customer-search .customer-search-groups .row',
        '.customer-search-group'
      );

      var groupCount = $('.customer-search .customer-search-groups .customer-search-group').length;
      if (groupCount == 0) {
        customerSearchGroupIndex = 0;
        $('.customer-search a.start-new-customer-search').show();
        $('.customer-search .show-with-customer-search').hide();
        $('.customer-search .customer-search-groups').hide();
        $('.customer-search .customer-sort-groups').hide();
        $('.customer-search .customer-search-groups').html('');
        $('.customer-search .customer-sort-groups').html('');
      } else {
        $('.customer-search .customer-search-groups .customer-search-group .and-text').show();
        var lastGroup = $('.customer-search .customer-search-groups .customer-search-group').last();
        lastGroup.find('.and-text').hide();
        lastGroup.find('.add-customer-search-group').show();
      }
    } else {
      var termIndex = $(this).parent().data('customer-search-term-index');
      $(this).parent().siblings('.or-text[data-search-term-index="' + termIndex + '"]').remove();
      $(this).parent().remove();
      var firstDiv = customerSearchGroup.find('.well').children().first();
      if (firstDiv.hasClass('or-text')) {
        firstDiv.remove();
      }
    }
  });
  $('.customer-search').on('click', '.customer-sort-groups .customer-sort-group a.close', function(e) {
    e.preventDefault();

    if ($('.customer-search .customer-sort-groups .customer-sort-group').length == 1) {
      return;
    }

    var customerSortGroup = $(this).parents('.customer-sort-group').first();
    var customerSortGroupParent = customerSortGroup.parent();
    customerSortGroup.remove();
    organizeRows(
      $('.customer-search .customer-sort-groups .row').index(customerSortGroupParent),
      '.customer-search .customer-sort-groups .row',
      '.customer-sort-group'
    );
    $('.customer-search .customer-sort-groups .customer-sort-group .then-text').show();
    var lastGroup = $('.customer-search .customer-sort-groups .customer-sort-group').last();
    lastGroup.find('.then-text').hide();
    lastGroup.find('.add-customer-sort-group').show();
  });

  $('.customer-search').on('change', 'select.customer-search-field-name', function() {
    if ($(this).val().toLowerCase() == 'area') {
      $(this).siblings('input.customer-search-term-text-input').hide();
      $(this).siblings('select.customer-search-term-area-input').show();
    } else {
      $(this).siblings('select.customer-search-term-area-input').hide();
      $(this).siblings('input.customer-search-term-text-input').show();
    }
  });

  $(document).on('click', '.customer-search .customer-search-submit', function() {
    var data = {
      groups: {
        search: {},
        sort: {},
        extras: {}
      }
    };
    $('.customer-search .customer-search-groups .customer-search-group').each(function() {
      var groupIndex = $(this).data('customer-search-group-index');
      var groupData = {};
      $(this).find('.customer-search-term').each(function() {
        var termIndex = $(this).data('customer-search-term-index');
        var field = $(this).find('[name="customer_search[' + groupIndex + '][' + termIndex + '][field]"]').val();
        var term = $(this).find('[name="customer_search[' + groupIndex + '][' + termIndex + '][term]"]:visible').val();
        if ((field || '').trim() && (term || '').trim()) {
          groupData[termIndex] = {
            field: field,
            term: term
          };
        }
      });
      data.groups.search[groupIndex] = groupData;
    });

    var sortGroups = {};
    $('.customer-search .customer-sort-groups .customer-sort-group').each(function() {
      var groupIndex = $(this).data('customer-sort-group-index');
      var field = $(this).find('[name="customer_sort_by[' + groupIndex + '][field]"]').val();
      var order = $(this).find('[name="customer_sort_by[' + groupIndex + '][order]"]').val();
      var excludeNumbering = $(this).find('[name="customer_sort_by[' + groupIndex + '][exclude_numbering]"]').is(':checked');
      if ((field || '').trim() && (order || '').trim()) {
        sortGroups[groupIndex] = {
          field: field,
          order: order,
          exclude_numbering: excludeNumbering
        };
      }
    });
    data.groups.sort = sortGroups;

    data.groups.extras.bookmarked = $('.customer-search [name="customer_search_bookmarked"]').val();
    data.groups.extras.assigned = $('.customer-search [name="customer_search_assigned"]').val();
    data.groups.extras.active = $('.customer-search [name="customer_search_active"]').val();
    data.groups.extras.owing = $('.customer-search [name="customer_search_owing"]').val();

    $.ajax({
      url: '/customer_search_queries/search',
      method: 'POST',
      dataType: 'JSON',
      data: data
    }).done(function(data) {
      if (data.success) {
        var div = $('.customer-search');
        div.slideUp(function() {
          $(this).find('.customer-search-groups').html('');
          $(this).find('.customer-sort-groups').html('');
          $('.customer-search-pending').slideDown();
          $('.customer-search-export').remove();
          setTimeout(checkSearchStatus, 1000);
        });
        customerSearchGroupIndex = 0;
      } else if (data.redirect) {
        location.href = data.redirect;
      }
    });
  });

  if ($('.customer-search-pending').is(':visible')) {
    checkSearchStatus();
  } else if ($('.customer-search-export').hasClass('pending')) {
    checkExportStatus();
  }

  var searchToggleClasses = ['both', 'applied', 'unapplied'];
  $('.customer-search-toggle[data-toggle="tooltip"]').each(function() {
    var self = $(this);
    var field = $('input[name="' + self.data('field') + '"]');
    var val = field.val();
    var index = 0;
    if (val == 'applied') {
      index = 1;
    } else if (val == 'unapplied') {
      index = 2;
    }
    self.addClass(searchToggleClasses[index]);
    self.attr('title', self.data('tooltip-' + index));
    field.val(self.data('value-' + index));
  }).on('click', function(e) {
    e.preventDefault();
    var self = $(this);
    self.tooltip('destroy');
    var field = $('input[name="' + self.data('field') + '"]');
    var index = searchToggleClasses.indexOf(field.val());
    self.removeClass(searchToggleClasses[index]);
    if (++index > 2) {
      index = 0;
    }
    self.addClass(searchToggleClasses[index]);
    self.attr('title', self.data('tooltip-' + index));
    field.val(self.data('value-' + index));
    self.tooltip('show');
  }).tooltip();

  $(document).on('click', 'tr td .remove-customer-from-search-results', function(e) {
    e.preventDefault();
    var self = $(this);
    $.ajax({
      url: '/customer_search_queries/remove_customer_from_search_results',
      method: 'DELETE',
      dataType: 'JSON',
      data: {
        customer_id: self.data('customer-id')
      }
    }).done(function(data) {
      if (data.success) {
        self.parents('tr').remove();
      }
    });
  });

});
