$(function() {
  $('.jobs-table tr').each(function() {
    var self = $(this);
    var table = self.parents('table.jobs-table');
    var jobId = self.data('job-id');
    var isWeeksShowTable = table.hasClass('weeks-show-table');

    self.find('input, select')
        .on('change', function() {
      var authenticityToken = $('meta[name="csrf-token"]').attr('content');
      var input = $(this);
      var ajaxLoader = input.siblings('.ajax-loader');
      var jobData = {};
      var inputName = input.attr('name');
      jobData[inputName] = input.val();

      var data = {
        _method: 'patch',
        authenticity_token: authenticityToken,
        job: jobData
      };

      if (isWeeksShowTable) {
        data['week_id'] = table.data('week-id');
      }

      input.hide();
      ajaxLoader.show();
      $.ajax({
        url: '/jobs/' + jobId,
        type: 'POST',
        dataType: 'JSON',
        data: data
      }).done(function(data) {
        ajaxLoader.hide();
        input.show();
        if (!data.success && data.job && data.job[inputName]) {
          input.val(data.job[inputName]);
        }
        if (data.success && isWeeksShowTable) {
          var hasClickableRowClassAddtion = self.hasClass('clickable-row') ? ' clickable-row' : '';
          self.attr('class', (data.current_week_job_html_classes || '') + hasClickableRowClassAddtion);
          var workListGrid = $('.work-list-grid');
          if (data.work_list_without_jobs_id) {
            workListGrid.find('td[data-work-list-id="' + data.work_list_without_jobs_id + '"]')
                        .removeClass('work-list-with-jobs');
          }
          if (data.work_list_with_jobs_id) {
            workListGrid.find('td[data-work-list-id="' + data.work_list_with_jobs_id + '"]')
                        .addClass('work-list-with-jobs');
          }
        }
      });
    });
  });
});
