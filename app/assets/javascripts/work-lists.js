$(function() {
  $('.view-work-list-container a.view-work-list').on('click', function() {
    var authenticityToken = $('meta[name="csrf-token"]').attr('content');
    var weekId = $(this).data('week-id');
    var day = $(this).siblings('select[name="day"]').val();
    var workListCategoryId = $(this).siblings('select[name="work_list_category_id"]').val();
    $.ajax({
      url: '/work_lists/view',
      data: {
        week_id: weekId,
        day: day,
        work_list_category_id: workListCategoryId,
        authenticity_token: authenticityToken
      }
    });
  });

  /*$('table.work-list-table').each(function() {
    var thisTable = $(this);*/

    /*thisTable.find('.job-notes form')
             .on('submit', function() {
      $(this).find('input:submit')
             .hide();
      $(this).find('.ajax-loader')
             .show();
    });*/

    //thisTable.find('.job-notes a.edit-notes')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-table .job-notes a.edit-notes', function(e) {
      e.preventDefault();
      $(this).siblings('.job-notes .job-notes-form.modal')
             .modal('show');
    });

    //thisTable.find('.view-notes').popover({html: true, trigger: 'hover'}).on('click', function() { return false; });
  //});

  //$('table.work-list-table, table.finance-unpaid-jobs').each(function() {
  //  var thisTable = $(this);

    //thisTable.find('.job-paid a.mark-as-paid')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-table .job-paid a.mark-as-paid, table.finance-unpaid-jobs .job-paid a.mark-as-paid', function(e) {
      e.preventDefault();
      $(this).siblings('.job-paid .job-paid-form.modal')
             .modal('show');
    });
  //})

  //$('table.work-list-table, table.weeks-show-table').each(function() {
  //  var thisTable = $(this);

    //thisTable.find('a.edit-job-price')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-table a.edit-job-price, table.weeks-show-table a.edit-job-price', function(e) {
      e.preventDefault();
      $(this).parents('.td-content')
             .hide()
             .siblings('.edit-job-price-field')
             .show();
    });

    //thisTable.find('.edit-job-price-field button')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-table .edit-job-price-field button, table.weeks-show-table .edit-job-price-field button', function(e) {
      e.preventDefault();
      var thisButton = $(this);

      var inputField = thisButton.siblings('input');
      var jobId = thisButton.data('job-id');
      var priceExcludingVat = null;
      var priceIncludingVat = null;
      if (inputField.hasClass('price-excluding-vat')) {
        priceExcludingVat = inputField.val();
      } else {
        priceIncludingVat = inputField.val();
      }
      var t1 = thisButton.parents('table').data('t1');
      var t2 = thisButton.parents('table').data('t2');
      var authenticityToken = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: '/jobs/' + jobId + '/update_price',
        dataType: 'json',
        method: 'put',
        data: {
          price_excluding_vat: priceExcludingVat,
          price_including_vat: priceIncludingVat,
          t1: t1,
          t2: t2,
          authenticity_token: authenticityToken
        }
      }).done(function(data) {
        var tdContent = thisButton.parents('.edit-job-price-field')
                                  .siblings('.td-content');
        var tr = tdContent.parents('tr');
        if (data.success && data.success != 'false') {
          tr.find('.price-excluding-vat')
            .html(data.price_excluding_vat);
          tr.find('input.price-excluding-vat')
            .val(data.price_excluding_vat);
          tr.find('.price-vat')
            .html(data.price_vat);
          tr.find('.price-including-vat')
            .html(data.price_including_vat);
          tr.find('input.price-including-vat')
            .val(data.price_including_vat);
          tr.find('.customer-outstanding-money')
            .html(data.customer_outstanding_money);
        } else {
          inputField.val('');
        }
        thisButton.parents('.edit-job-price-field')
                  .hide();
        tdContent.show();
      });
    });


    //thisTable.find('a.edit-job-invoice-number')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-table a.edit-job-invoice-number, table.weeks-show-table a.edit-job-invoice-number', function(e) {
      e.preventDefault();
      $(this).parents('.td-content')
             .hide()
             .siblings('.edit-job-invoice-number-field')
             .show();
    });

    //thisTable.find('.edit-job-invoice-number-field button')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-table .edit-job-invoice-number-field button, table.weeks-show-table .edit-job-invoice-number-field button', function(e) {
      e.preventDefault();
      var thisButton = $(this);

      var inputField = thisButton.siblings('input');
      var jobId = thisButton.data('job-id');
      var invoiceNumber = inputField.val();
      var t1 = thisButton.parents('table').data('t1');
      var t2 = thisButton.parents('table').data('t2');
      var authenticityToken = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: '/jobs/' + jobId + '/update_invoice_number',
        dataType: 'json',
        method: 'put',
        data: {
          invoice_number: invoiceNumber,
          t1: t1,
          t2: t2,
          authenticity_token: authenticityToken
        }
      }).done(function(data) {
        var tdContent = thisButton.parents('.edit-job-invoice-number-field')
                                  .siblings('.td-content');
        var tr = tdContent.parents('tr');
        if (data.success && data.success != 'false') {
          tr.find('.invoice-number')
            .html(data.invoice_number);
        } else {
          inputField.val('');
        }
        thisButton.parents('.edit-job-invoice-number-field')
                  .hide();
        tdContent.show();
      });
    });

  //});



  //$('table.work-list-grid').each(function() {
  //  var thisTable = $(this);

    //thisTable.find('a.edit-work-list-tag')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-grid a.edit-work-list-tag', function(e) {
      e.preventDefault();
      $(this).parents('.td-content')
             .hide()
             .siblings('.edit-work-list-tag-field')
             .show();
    });

    //thisTable.find('.edit-work-list-tag-field button')
    //         .on('click', function(e) {
    $(document).on('click', 'table.work-list-grid .edit-work-list-tag-field button', function(e) {
      e.preventDefault();
      var thisButton = $(this);

      var inputField = thisButton.siblings('input');
      var workListId = thisButton.data('work-list-id');
      var tag = inputField.val();
      var authenticityToken = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: '/work_lists/' + workListId + '/update_tag',
        dataType: 'json',
        method: 'put',
        data: {
          tag: tag,
          authenticity_token: authenticityToken
        }
      }).done(function(data) {
        var tdContent = thisButton.parents('.edit-work-list-tag-field')
                                  .siblings('.td-content');
        if (data.success && data.success != 'false') {
          tdContent.find('.full-name a')
                   .html(data.full_name);
        } else {
          inputField.val('');
        }
        thisButton.parents('.edit-work-list-tag-field')
                  .hide();
        tdContent.show();
      });
    });

  //});


  $("#move-selected-jobs-to-work-list").each(function() {
    var container = $(this);
    var workListTable = $('table.work-list-table');

    container.find('a')
             .on('click', function(e) {
      e.preventDefault();

      var data = {
        authenticity_token: $('meta[name="csrf-token"]').attr('content'),
        date: container.children('input[name="date"]').val(),
        work_list_category_id: container.children('select[name="work_list_category_id"]').val(),
        job_ids: $.map(workListTable.find('input[type="checkbox"]:checked'), function(checkbox) {
          return $(checkbox).data('job-id');
        })
      };

      if ($(this).hasClass('job-cancel-button')) {
        data.date = null;
        data.day = -1;
        data.work_list_category_id = null;
      }

      $.ajax({
        url: '/work_lists/' + workListTable.data('work-list-id') + '/move_jobs',
        dataType: 'json',
        method: 'put',
        data: data
      }).done(function(data) {
        if (data.job_ids_to_remove) {
          $.each(data.job_ids_to_remove, function(i, jobId) {
            workListTable.find('tr#job-' + jobId)
                         .remove();
            workListTable.find('tr#job-' + jobId + '-notes')
                         .remove();
          });
        }
      });
    });
  });

});
