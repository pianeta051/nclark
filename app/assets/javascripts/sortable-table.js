//http://www.foliotek.com/devblog/make-table-rows-sortable-using-jquery-ui-sortable/

$(function() {
  var fixHelper = function(e, ui) {
    ui.children().each(function() {
      $(this).width($(this).width());
    });
    return ui;
  };

  var orderingSuccessCallback = function() {};

  (function() {
    var table = $("table.ui-sortable");
    var tbody = table.find('tbody');

    if (table.hasClass('work-list-table')) {
      $('#work-list-ordering-enabler').on('click', function() {
        $(this).hide();
        $('#work-list-ordering-buttons').show();
        tbody.find('.work-list-job-notes').hide();
        tbody.find('a').hide();
        tbody.sortable({
          helper: fixHelper,
          items: 'tr:not(.ui-state-disabled)'
        });
        return false;
      });

      orderingSuccessCallback = function() {
        var enablerButton = $('#work-list-ordering-enabler').show().find('a');
        $('#work-list-ordering-buttons').hide();
        var previousText = enablerButton.text();
        enablerButton.text('Saved!');
        setTimeout(function() {
          enablerButton.text(previousText);
        }, 2000);
        tbody.sortable("destroy");
        tbody.find('tr.work-list-job-notes').each(function(i, el) {
          var tr = $(el);
          tr.insertAfter(tbody.find('tr.work-list-job[data-job-id="' + tr.data('job-id') + '"]'));
        });
        tbody.find('.work-list-job-notes').show();
        tbody.find('a').show();
      };
    } else {
      table.find('tbody').sortable({
        helper: fixHelper
      });
    }
  })();

  $('a.save-ordering').on('click', function(e) {
    e.preventDefault();

    var self = $(this);
    var table = $(self.data('table-selector'));

    var data = {};
    var count = 0;
    table.find("tbody tr:not(.ui-state-disabled)").each(function() {
      if ($(this).parent().parent()[0] == table[0]) {
        var id = $(this).data('record-id');
        data[id] = count++;
      }
    });
    
    $.ajax({
      url: self.attr('href'),
      dataType: 'json',
      type: 'PUT',
      data: {
        ordering: data,
        work_list_order: ('' + self.data('work-list-order')) === 'true' ? 'true' : 'false'
      }
    }).done(function(data) {
      if (data.success) {
        var previousText = self.text();
        self.text('Saved!');
        setTimeout(function() {
          self.text(previousText);
        }, 2000);
        orderingSuccessCallback();
      }
    });
    return false;
  });
});