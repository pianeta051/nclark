var initWysihtml5;
$(function() {
  var idIsSameAsName = function(el) {
    if (!el.attr('id') || !el.attr('name')) {
      return false;
    }
    return el.attr('id') == el.attr('name').replace('[', '_').replace(']', '');
  };

  var textareaCounter = 0;
  var editors = [];
  initWysihtml5 = function() {
    $('textarea').each(function() {
      if ($(this).data('wysihtml5-editor')) {
        return;
      }

      var thisTextarea = $(this);
      if (!thisTextarea.attr('id') || idIsSameAsName(thisTextarea)) {
        textareaCounter++;
        thisTextarea.attr('id', 'textarea' + textareaCounter);
      }
      thisTextarea.before('<div id="wysihtml5-toolbar-' + thisTextarea.attr('id') + '" style="display:none;">' + $('#wysihtml5-toolbar-template').html() + '</div>');
      var editor = new wysihtml5.Editor(thisTextarea.attr('id'), {
        name: 'wysihtml5-' + thisTextarea.attr('id'),
        toolbar: "wysihtml5-toolbar-" + thisTextarea.attr('id'),
        parserRules: wysihtml5ParserRules
      });
      editor.on('blur', function() {
        thisTextarea.change();
      });
      editors.push(editor);
      $(this).data('wysihtml5-editor', editor);
      $(editor.composer.iframe).wysihtml5_size_matters();
    });
  };

  initWysihtml5();
});
