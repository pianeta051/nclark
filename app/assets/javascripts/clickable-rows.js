$(function() {
  //$('tr.clickable-row').each(function() {
  //  var self = $(this);
  //  self.find('td.clickable')
  //      .on('mousedown', function(e) {
  $(document).on('mousedown', 'tr.clickable-row td.clickable', function(e) {
    if (e.which != 1) { // if not left mouse button
      return;
    }

    $(this).addClass('clickable-row-mousedown-on')
           .data('mousedownx', e.pageX)
           .data('mousedowny', e.pageY);
  });
  //});

  $(document).on('mouseup', function(e) {
    var td = $(e.target);
    if (td.hasClass('clickable-row-mousedown-on')) {
      var x = td.data('mousedownx');
      var y = td.data('mousedowny');

      if (Math.abs(e.pageX - x) < 10 && Math.abs(e.pageY - y) < 10) {
        var tr = td.parent();
        var url = '/' + tr.data('resource') + '/' + tr.data('id');
        if (tr.data('extra-params')) {
          url += '?' + tr.data('extra-params');
        }
        window.location = url;
      }
    }
    $('tr.clickable-row td.clickable.clickable-row-mousedown-on').removeClass('clickable-row-mousedown-on');
  });

  $('td.checkbox-toggle input[type="checkbox"]').on('click', function(e) {
    e.stopPropagation();
  });

  $('td.checkbox-toggle').on('click', function() {
    $(this).children('input[type="checkbox"]')
           .each(function() {
      $(this).prop('checked', !$(this).is(':checked'));
    });
  });
});
