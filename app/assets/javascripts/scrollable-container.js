$(function() {

  $('.scrollable-container').each(function() {
    var self = $(this);
    var maxHeight = self.data('max-height');
    if (maxHeight) {
      maxHeight = parseInt(maxHeight);
    } else {
      maxHeight = 300;
    }
    if (self.height() > maxHeight) {
      self.css('max-height', maxHeight + 'px')
          .css('overflow-y', 'scroll');
    }
  });

});