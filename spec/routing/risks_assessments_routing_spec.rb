require "spec_helper"

describe RisksAssessmentsController do
  describe "routing" do

    it "routes to #index" do
      get("/risks_assessments").should route_to("risks_assessments#index")
    end

    it "routes to #new" do
      get("/risks_assessments/new").should route_to("risks_assessments#new")
    end

    it "routes to #show" do
      get("/risks_assessments/1").should route_to("risks_assessments#show", :id => "1")
    end

    it "routes to #edit" do
      get("/risks_assessments/1/edit").should route_to("risks_assessments#edit", :id => "1")
    end

    it "routes to #create" do
      post("/risks_assessments").should route_to("risks_assessments#create")
    end

    it "routes to #update" do
      put("/risks_assessments/1").should route_to("risks_assessments#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/risks_assessments/1").should route_to("risks_assessments#destroy", :id => "1")
    end

  end
end
