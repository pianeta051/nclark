# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :expense do
    business_id 1
    date "2015-07-15"
    cents 1
    vat_cents 1
    description "MyText"
  end
end
