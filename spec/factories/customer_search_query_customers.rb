# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :customer_search_query_customer do
    customer_search_query_id 1
    customer_id 1
  end
end
