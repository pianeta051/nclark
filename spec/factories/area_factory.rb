# == Schema Information
#
# Table name: areas
#
#  id          :integer          not null, primary key
#  business_id :integer          not null
#  name        :string(40)       not null
#  created_at  :datetime
#  updated_at  :datetime
#  color       :string(6)
#

FactoryGirl.define do
  factory :area do
    business
    name 'place'
  end
end
