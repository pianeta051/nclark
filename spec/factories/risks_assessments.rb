# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :risks_assessment do
    yes false
    too_high false
    ground_not_safe false
    no_clear_access_to_window false
    work_from_inside false
    footed_ladder false
    folded_ladder false
    read_and_wash false
    secured_ladder false
    harness false
    bar false
    other "MyString"
    why "MyString"
    operative "MyString"
    date "2020-02-20"
    customer nil
  end
end
