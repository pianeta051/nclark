# == Schema Information
#
# Table name: customers
#
#  id                      :integer          not null, primary key
#  business_id             :integer          not null
#  area_id                 :integer          not null
#  regularity              :integer
#  created_at              :datetime
#  updated_at              :datetime
#  start_date              :date
#  regular_price_cents     :integer          default(0)
#  regularity_type         :string(6)
#  add_vat                 :boolean          default(FALSE)
#  color                   :string(6)
#  non_regular             :boolean          default(TRUE)
#  customer_details_cache  :hstore
#  last_clean              :date
#  jobs_count              :integer          default(0), not null
#  area_color_cache        :string(6)        not null
#  area_name_cache         :string(255)
#  active                  :boolean          default(TRUE)
#  outstanding_money_cents :integer          default(0)
#  owner_id                :integer
#  day_span                :integer          default(1)
#

FactoryGirl.define do
  factory :customer do
    business
    title "Mr"
    forename "John"
    surname "Doe"
    address_line_1 "25 Something Lane"
    address_line_2 ""
    town "Place"
    postcode "AQ22 4RE"
    phone "01234567896"
    email "john.doe@example.com"
    area
    regularity 4
  end
end
