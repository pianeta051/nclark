# == Schema Information
#
# Table name: work_list_categories
#
#  id                :integer          not null, primary key
#  business_id       :integer          not null
#  name              :string(40)       not null
#  created_at        :datetime
#  updated_at        :datetime
#  tag               :string(255)
#  target_cents      :integer          default(0)
#  balance_cents     :integer          default(0)
#  balance_date_from :date
#  user_id           :integer
#

FactoryGirl.define do
  factory :work_list_category do
    business
    name "MyString"
  end
end
