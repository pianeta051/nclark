# == Schema Information
#
# Table name: jobs
#
#  id                    :integer          not null, primary key
#  business_id           :integer          not null
#  customer_id           :integer          not null
#  week_id               :integer          not null
#  cents                 :integer          not null
#  created_at            :datetime
#  updated_at            :datetime
#  work_list_id          :integer
#  day                   :integer          default(-1)
#  work_list_category_id :integer
#  vat_rate              :decimal(10, 2)   default(0.0)
#  completed_at          :date
#  paid_at               :date
#  payment_method        :string(15)
#  payment_amount_cents  :integer
#  invoice_number        :string(40)
#  start_time            :time
#  finish_time           :time
#  payment_charge_cents  :integer          default(0)
#  work_list_order       :integer
#  amount_paid_cents     :integer          default(0)
#  linked_job_id         :integer
#  child_jobs_count      :integer          default(0)
#  linked_job_day        :integer          default(1)
#  completed_cache       :boolean          default(FALSE)
#  payments_count        :integer
#  check_risk            :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :job do
    customer_id 1
    week_id 1
    cents 1
    date "2013-05-24"
    notes "MyText"
    completed false
    paid false
  end
end
