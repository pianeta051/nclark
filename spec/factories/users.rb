# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
  	business
    sequence(:name) { |n| "john.doe#{n}" }
    password "password"
    password_confirmation "password"
  end
end