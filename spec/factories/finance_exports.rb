# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :finance_export do
    user_id 1
    terms ""
    xlsx_file "MyString"
    export_finished_at "2015-12-31 12:18:34"
  end
end
