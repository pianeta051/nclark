# == Schema Information
#
# Table name: customer_details
#
#  id            :integer          not null, primary key
#  customer_id   :integer          not null
#  name          :string(250)
#  value         :text
#  created_at    :datetime
#  updated_at    :datetime
#  parent_id     :integer
#  trimmed_value :text
#  group_index   :integer          default(0)
#

FactoryGirl.define do
  factory :customer_detail do
    name "MyString"
    value "MyString"
  end
end
