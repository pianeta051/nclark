# == Schema Information
#
# Table name: work_list_notes
#
#  id           :integer          not null, primary key
#  work_list_id :string(255)      not null
#  user_id      :string(255)
#  content      :text
#  created_at   :datetime
#  updated_at   :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :work_list_note do
    work_list_id "MyString"
    user_id "MyString"
    content "MyText"
  end
end
