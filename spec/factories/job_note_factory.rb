# == Schema Information
#
# Table name: job_notes
#
#  id              :integer          not null, primary key
#  job_id          :integer          not null
#  user_id         :integer
#  content         :text
#  created_at      :datetime
#  updated_at      :datetime
#  user_name_cache :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job_note do
    job_id 1
    user_id 1
    content "MyString"
  end
end
