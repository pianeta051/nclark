# == Schema Information
#
# Table name: businesses
#
#  id                          :integer          not null, primary key
#  email                       :string(255)      default(""), not null
#  encrypted_password          :string(255)      default(""), not null
#  reset_password_token        :string(255)
#  reset_password_sent_at      :datetime
#  remember_created_at         :datetime
#  sign_in_count               :integer          default(0)
#  current_sign_in_at          :datetime
#  last_sign_in_at             :datetime
#  current_sign_in_ip          :string(255)
#  last_sign_in_ip             :string(255)
#  confirmation_token          :string(255)
#  confirmed_at                :datetime
#  confirmation_sent_at        :datetime
#  unconfirmed_email           :string(255)
#  created_at                  :datetime
#  updated_at                  :datetime
#  name                        :string(255)      not null
#  cycle_length                :integer          default(4)
#  default_vat_rate            :decimal(10, 2)   default(0.0)
#  customer_color_field_name   :string(100)      default("Colour")
#  work_list_payment_methods   :string(255)      default("cash,cheque,credit,debit,bank transfer")
#  credit_charge_cents         :integer          default(0)
#  debit_charge_cents          :integer          default(0)
#  credit_charge_is_percentage :boolean          default(FALSE)
#  debit_charge_is_percentage  :boolean          default(FALSE)
#  default_publication_time    :time
#

FactoryGirl.define do
  factory :business do
    sequence(:email) { |n| "john.doe#{n}@example.com" }
    password "password"
    password_confirmation "password"
    name "Test business"

    after :create do |user|
      user.confirm!
    end
  end
end
