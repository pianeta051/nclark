# == Schema Information
#
# Table name: customer_detail_categories
#
#  id          :integer          not null, primary key
#  business_id :integer          not null
#  name        :string(250)      not null
#  order       :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#

FactoryGirl.define do
  factory :customer_detail_category do
    name "MyString"
    order 1
  end
end
