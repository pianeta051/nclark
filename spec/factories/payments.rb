# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    customer_id 1
    paid_at "2015-07-29"
    payment_method "MyString"
    payment_amount_cents 1
    payment_charge_cents 1
  end
end
