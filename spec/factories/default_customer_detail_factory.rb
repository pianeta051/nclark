# == Schema Information
#
# Table name: default_customer_details
#
#  id                          :integer          not null, primary key
#  business_id                 :integer          not null
#  name                        :string(250)
#  large_field                 :boolean          default(FALSE)
#  visible_in_index            :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  singular                    :boolean          default(FALSE)
#  order                       :integer          not null
#  customer_detail_category_id :integer          not null
#  visible_in_work_list        :boolean          default(FALSE)
#  work_list_order             :integer          not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :default_customer_detail do
    business_id 1
    name "field"
    large_field false
    visible_in_index false
  end
end
