# == Schema Information
#
# Table name: risks_assessments
#
#  id                        :integer          not null, primary key
#  yes                       :boolean
#  no                        :boolean
#  too_high                  :boolean
#  ground_not_safe           :boolean
#  no_clear_access_to_window :boolean
#  work_from_inside          :boolean
#  footed_ladder             :boolean
#  folded_ladder             :boolean
#  work_positioning_system   :boolean
#  read_and_wash             :boolean
#  secured_ladder            :boolean
#  harness                   :boolean
#  bar                       :boolean
#  other                     :string(255)
#  why                       :text
#  operative                 :string(255)
#  date                      :date
#  user_id                   :integer
#  customer_id               :integer
#  created_at                :datetime
#  updated_at                :datetime
#  not_all                   :boolean
#  worklist_id               :integer
#

require 'spec_helper'

describe RisksAssessment do
  pending "add some examples to (or delete) #{__FILE__}"
end
