# == Schema Information
#
# Table name: default_customer_details
#
#  id                          :integer          not null, primary key
#  business_id                 :integer          not null
#  name                        :string(250)
#  large_field                 :boolean          default(FALSE)
#  visible_in_index            :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  singular                    :boolean          default(FALSE)
#  order                       :integer          not null
#  customer_detail_category_id :integer          not null
#  visible_in_work_list        :boolean          default(FALSE)
#  work_list_order             :integer          not null
#

require 'spec_helper'

describe DefaultCustomerDetail do
  pending "add some examples to (or delete) #{__FILE__}"
end
