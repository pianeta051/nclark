# == Schema Information
#
# Table name: work_lists
#
#  id                        :integer          not null, primary key
#  work_list_category_id     :integer          not null
#  week_id                   :integer          not null
#  day                       :integer          not null
#  created_at                :datetime
#  updated_at                :datetime
#  token_1                   :string(255)
#  token_2                   :string(255)
#  tag                       :string(255)
#  completed                 :boolean          default(FALSE)
#  target_cents              :integer          default(0)
#  date_cache                :date
#  job_counter_cache         :integer
#  completed_job_cents_cache :integer
#  target_delta_cents_cache  :integer          default(0)
#  visible                   :boolean          default(FALSE)
#  publication_date          :date
#  publication_time          :time
#

require 'spec_helper'

describe WorkList do
  pending "add some examples to (or delete) #{__FILE__}"
end
