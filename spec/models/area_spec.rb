# == Schema Information
#
# Table name: areas
#
#  id          :integer          not null, primary key
#  business_id :integer          not null
#  name        :string(40)       not null
#  created_at  :datetime
#  updated_at  :datetime
#  color       :string(6)
#

require 'spec_helper'

describe Area do
  let(:area) { FactoryGirl.build(:area) }

  describe "validations" do
    it "should have a valid factory" do
      area.should be_valid
    end

    describe "name" do
      it "should be required" do
        area.name = ''
        area.should_not be_valid
      end

      it "should be no longer than 40 characters" do
        area.name = 'a' * 41
        area.should_not be_valid
      end
    end
  end
end
