module ControllerMacros
  def login
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:business]
      @business = FactoryGirl.create(:business)
      sign_in @business

      @user = FactoryGirl.create(:user, business: @business)
      session[:user_id] = @user.id
    end
  end
end