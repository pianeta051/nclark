require 'spec_helper'

describe "risks_assessments/index" do
  before(:each) do
    assign(:risks_assessments, [
      stub_model(RisksAssessment,
        :yes => false,
        :too_high => false,
        :ground_not_safe => false,
        :no_clear_access_to_window => false,
        :work_from_inside => false,
        :footed_ladder => false,
        :folded_ladder => false,
        :read_and_wash => false,
        :secured_ladder => false,
        :harness => false,
        :bar => false,
        :other => "Other",
        :why => "Why",
        :operative => "Operative",
        :customer => nil
      ),
      stub_model(RisksAssessment,
        :yes => false,
        :too_high => false,
        :ground_not_safe => false,
        :no_clear_access_to_window => false,
        :work_from_inside => false,
        :footed_ladder => false,
        :folded_ladder => false,
        :read_and_wash => false,
        :secured_ladder => false,
        :harness => false,
        :bar => false,
        :other => "Other",
        :why => "Why",
        :operative => "Operative",
        :customer => nil
      )
    ])
  end

  it "renders a list of risks_assessments" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Other".to_s, :count => 2
    assert_select "tr>td", :text => "Why".to_s, :count => 2
    assert_select "tr>td", :text => "Operative".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
