require 'spec_helper'

describe "risks_assessments/new" do
  before(:each) do
    assign(:risks_assessment, stub_model(RisksAssessment,
      :yes => false,
      :too_high => false,
      :ground_not_safe => false,
      :no_clear_access_to_window => false,
      :work_from_inside => false,
      :footed_ladder => false,
      :folded_ladder => false,
      :read_and_wash => false,
      :secured_ladder => false,
      :harness => false,
      :bar => false,
      :other => "MyString",
      :why => "MyString",
      :operative => "MyString",
      :customer => nil
    ).as_new_record)
  end

  it "renders new risks_assessment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", risks_assessments_path, "post" do
      assert_select "input#risks_assessment_yes[name=?]", "risks_assessment[yes]"
      assert_select "input#risks_assessment_too_high[name=?]", "risks_assessment[too_high]"
      assert_select "input#risks_assessment_ground_not_safe[name=?]", "risks_assessment[ground_not_safe]"
      assert_select "input#risks_assessment_no_clear_access_to_window[name=?]", "risks_assessment[no_clear_access_to_window]"
      assert_select "input#risks_assessment_work_from_inside[name=?]", "risks_assessment[work_from_inside]"
      assert_select "input#risks_assessment_footed_ladder[name=?]", "risks_assessment[footed_ladder]"
      assert_select "input#risks_assessment_folded_ladder[name=?]", "risks_assessment[folded_ladder]"
      assert_select "input#risks_assessment_read_and_wash[name=?]", "risks_assessment[read_and_wash]"
      assert_select "input#risks_assessment_secured_ladder[name=?]", "risks_assessment[secured_ladder]"
      assert_select "input#risks_assessment_harness[name=?]", "risks_assessment[harness]"
      assert_select "input#risks_assessment_bar[name=?]", "risks_assessment[bar]"
      assert_select "input#risks_assessment_other[name=?]", "risks_assessment[other]"
      assert_select "input#risks_assessment_why[name=?]", "risks_assessment[why]"
      assert_select "input#risks_assessment_operative[name=?]", "risks_assessment[operative]"
      assert_select "input#risks_assessment_customer[name=?]", "risks_assessment[customer]"
    end
  end
end
