require 'spec_helper'

describe "risks_assessments/show" do
  before(:each) do
    @risks_assessment = assign(:risks_assessment, stub_model(RisksAssessment,
      :yes => false,
      :too_high => false,
      :ground_not_safe => false,
      :no_clear_access_to_window => false,
      :work_from_inside => false,
      :footed_ladder => false,
      :folded_ladder => false,
      :read_and_wash => false,
      :secured_ladder => false,
      :harness => false,
      :bar => false,
      :other => "Other",
      :why => "Why",
      :operative => "Operative",
      :customer => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/Other/)
    rendered.should match(/Why/)
    rendered.should match(/Operative/)
    rendered.should match(//)
  end
end
