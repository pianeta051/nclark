require 'spec_helper'

feature 'Business signs up' do
	scenario 'with valid email, name and password' do
		business = FactoryGirl.build(:business)
	    expect{sign_up_with business}.to change{Business.count}.by(1)

	    expect(page).to have_selector '.alert-notice', text: 'A message with a confirmation link has been sent to your email address. Please open the link to activate your account.'

	    open_email(business.email)
	    current_email.click_link 'Confirm my account'

	    expect(page).to have_css '.alert-notice', text: 'Your account was successfully confirmed. You are now signed in.'
  	end

  	scenario 'with blank email' do
  		business = FactoryGirl.build(:business)
  		business.email = ''
  		expect{sign_up_with business}.to_not change{Business.count}
  		expect(page).to have_text 'Email can\'t be blank'
  	end
#begin carlos
  	scenario 'with too long email' do
  		business = FactoryGirl.build(:business)
  		business.email = 'a' * 256
  		expect{sign_up_with business}.to_not change{Business.count}
  		expect(page).to have_text 'Email is invalid'
  	end
  	#  	 	
	def sign_up_with(business)
	    visit new_business_registration_path
	    fill_in 'Business name', with: business.name
	    fill_in 'Email', with: business.email
	    fill_in 'Password', with: business.password
	    fill_in 'Password confirmation', with: business.password
	    click_button 'Sign up'
  	end
end