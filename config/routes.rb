NclarkWindows::Application.routes.draw do



  devise_for :businesses, controllers: { registrations: 'businesses/registrations' }

  resources :businesses, only: [:update]
  get 'businesses', to: 'pages#home'

  resources :default_customer_details, except: [:index, :show] do
    collection do
      put :update_ordering, format: :json
    end

    member do
      get :toggle_visible_in_work_list, format: :js
    end
  end

  resources :customer_detail_categories, except: [:index, :show] do
    collection do
      put :update_ordering, format: :json
    end
  end

  resources :areas

  resources :customers do
    resources :risks_assessments do
      member do
        put 'check_assessment', to: 'risks_assessments#check_assessment', as: :check_risks_assessment
      end
    end
    resources :customer_notes, only: [:create, :destroy], format: :js
    resources :attachments, only: [:create, :destroy]
    resources :payments, except: :show

    member do
      post :create_job_for_current_week, format: :js
      get :activate
      get :deactivate
    end

    collection do
      get :clear_quick_search_params
    end
  end

  resources :jobs, except: [:index] do
    resources :job_notes, only: [:create, :destroy], format: :js

    member do
      get :mark_as_risk_checked, format: :js
      get :mark_as_completed, format: :js
      get :unmark_as_completed, format: :js
      put :mark_as_paid, format: :js
      put :update_notes, format: :js
      put :update_price, format: :json
      put :update_invoice_number, format: :json
      get :quick_new
      post :quick_create
    end
  end

  resources :work_list_categories, except: [:index, :show] do
    member do
      patch :update_balance_date_from
    end
  end

  resources :work_lists, only: [:show] do
    resources :work_list_notes, only: [:create, :destroy], format: :js

    collection do
      get :view, format: :js
    end

    member do
      put :update_tag, format: :json
      put :move_jobs, format: :json
      get :mark_as_completed
      get :mark_as_incompleted
      post :customer_search, format: :js
      get :add_job_for_customer, format: :js
      patch :update_target
      put :update_job_ordering, format: :json
      get :reset_job_ordering
      get :get_simple_show, format: :js
    end
  end

  resources :weeks, only: [:index, :show, :edit, :update] do
    collection do
      get :generate_cycle
    end

    member do
      get :work_lists
      patch :pull_work
      patch :push_work
    end
  end

  resources :users, except: [:show] do
    collection do
      get :sign_in
      post :sign_in, to: "users#sign_in_submit"
      get :sign_out_user
    end
  end

  resources :bookmarks, only: [:create], format: :json do
    collection do
      delete :destroy
    end
  end

  resources :finances, only: [:index, :show] do
    collection do
      get :weeks
      post :export
      get :get_export_status, format: :json
    end
  end

  resources :targets, only: [:index, :show]
  resources :expenses

  resources :customer_search_queries, only: [] do
    collection do
      post :search, format: :json
      get :get_search_status, format: :json
      delete :clear_search
      delete :remove_customer_from_search_results
      post :export
      get :get_export_status, format: :json
    end
  end

  get :work_list_settings, to: 'pages#work_list_settings'
  get :toggle_work_list_payment_method, to: 'businesses#toggle_work_list_payment_method', format: :js

  root to: 'pages#home'
end
