class WeekShifter

  def self.push_week_work_one_week(business, week)
    last_week = business.weeks.order('weeks.start_date DESC').first
    if last_week.jobs.count.zero?
      new_last_week = last_week
    else
      new_last_week = business.weeks.create!(start_date: last_week.next_week_start_date)
      WeekGenerator.generate_work_lists_for_week(business, new_last_week)
    end

    if new_last_week.start_date > week.start_date
      temp_new_last_week = new_last_week
      while temp_new_last_week != week
        previous_week = business.weeks.where(start_date: temp_new_last_week.previous_week_start_date).first
        if previous_week.nil?
          previous_week = business.weeks.create!(start_date: temp_new_last_week.previous_week_start_date)
          WeekGenerator.generate_work_lists_for_week(business, previous_week)
        end
        temp_new_last_week = previous_week
      end
    end

    while (previous_week = business.weeks.where(start_date: new_last_week.previous_week_start_date).first).present?
      previous_week.reset_jobs_to_another_week(new_last_week)
      break if previous_week == week
      new_last_week = previous_week
    end
  end

  def self.pull_week_work_one_week(business, week)
    while (next_week = business.weeks.where(start_date: week.next_week_start_date).first).present?
      next_week.reset_jobs_to_another_week(week)
      week = next_week
    end
  end

  def self.push_customer_start_dates_one_week(business)
    business.customers.not_one_off.find_each do |customer|
      customer.update_attribute(:start_date, (customer.start_date + 1.week).to_date)
    end
  end

  def self.pull_customer_start_dates_one_week(business)
    min_date = Date.parse("2000/1/1")
    business.customers.not_one_off.find_each do |customer|
      new_start_date = (customer.start_date - 1.week).to_date
      new_start_date = customer.start_date if new_start_date < min_date
      customer.update_attribute(:start_date, new_start_date)
    end
  end

end
