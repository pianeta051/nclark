namespace :work_lists do

  desc "Reset work list targets to work list category targets"
  task reset_targets: :environment do
    puts "Resetting work list targets to work list category targets..."
    WorkListCategory.find_each do |work_list_category|
      work_list_category.work_lists.update_all(target_cents: work_list_category.target_cents)
    end
    puts "Done."
  end

  desc "Update job caches"
  task update_job_caches: :environment do
    puts "Updating job caches for work lists"
    WorkList.find_each do |work_list|
      job_counter = work_list.job_counter_cache
      completed_job_cents = work_list.completed_job_cents_cache
      target_delta_cents = work_list.target_delta_cents_cache
      work_list.update_job_caches!
      if job_counter != work_list.job_counter_cache || completed_job_cents != work_list.completed_job_cents_cache || target_delta_cents != work_list.target_delta_cents_cache
        puts "Cache updated for work list #{work_list.id}"
      end
    end
    puts "Done."
  end

end
