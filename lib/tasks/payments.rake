namespace :payments do

  desc "Get accounts"
  task get_accounts: :environment do
    detail_names = Business.find(6).index_default_customer_details.pluck(:name)
    File.open("aug_to_oct_payments.csv", 'w') do |file|
      file.puts("date,amount,customer,method,,date,receipt no,price,vat,total,description")
      payments = Payment.where('payments.customer_id IN (SELECT customers.id FROM customers WHERE customers.business_id = ?)', 6)
                         .where('payments.cents > 0')
                         .where('payments.date >= ?', Date.parse('2015-08-01'))
                         .where('payments.date <= ?', Date.parse('2015-10-31'))
                         .order('payments.method ASC NULLS FIRST')
                         .order('payments.date ASC')
                         .to_a
      expenses = Expense.where(business_id: 6)
                         .where('expenses.date >= ?', Date.parse('2015-08-01'))
                         .where('expenses.date <= ?', Date.parse('2015-10-31'))
                         .order('expenses.date ASC')
                         .to_a

      max_length = payments.length > expenses.length ? payments.length : expenses.length

      (0...max_length).each do |i|
        payment = payments[i]
        expense = expenses[i]

        str = ""
        if payment.nil?
          str += ",,,"
        else
          str += payment.date.to_s(:long).gsub(',', '').gsub('  ', ' ')
          str += ","
          str += (payment.amount + payment.charge).format.gsub(',', '')
          str += ","
          str += payment.customer.index_detail_values_from_cache(detail_names)[0].gsub(',', ' ')
          str += ","
          str += payment.method.to_s
        end
        str += ",,"
        if expense.nil?
          str += ",,,,,"
        else
          str += expense.date.to_s(:long).gsub(',', '').gsub('  ', ' ')
          str += ","
          str += expense.receipt_number
          str += ","
          str += expense.price.format.gsub(',', '')
          str += ","
          str += expense.vat.format.gsub(',', '')
          str += ","
          str += (expense.price + expense.vat).format.gsub(',', '')
          str += ","
          str += expense.description.gsub(',', ';')
        end
        file.puts(str)
      end
=begin
      file.puts("date,amount,customer,method")
      Payment.where('payments.customer_id IN (SELECT customers.id FROM customers WHERE customers.business_id = ?)', 6)
             .where('payments.cents > 0')
             .where('payments.date >= ?', Date.parse('2015-08-01'))
             .where('payments.date <= ?', Date.parse('2015-10-31'))
             .order('payments.method ASC NULLS FIRST')
             .order('payments.date ASC')
             .each do |payment|
        str = payment.date.to_s(:long).gsub(',', '').gsub('  ', ' ')
        str += ","
        str += (payment.amount + payment.charge).format.gsub(',', '')
        str += ","
        str += payment.customer.index_detail_values_from_cache(detail_names)[0].gsub(',', ' ')
        str += ","
        str += payment.method.to_s
        file.puts(str)
      end

      file.puts("date,receipt no,price,vat,total")
      Expense.where(business_id: 6)
             .where('expenses.date >= ?', Date.parse('2015-08-01'))
             .where('expenses.date <= ?', Date.parse('2015-10-31'))
             .order('expenses.date ASC')
             .each do |expense|
        str = expense.date.to_s(:long).gsub(',', '').gsub('  ', ' ')
        str += ","
        str += expense.receipt_number
        str += ","
        str += expense.price.format.gsub(',', '')
        str += ","
        str += expense.vat.format.gsub(',', '')
        str += ","
        str += (expense.price + expense.vat).format.gsub(',', '')
        str += ","
        str += expense.description
        file.puts(str)
      end
=end
    end
  end

end
