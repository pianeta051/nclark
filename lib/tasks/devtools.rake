namespace :devtools do

  desc "Reset password"
  task reset_password: :environment do
    Business.find(6).update_attributes(password: "password", password_confirmation: "password")
    Business.find(6).users.find(1).update_attributes(password: "password", password_confirmation: "password")
  end

end
