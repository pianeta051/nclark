namespace :businesses do

  desc "Copy Nick's settings to the other businesses"
  task copy_nicks_settings_to_other_businesses: :environment do
    puts "Copying Nick's settings to the other businesses..."
    Business.find_each do |business|
      if business.id == 6
        puts "Skipping Nick's business"
      else
        puts "Updating #{business.name}... (#{business.customers.count} customers)"
        business.send(:copy_nicks_settings!)
      end
    end
    puts "Done"
  end

  desc "Do data migrations for early August changes"
  task do_data_migrations_early_august: :environment do
    Job.where('jobs.completed_at IS NOT NULL').update_all(completed_cache: true)
    
    ActiveRecord::Base.connection.execute("
      INSERT INTO job_payments (job_id, payment_id, cents, created_at, updated_at) (
        SELECT payments.job_id, payments.id, payments.cents, localtimestamp, localtimestamp
        FROM payments
        WHERE payments.job_id IS NOT NULL
        AND payments.id NOT IN (
          SELECT job_payments.payment_id FROM job_payments
        )
      )
    ")

    WorkList.find_each(&:update_job_caches!)
    WorkListCategory.find_each(&:update_balance!)
  end

end
