namespace :expenses do

  desc "Move expenses attachment to Attachment records"
  task move_attachments: :environment do
    Expense.where('expenses.attachment IS NOT NULL').find_each do |expense|
      if expense.attachment.present?
        a = expense.attachments.build(remote_attachment_url: expense.attachment.url)
        a.save
      end
    end
  end

  desc "Remove expenses attachments"
  task remove_attachments: :environment do
    Expense.where('expenses.attachment IS NOT NULL').find_each do |expense|
      expense.update_attributes(remove_attachment: true)
    end
  end

end
