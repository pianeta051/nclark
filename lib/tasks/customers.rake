namespace :customers do

  desc "Update customer details caches"
  task update_customer_details_caches: :environment do
    puts "Updating customer details caches..."
    i = 0
    Customer.find_each do |customer|
      i += 1
      customer.send(:update_customer_details_cache)
      puts(i) if i % 100 == 0
    end
    puts "Done"
  end

  desc "Update last clean"
  task update_last_cleans: :environment do
    puts "Updating last cleans..."
    i = 0
    Customer.find_each do |customer|
      i += 1
      customer.send(:update_last_clean)
      puts(i) if i % 100 == 0
    end
    puts "Done"
  end

  desc "Get accounts"
  task :get_accounts, [:month_range] => [:environment] do |t, args|
    Business.find_each do |business|
      name = business.name.gsub(/[^a-zA-Z]/, '')
      next unless name == 'NClarkWindowCleaningLtd'
      File.open(name + ".csv", 'w') do |file|
        range = eval(args[:month_range].gsub('-', '..'))
        range.each do |i|
          month_start_date = Date.today.beginning_of_year + (i - 1).months
          month_end_date = month_start_date.end_of_month
          business.jobs
                  .where('jobs.paid_at >= ?', month_start_date)
                  .where('jobs.paid_at <= ?', month_end_date)
                  .order('jobs.payment_method ASC, jobs.paid_at ASC')
                  .to_a
                  .each do |job|
            str = job.payment_method.to_s
            str += ","
            str += job.payment_amount.format
            str += ","
            str += job.customer.customer_details.where('customer_details.name ILIKE ?', 'Address line').first.try(:value).to_s.gsub(',', ' ')
            str += ","
            str += job.paid_at.to_s(:long).gsub(',', '')
            file.puts(str)
          end
        end
      end
    end
  end

  desc "Get customer addresses"
  task get_addresses: :environment do
    Business.find_each do |business|
      name = business.name.gsub(/[^a-zA-Z]/, '')
      next unless name == 'NClarkWindowCleaningLtd'
      File.open(name + ".csv", 'w') do |file|
        fields = %w(Forename Surname Address\ Line Town County Postcode)
        business.customers.active.find_each do |customer|
          cache = customer.customer_details_cache
          file.puts(fields.map { |f| cache[f].to_s.gsub(CustomerDetail::CACHE_VALUE_SEPARATOR, ', ') }.join(';'))
        end
      end
    end
  end

end
