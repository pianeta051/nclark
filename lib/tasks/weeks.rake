namespace :weeks do

  desc "Generate new week (to be run at beginning of week)"
  task generate_new_week: :environment do
    if Time.now.wday == 1
      date = Date.today.beginning_of_week
      Business.find_each do |business|
        previous_week = business.weeks.where(start_date: 1.week.ago.to_date).first
        week = business.weeks.where(start_date: date.to_date).first
        if week
          if previous_week
            updated_job_count = previous_week.jobs
                                             .not_completed
                                             .not_already_in_week(week)
                                             .update_all(work_list_id: nil, work_list_category_id: nil, day: nil, week_id: week.id)
            previous_week.jobs
                         .not_completed
                         .destroy_all
            puts "#{updated_job_count} jobs shifted to new week for business #{business.id} (#{business.name})"
          else
            puts "No previous week to shift jobs from for business #{business.id} (#{business.name})"
          end
        end

        new_week = WeekGenerator.generate_week_for(business, (date + (business.cycle_length - 1).weeks).to_date)
        unless new_week
          puts "Error with generating week for business #{business.id} (#{business.name})"
        end
      end
    else
      puts "Skipping week generation (not Monday)"
    end
  end

  desc "Update caches for weeks and customers"
  task update_caches: :environment do
    puts "Updating caches..."
    Customer.find_each(&:calculate_job_payments!)
    puts "Customers done"
    Week.find_each(&:update_finances_cache!)
    puts "Weeks done"
  end

end
