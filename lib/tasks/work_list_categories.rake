namespace :work_list_categories do

  desc "Update work list category balances"
  task reset_targets: :environment do
    puts "Updating work list category balances..."
    WorkListCategory.find_each(&:update_balance!)
    puts "Done."
  end

end
