class WeekGenerator

  def self.generate_week_for(business, start_date)
    start_date = start_date.beginning_of_week

    puts "Creating week for business #{business.id} (#{business.name}) starting #{start_date}..."

    week = business.weeks.where(start_date: start_date).first || business.weeks.create!(start_date: start_date)
    generate_jobs_for_week(business, week)
    generate_work_lists_for_week(business, week)

    puts "Done."
    return week
  rescue Exception => e
    puts "Error: #{e}"
    return nil
  end

  def self.generate_jobs_for_week(business, week)
    puts "Creating jobs for week starting #{week.start_date}..."

    beginning_of_this_week = week.start_date.beginning_of_week
    end_of_this_week = week.start_date.end_of_week

    business.customers.active.not_one_off.find_each do |customer|
      next if beginning_of_this_week < customer.start_date.beginning_of_week
      next if week.jobs.where(customer_id: customer.id).first.present?

      add_job_for_customer = false

      #TODO: replace with more efficient algorithm
      regularity = customer.regularity_in_time
      date = customer.start_date
      while date.beginning_of_week <= beginning_of_this_week
        if date.beginning_of_week == beginning_of_this_week
          add_job_for_customer = true
          break
        end
        date += regularity
      end
      
      if add_job_for_customer
        attributes = {
          customer: customer,
          week: week,
          cents: customer.regular_price_cents,
          vat_rate: customer.add_vat? ? business.default_vat_rate : 0.0
        }
        job = business.jobs.create(attributes)
        if customer.day_span > 1
          child_attributes = attributes.merge(linked_job: job)
          (2..customer.day_span).each do |i|
            business.jobs.create(child_attributes.merge(linked_job_day: i))
          end
        end
      end
    end
  end

  def self.generate_work_lists_for_week(business, week)
    week.create_work_lists(business)
  end

end
