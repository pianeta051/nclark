module Bookmarkable
  def self.included(klass)
    klass.has_many :bookmarks, as: :bookmarkable, dependent: :destroy
    class_name = klass.name
    table_name = class_name.downcase.pluralize
    klass.scope :bookmarked_by, lambda { |user|
      klass.where("#{table_name}.id IN (SELECT bookmarks.bookmarkable_id FROM bookmarks WHERE bookmarks.bookmarkable_type = ? AND bookmarks.user_id = ?)", class_name, user.id)
    }
    klass.scope :unbookmarked_by, lambda { |user|
      klass.where("#{table_name}.id NOT IN (SELECT bookmarks.bookmarkable_id FROM bookmarks WHERE bookmarks.bookmarkable_type = ? AND bookmarks.user_id = ?)", class_name, user.id)
    }
  end

  def bookmarked?(user, bookmark_bookmarkable_ids_cache = nil)
    if bookmark_bookmarkable_ids_cache
      bookmark_bookmarkable_id = bookmark_bookmarkable_ids_cache.bsearch { |bookmarkable_id| bookmarkable_id >= id }
      bookmark_bookmarkable_id == id
    else
      bookmarks.where(user: user).count > 0
    end
  end
end
